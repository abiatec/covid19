<?php
require('../boot.php');

error_reporting(0);


use X4\Classes\Install;

session_start();

header('Content-Type: text/html; charset=utf-8');

$install = new Install();

function showDomains($install)
{


    $domains = $install->getCurrentInstalledDomains();

    echo '<h3>Choose domain source</h3><form method="POST">';

    foreach ($domains as $domain) {
        echo '<input name="domain" type="radio" value=' . $domain['basic'] . '>' . $domain['basic'] . "\r\n";
    }

    echo '<br/><input type="submit"></form>';
}


if ($_POST['domain']) {
    $install->transformDomains(array($_POST['domain'] => HTTP_HOST));
    $install->createInitialFolders();
    $install->runModuleInstallers();

    $adm = new X4\AdminBack\AdminPanel();
    $adm->clearCache(true);

    echo '<p>DOMAIN REINSTALLED ' . $_POST['domain'] . '</p>';
    echo '<p>CACHE  CLEARED</p>';

}

showDomains($install);
