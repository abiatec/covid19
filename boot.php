<?php

if (!file_exists(__DIR__ . '/x4/vendor/autoload.php')) {
    echo('Composer dependencies are not installed - please run `composer install` ');
    die();
}

require_once __DIR__ . '/x4/vendor/autoload.php';

require($_SERVER['DOCUMENT_ROOT'] . '/x4/inc/core/helpers.php');
require($_SERVER['DOCUMENT_ROOT'] . '/x4/inc/helpers/common.helper.php');
require($_SERVER['DOCUMENT_ROOT'] . '/x4/inc/classes/ImageRendererUtils.php');


$generationTimeStart = Common::getmicrotime();

Common::includeAll($_SERVER['DOCUMENT_ROOT'] . '/x4/inc/helpers', array('common.helper.php'));

X4Autoloader::init();

use X4\Classes\XRegistry;
use X4\Classes\MultiSection;
use X4\Classes\XPDO;
use X4\Classes\XNameSpaceHolder;
use X4\Classes\XEventMachine;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

require($_SERVER['DOCUMENT_ROOT'] . '/conf/init.php');
require(xConfig::get('PATH', 'CORE') . 'connector.php');
require(xConfig::get('PATH', 'CORE') . 'xModuleCommon.php');
require(xConfig::get('PATH', 'CORE') . 'xModuleCommonModels.php');
require(xConfig::get('PATH', 'CORE') . 'xModuleCommonModel.php');
require(xConfig::get('PATH', 'CORE') . 'xModulePrototype.php');
require(xConfig::get('PATH', 'CORE') . 'xListener.php');
require(xConfig::get('PATH', 'CORE') . 'xPlugin.php');
require(xConfig::get('PATH', 'CORE') . 'xPluginBack.php');
require(xConfig::get('PATH', 'CORE') . 'xModuleBack.php');
require(xConfig::get('PATH', 'CORE') . 'xModule.php');
require(xConfig::get('PATH', 'CORE') . 'xTpl.php');
require(xConfig::get('PATH', 'CORE') . 'xModuleApi.php');
require(xConfig::get('PATH', 'CORE') . 'xAction.php');
require(xConfig::get('PATH', 'CORE') . 'core.php');
require(xConfig::get('PATH', 'CORE') . 'pageAgregator.php');
require(xConfig::get('PATH', 'CORE') . 'helpers.tpl.php');
require(xConfig::get('PATH', 'XOAD') . 'xoad.php');


class boot
{
    public static function loggerStart()
    {
        try {
            $log = new Logger('general');
            $log->pushHandler(new StreamHandler(PATH_ . 'logs/general.log', Logger::INFO));
            xRegistry::set('logger', $log);

            $errLog = new Logger('error');
            $errLog->pushHandler(new StreamHandler(PATH_ . 'logs/error.log', Logger::INFO));
            xRegistry::set('errorLogger', $errLog);

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public static function connectDatabase()
    {
        try {
            XPDO::setSource(xConfig::get('DB', 'DB_HOST'), xConfig::get('DB', 'DB_NAME'), xConfig::get('DB', 'DB_USER'), xConfig::get('DB', 'DB_PASS'), 'utf8', xConfig::get('DB', 'DB_PORT'));
            XRegistry::set('XPDO', XPDO::getInstance());
        } catch (\Exception $e) {

            if (!$_SERVER['CONSOLE']) {
                throw $e;
            }
        }
    }

    public static function setupNSObjects()
    {
        $enhance = new ENHANCE();
        XRegistry::set('ENHANCE', $enhance);
        XNameSpaceHolder::addObjectToNS('E', $enhance);

        $debug = new DEBUG();
        XRegistry::set('DEBUG', $debug);
        XNameSpaceHolder::addObjectToNS('D', $debug);

        $localize = new LOCALIZE();
        XRegistry::set('LOCALIZE', $localize);
        XNameSpaceHolder::addObjectToNS('L', $localize);
    }
}


Common::loadDriver('XCache', 'XCacheFileDriver');
boot::loggerStart();
XRegistry::set('TMS', $TMS = new MultiSection());
boot::connectDatabase();
boot::setupNSObjects();

XRegistry::set('EVM', XEventMachine::getInstance());

xCore::pluginEventDetector();
xCore::moduleEventDetector();

