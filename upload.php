<?php
error_reporting(0);
use X4\Classes\MatrixFileManager;

require('boot.php');

if (isset($_POST['path'])) {
    $MFM = new MatrixFileManager();
    $MFM->getFile();
} else {

    if (!empty($_FILES['file'])) {
        $file = $_FILES['file']['name'];
        $file = ENHANCE::translit(array('value' => $file));
        $file = preg_replace("/[^A-Za-z0-9\.]/", '', $file);
        $file = date('Y-m-d_H-i-s') . '_' . $file;
        $_FILES['file']['name'] = $file;
    }


    session_start();

    $user_upload_dir = '/media/uploads/' . session_id() . '/';

    $options = array(
        'user_upload_dir' => $user_upload_dir,
        'param_name' => 'file',
        'accept_file_types' => '/\.(doc|jpeg|jpg|png|gif|pdf|docx)$/i'
    );

    require_once($_SERVER['DOCUMENT_ROOT'] . '/x4/inc/ext/fileupload/UploadHandler.php');

    $upload_handler = new UploadHandler($options);

}
