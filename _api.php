<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS,PUT,DELETE');
header('Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept');

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    // return only the headers and not the content
    // only allow CORS if we're doing a GET - i.e. no saving for now.
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']) && $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'] == 'GET') {
        header('Access-Control-Allow-Headers: X-Requested-With, Authorization, Content-Type');
    }
    exit;
}

error_reporting(0);

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Attribute\NamespacedAttributeBag;
use X4\Classes\XRegistry;
use X4\Classes\AltoRouter;
use X4\Classes\XApi;

require('boot.php');

XRegistry::set('SESSION', new Session(new NativeSessionStorage(),new NamespacedAttributeBag()));
XRegistry::get('SESSION')->start();


XRegistry::get('EVM')->fire('apiBoot');
xConfig::set('GLOBAL', 'currentMode', 'api');

$router = new altoRouter();

$apiController = new XApi();

if (xConfig::get('GLOBAL', 'apiAuthEnabled')) {
    $apiController->auth(xConfig::get('GLOBAL', 'apiBasicAuthLogin'), xConfig::get('GLOBAL', 'apiBasicAuthPassword'));
}

$router->map('GET|POST|DELETE|PATCH|PUT|HEAD|OPTIONS', '/~api/[*:api]/[*:module]/[a:action]/[*:trailing]?', 'apiController#route', 'api');

$router->map('GET', '/~api/[*:api]/[*:module]', 'apiController#document', 'apiDocs');

$match = $router->match();

if ($match === false) {

    $apiController->error500();
    echo "url error:\r\n";
    echo "should be /~api/[*:api]/[*:module]/[*:action]/[*:trailing] format";
    echo "\r\n";
    echo "<br/>";
    echo "use /~api/[*:api]/[*:module] for docs";

} else {

    list($controller, $action) = explode('#', $match['target']);
    if (method_exists($apiController, $action)) {
        $result = call_user_func_array(array($apiController, $action), array($match['params']));
        echo $result;
    } else {
        $apiController->error500();
        echo "route is not defined\r\n";

    }
}
