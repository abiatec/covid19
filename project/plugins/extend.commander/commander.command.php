<?php

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Ifsnop\Mysqldump as IMysqldump;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Daveismyname\SqlImport\Import;


class clearCacheCommand extends Command
{
    public $foldersDoNotClear = array('treewarm', 'PSG-param', 'imagecache', 'templates');

    protected function configure()
    {
        $this->setName('commander:clearCache')
            ->setDescription('Clears all the cache')
            ->setHelp('This command allows you remove cache')
            ->addOption(
                'PSG',
                null,
                InputOption::VALUE_OPTIONAL,
                'Remove PSG cache',
                false
            );

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['Begin cache clear', '========================', '',]);
        $cachePath = xConfig::get('PATH', 'CACHE');
        $folders = array_diff(scandir($cachePath), array('.', '..'));

        $optionValue = $input->getOption('PSG');

        if ($optionValue !== false) {
            $this->foldersDoNotClear = array_diff($this->foldersDoNotClear, ['PSG-param']);
        }

        if (!empty($folders)) {
            foreach ($folders as $path) {

                if (!in_array($path, $this->foldersDoNotClear)) {

                    $output->writeln([$cachePath . $path]);

                    if (strstr(php_uname(), 'Windows')) {
                        exec(sprintf("rd /s /q %s", escapeshellarg($cachePath . $path)));

                    } else {
                        exec(sprintf("rm -rf %s", escapeshellarg($cachePath . $path)));
                    }
                }

            }
        }


        if (strstr(php_uname(), 'Windows')) {
            $output->writeln(['Remove bak files', '========================', '',]);
            exec(escapeshellarg('del /s /f /q ' . $_SERVER['DOCUMENT_ROOT'] . '\*.bak'));
        } else {
            $output->writeln(['', '========================', 'Set permissions']);
            exec("chmod -R 777 " . $cachePath);
        }


        X4\Classes\XRegistry::get('EVM')->fire('AdminPanel:afterCacheClear', array('instance' => $this));

        $output->writeln(['', '========================', 'Cleared']);
    }
}


class dumpDatabaseCommand extends Command
{


    protected function configure()
    {
        $this->setName('commander:dumpDB')
            ->setDescription('Dumps db')
            ->setHelp('This command allows you to dump DB use commander:dumpDB outputFilename.sql ')
            ->addArgument('file', InputArgument::OPTIONAL, 'The input file.');

    }


    private function createFolder($sqlPathName)
    {
        $fileSystem = new Filesystem();
        try {
            if (!$fileSystem->exists($sqlPathName)) {
                $fileSystem->mkdir($sqlPathName, xConfig::get('GLOBAL', 'defaultModeFiles'));

            }
            return true;
        } catch (IOExceptionInterface $exception) {
            echo "An error occurred while creating your directory at " . $exception->getPath();
            return false;
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['<info>Dump process started</info>']);

        $sqlPathName = PATH_ . 'sql/';

        if (!$this->createFolder($sqlPathName)) {
            $output->writeln(['<info>Folder is not writable</info>' . ' ' . $sqlPathName]);
            return null;
        }

        $date = date('d-m-y-H-i-s');

        $fileName = $input->getArgument('file');

        if (empty($fileName)) {
            $fileName = "backup${date}.sql";
        }

        $dumpSettings = array(
            'compress' => IMysqldump\Mysqldump::NONE,
            'no-data' => false,
            'add-drop-table' => true,
            'single-transaction' => true,
            'lock-tables' => true,
            'add-locks' => true,
            'extended-insert' => true,
            'disable-foreign-keys-check' => true,
            'skip-triggers' => false,
            'add-drop-trigger' => true,
            'databases' => true,
            'add-drop-database' => true,
            'hex-blob' => true
        );

        $dump = new IMysqldump\Mysqldump('mysql:host=' . xConfig::get('DB', 'DB_HOST') . ';dbname=' . xConfig::get('DB', 'DB_NAME'), xConfig::get('DB', 'DB_USER'), xConfig::get('DB', 'DB_PASS'), $dumpSettings);

        $dump->start($sqlPathName . $fileName);

        $output->writeln(['<info>Dumped -></info> ' . $fileName]);
    }
}


class loadDatabaseCommand extends Command
{


    protected function configure()
    {
        $this->setName('commander:loadDB')
            ->setDescription('Loads db')
            ->setHelp('This command allows you to load DB use  commander:loadDB  inputFilename.sql ')
            ->addArgument('file', InputArgument::REQUIRED, 'The input file.');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['<info>Load process started</info>']);

        $fileName = PATH_ . 'sql/' . $input->getArgument('file');
        if (file_exists($fileName)) {

            $import = new Import($fileName,
                xConfig::get('DB', 'DB_USER'),
                xConfig::get('DB', 'DB_PASS'),
                xConfig::get('DB', 'DB_NAME'),
                xConfig::get('DB', 'DB_HOST'),
                true);
        }

        $output->writeln(['<info>Loaded -></info> ' . $fileName]);

    }
}


class showAvailDumpsCommand extends Command
{


    protected function configure()
    {
        $this->setName('commander:showDumps')
            ->setDescription('shows db dumps list')
            ->setHelp('This command allows you to show dumps list');

    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['<info>Dumps list:</info>']);
        foreach (new DirectoryIterator(PATH_ . 'sql/') as $fileInfo) {
            if ($fileInfo->isDot() || $fileInfo->getExtension() != 'sql') {
                continue;
            }
            $output->writeln([$fileInfo->getFilename() . ' ' .  XFILES::formatSize($fileInfo->getSize()).' '.date('Y-m-d h:i:s',$fileInfo->getATime())]);
        }
    }
}





