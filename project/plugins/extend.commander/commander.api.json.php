<?php

/**
 *
 * @SWG\Swagger(
 *   @SWG\Info(
 *     title="X4 commander plugin API",
 *     version="1.0.0"
 *   ),
 *     schemes={"http","https"},
 *     basePath="/~api/json/extend.commander",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *
 *     @SWG\Definition(
 *         definition="Error",
 *         required={"code", "message"},
 *         @SWG\Property(
 *             property="code",
 *             type="integer",
 *             format="int32"
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string"
 *         )
 *     )
 *
 * )
 */
class commanderApiJson extends commanderFront
{

    public $listenerInstance;

    public function __construct($listenerInstance)
    {
        $this->listenerInstance = $listenerInstance;
        parent::__construct(__FILE__);
    }


    /**
     * @SWG\Get(
     *     path="/clearCache",
     *     summary="Clears cache",
     *     operationId="clearCache",
     *     produces={"application/json"},
     *     @SWG\Response(response=200, description="News items"),
     * )
     */

    public function clearCache()
    {
        $adm = new X4\AdminBack\AdminPanel();
        $adm->clearCache(true);
        return array('result' => true);
    }


}
