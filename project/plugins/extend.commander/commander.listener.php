<?php

use Migrate\Command\InitCommand;
use Migrate\Command\CreateCommand;
use Migrate\Command\DownCommand;
use Migrate\Command\UpCommand;
use Migrate\Command\AddEnvCommand;
use Migrate\Command\StatusCommand;


class commanderListener extends xListener implements xPluginListener
{
    public function __construct()
    {
        parent::__construct('extend.commander');
        self::$commandsRegistry[]=new CreateCommand();
        self::$commandsRegistry[]=new DownCommand();
        self::$commandsRegistry[]=new UpCommand();
        self::$commandsRegistry[]=new AddEnvCommand();
        self::$commandsRegistry[]=new StatusCommand();
        self::$commandsRegistry[]=new InitCommand();
    }
}
