<?php


class shortLinksTpl extends xTpl implements xPluginTpl
{
    public function __construct(){}
        
    public function pre($params)
    {                                                
       return htmlentities($params['value'], ENT_QUOTES, 'UTF-8');
    }
    
}
