<?php
use X4\Classes\xRegistry;
use X4\Classes\XPDO;

class shortLinksListener extends xListener  implements xPluginListener
{
    public function __construct()
    {
        parent::__construct('catalog.shortLinks');     
        $this->useModuleTplNamespace();        
    }
    
}
