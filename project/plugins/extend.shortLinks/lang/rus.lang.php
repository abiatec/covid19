<?php

$LANG = array(
    'text' => 'Текст',
    'shortLinks' => 'Короткие ссылки',
    'edit_ShortLink' => 'Редактировать ссылку',
    'rules_list'=>'Список ссылок',


    'linkFrom' => 'Короткая ссылка',
    'linkTo' => 'Перенаправление на Ссылка',
    'text' => 'Коммментарий',
    'numberOfTransitions' => 'Кол-во перходов',

    'linkToTitle' => 'Обязательно относительные ссылки /  без hostName',

    'generateLink' => 'Сгенерировать ссылку',


    'shortlink-rule-edited' => 'Такая ссылка уже существует',
    'shortlink-rule-edited-format' => 'Ссылка должна начинаться на sh-',
    'empty-link-edited' => 'Ссылка пуста',

    'is-save-edited' => 'Сохранено успешно',



);
