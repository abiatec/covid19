shortLinksBack = new Class({
    Extends: _xModuleBack,
    initialize: function (name) {

        this.setName(name);
        this.parent();
        this.setLayoutScheme('listView', {});
        this.connector.module = 'plugin.extend.shortLinks';

    },


    onModuleInterfaceBuildedAfter: function () {

    },

    onHashDispatch: function (e, v) {
        this.tabs.makeActive('t' + e);
        return true;
    },


    CRUN: function () {

    },


    start: function () {
        

    },


    
    buildInterface: function () {
        this.parent();

    }


});
    
        