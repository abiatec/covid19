<?php

class locationsTpl extends xTpl implements xPluginTpl
{

    public function __construct($module)
    {
        $this->listenerInstance = $module;
        parent::__construct(__FILE__);
    }


    public function getLocationByOwner($params)
    {

        if (isset($params['id']) && !empty($params['id'])) {

            $catalog = xCore::moduleFactory('catalog.front');
            $locs = $catalog->_commonObj->_tree->selectAll()->where(['location.owner', '=', (int)$params['id']], ['location.type', '=', 'Shipping'])->run();

            return $locs;

        } else {

            return false;
        }
    }


}
