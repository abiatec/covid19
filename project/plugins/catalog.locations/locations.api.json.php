<?php


/**
 *
 * @SWG\Swagger(
 *   @SWG\Info(
 *     title="X4 rgand plugin API",
 *     version="1.0.0"
 *   ),
 *     schemes={"http","https"},
 *     basePath="/~api/json/catalog.locations",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *
 *
 *   @SWG\Definition(
 *     definition="locationObject",
 *     type="object",
 *     allOf={
 *      @SWG\Schema(
 *           @SWG\Property(property="location.type", type="string" ),
 *           @SWG\Property(property="location.isMain", type="string"),
 *           @SWG\Property(property="Name", type="string" ),
 *           @SWG\Property(property="location.description", type="string"),
 *           @SWG\Property(property="location.country", type="string"),
 *           @SWG\Property(property="location.state", type="string"),
 *           @SWG\Property(property="location.zip", type="string"),
 *           @SWG\Property(property="location.street_1", type="string"),
 *           @SWG\Property(property="location.street_2", type="string"),
 *           @SWG\Property(property="location.house", type="string"),
 *           @SWG\Property(property="location.appartment", type="string"),
 *           @SWG\Property(property="location.city", type="string")
 *       )
 *    }
 *    ),
 *
 *     @SWG\Definition(
 *         definition="Error",
 *         required={"code", "message"},
 *         @SWG\Property(
 *             property="code",
 *             type="integer",
 *             format="int32"
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string"
 *         )
 *     )
 *
 * )
 */
class locationsApiJson extends locationsFront
{

    public $listenerInstance;

    public function __construct($listenerInstance)
    {
        $this->listenerInstance = $listenerInstance;
        parent::__construct(__FILE__);
        $this->fusers = xCore::moduleFactory('fusers.front');
        $this->fdata = $this->fusers->_commonObj->getAuthorizedUser();
    }


    /**
     * @SWG\Post(
     *     path="/createNewLocation",
     *     summary="Creates new location",
     *     operationId="createNewLocation",
     *     produces={"application/json"},
     *
     *     @SWG\Parameter(
     *       type="string",
     *       name="Authorization",
     *       in="header",
     *      example="Bearer ",
     *      required=true
     *     ),
     *
     *     @SWG\Parameter(
     *         name="locationObject",
     *         in="body",
     *         description="location object",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/locationObject")
     *     ),
     *     @SWG\Response(response=200, description="Object created")
     * )
     */


    public function createNewLocation($params, $data)
    {
        $data['location.owner'] = $this->fdata['id'];

        $data['PropertySetGroup'] = $this->_config['locationsPropertySet'];

        $objId = $this->_module->_tree->initTreeObj($this->_config['locationsGroup'], '%SAMEASID%', '_CATOBJ', $data);

        if (!$objId) {
            return $this->error('object-not-created', 400);
        }

        return array('id' => $objId);
    }


    /**
     * @SWG\Post(
     *     path="/saveEditedLocation/id/{id}",
     *     summary="saves edited location",
     *     operationId="saveEditedLocation",
     *     produces={"application/json"},
     *        @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Object id",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *       type="string",
     *       name="Authorization",
     *       in="header",
     *      example="Bearer ",
     *      required=true
     *     ),
     *
     *     @SWG\Parameter(
     *         name="locationObject",
     *         in="body",
     *         description="location object",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/locationObject")
     *     ),
     *     @SWG\Response(response=200, description="Object created")
     * )
     */


    public function saveEditedLocation($params, $data)
    {
        if ($params['id'] && $this->fdata) {

            $owner = $this->_module->_tree->readNodeParam($params['id'], 'location.owner');

            if ($owner == $this->fdata['id']) {
                $this->_module->_tree->reInitTreeObj($params['id'], '%SAME%', $data, '_CATOBJ');
                return array('saved' => true);
            } else {

                return $this->error('not-an-owner', 402);
            }

        } else {
            return $this->error('user-not-authorized', 401);
        }

    }


    /**
     * @SWG\Get(
     *     path="/getLocation/id/{id}",
     *     summary="Gets location only this owner",
     *     operationId="getLocation",
     *     produces={"application/json"},
     *
     *     @SWG\Parameter(
     *      type="string",
     *      name="Authorization",
     *      in="header",
     *     example="Bearer ",
     *     required=true),
     *
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Object id",
     *         required=true
     *     ),
     *     @SWG\Response(response=200, description="Gets object info"),
     * )
     */


    public function getLocation($params)
    {

        if (isset($params['id'])) {

            $obj = $this->_module->_tree->selectStruct('*')->selectParams('*')->where(array
            (
                '@id',
                '=',
                $params['id']

            ))->run();

            if ($obj) {

                $obj = $this->_module->_commonObj->convertToPSG($obj, array('addWhere' => array('location.owner', '=', $this->fdata['id'])));

                return $obj;
            }

        } else {

            return $this->error('Id is not provided', 400);

        }
    }


    /**
     * @SWG\Get(
     *     path="/deleteLocation/id/{id}",
     *     summary="deletes location",
     *     operationId="deleteLocation",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         type="integer",
     *         description="location id",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *       type="string",
     *          name="Authorization",
     *       in="header",
     *      example="Bearer ",
     *     required=true),
     *
     *     @SWG\Response(response=200, description="")
     * )
     */

    public function deleteLocation($params)
    {
        if ($params['id'] && $this->fdata) {
            $owner = $this->_module->_tree->readNodeParam($params['id'], 'location.owner');

            if ($owner == $this->fdata['id']) {

                $this->_module->_commonObj->_tree->delete()->where(['@id', '=', $params['id']])->run();

                return array('deleted' => true);
            } else {

                return $this->error('User  not own this object', 401);
            }

        } else {
            return $this->error('Not auhtorized', 401);
        }


    }


    /**
     * @SWG\Get(
     *     path="/getBuyerLocations",
     *     summary="gets locations",
     *     operationId="getBuyerLocations",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *       type="string",
     *          name="Authorization",
     *       in="header",
     *      example="Bearer ",
     *     required=true),
     *
     *     @SWG\Response(response=200, description="")
     * )
     */

    public function getBuyerLocations($params)
    {
        if ($this->fdata) {

            $locations = $this->_module->_commonObj->_tree->selectAll()->where(['location.owner', '=', $this->fdata['id']])->run();
            return $locations;

        } else {

            return $this->error('User  not own this object', 401);
        }
    }


}
