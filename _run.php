<?php
error_reporting(0);

use X4\Classes\XRegistry;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Attribute\NamespacedAttributeBag;

require('boot.php');

XRegistry::set('SESSION', new Session(new NativeSessionStorage(),new NamespacedAttributeBag()));
XRegistry::get('SESSION')->start();

xCore::websiteLockerListener();
xConfig::set('GLOBAL', 'currentMode', 'front');
XRegistry::get('EVM')->fire('zero-boot');
xConfig::addToBranch('PATH', array('fullBaseUrlHost' => CHOST . $_SERVER['REQUEST_URI'], 'fullBaseUrl' => $_SERVER['REQUEST_URI'], 'baseUrl' => $_SERVER['REQUEST_URI']));
XRegistry::get('EVM')->fire('boot');
xCore::listenToXoad();
XRegistry::set('TPA', $TPA = new pageAgregator($generationTimeStart));
$TPA->setRenderMode(RENDERMODE);

echo $TPA->dispatcher();