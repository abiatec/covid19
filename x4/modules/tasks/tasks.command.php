<?php

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class tasksStartCommand extends Command
{
    protected function configure()
    {

        $this->setName('tasks:start')
            ->setDescription('Starts cron tasks')
            ->setHelp('This command allows to start tasks');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tasks = $this->_commonObj->getCurrentTasks();
        if (!empty($tasks)) {
            foreach ($tasks as $task) {
                $output->writeln("<info>{$task['task_name']} - {$task['task_method']}</info>");
                $date = date('d-m-Y H:i:s', $task['last_launch']);
                $output->writeln("<info>Last launch : {$date}</info>");
            }
        }
    }
}


