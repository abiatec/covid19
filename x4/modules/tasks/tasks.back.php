<?php

use X4\Classes\TableJsonSource;


class tasksBack extends xModuleBack
{
    use _TASK;

    public function __construct()
    {
        parent::__construct(__CLASS__);

    }

    public function tasksTable($params)
    {

        $source = new X4\Classes\TableJsonSource();

        $params['onPage'] = $this->_config['tasksPerPage'];

        $opt = array
        (
            'onPage' => $params['onPage'],
            'table' => 'tasks',
            'order' => array
            (
                'id',
                'asc'
            ),
            'idAsNumerator' => 'id',
            'columns' => array
            (
                'id' => array(),
                'task_name' => array(),
                'task_method' => array(),
                'period' => array(),
                'last_launch' => array
                (
                    'onAttribute' => TableJsonSource::$fromTimeStamp,
                    'onAttributeParams' => array('format' => 'd.m.y H:i:s')
                )
            )
        );

        $source->setOptions($opt);


        if (!$params['page']) {
            $params['page'] = 1;
        }


        $this->result = $source->createView($params['id'], $params['page']);

    }

    public function deleteTasks($params)
    {

        if (is_array($params['id'])) {
            $id = implode($params['id'], "','");
            $where = 'id in (\'' . $id . '\')';
        } else {
            $where = 'id="' . $params['id'] . '"';
        }

        $query = 'delete from tasks where ' . $where;

        if ($this->_PDO->query($query)) {
            $this->result['deleted'] = true;
        }
    }


}