<?php

class fusersTpl extends xTpl implements xModuleTpl
{

    public function getAdditionalFields($params)
    {
        return $this->_config['additionalFields'];
    }

    public function getUser($params)
    {
        return $_SESSION['siteuser'];
    }

    public function getUserById($params)
    {

        return $this->_tree->getNodeinfo($params['id']);

    }

    public function isFavorite($params)
    {
        if (!empty($params['obj_id']) && !empty($_SESSION['siteuser']['favorites'])) {
            return in_array($params['obj_id'], $_SESSION['siteuser']['favorites']);
        } else {
            return false;
        }
    }

    public function getUserPriceAlias()
    {
        if (empty($_SESSION['userPriceCategoryAlias'])) {
            if (!empty($_SESSION['userPriceCategory'])) {
                $explKey = explode('.', $_SESSION['userPriceCategory']);
                $catalog = xCore::moduleFactory('catalog.front');
                $pset = $catalog->_commonObj->_propertySetsTree->singleResult()->selectParams('*')->where(array('@ancestor', '=', 1), array('@basic', '=', $explKey[0]))->run();

                if ($pset) {
                    $property = $catalog->_commonObj->_propertySetsTree->singleResult()->selectParams('*')->where(array('@ancestor', '=', $pset['id']), array('@basic', '=', $explKey[1]))->run();
                    $_SESSION['userPriceCategoryAlias'] = $property['params']['alias'];
                    return $_SESSION['userPriceCategoryAlias'];
                }
            }
        } else {
            return $_SESSION['userPriceCategoryAlias'];
        }
    }

    public function getUserAdditionalById($params)
    {
        if (!empty($params['id'])) {
            $params['id'] = (int)$params['id'];
            $additional = $this->_tree->selectParams('*')->childs($params['id'], 1)->singleResult()->run();
            if ($additional['params']) {
                return $additional['params'];
            } else {
                return false;
            }
        }
    }


}
