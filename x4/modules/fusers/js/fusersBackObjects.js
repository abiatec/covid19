_FUSERSGROUP = new Class({

    Extends: CRUN,
    initialize: function (context) {
        this.parent(context, {
            objType: '_FUSERSGROUP',
            autoCreateMethods: true
        });


    },


    deleteGroup: function () {
        this.context.deleteObjectGrid(this.context.tree);
    },

    save: function (e) {
        e.preventDefault();
        this.parent();

        if (this.validated) {
            data = xoad.html.exportForm("create" + this.options.objType);
            this.context.connector.execute({
                onSave_FUSERSGROUP: {
                    data: data
                }
            });

        }
    },


    saveEdited: function (e) {
        e.preventDefault();


        this.parent();
        if (this.validated) {
            data = xoad.html.exportForm('edit_FUSERSGROUP');
            result = this.context.execute
            ({
                onSaveEdited_FUSERSGROUP: {
                    id: this.selectedId,
                    data: data
                }
            });

        }

    },

    edit: function (data) {

        this.context.tabs.addTab({
            id: 'teditnews',
            name: AI.translate('fusers', 'edit_group'),
            temporal: true,
            active: true
        }, true);


        this.parent(data);

        this.context.execute({
            onEdit_FUSERSGROUP: {
                id: data.id
            }
        });

        xoad.html.importForm('edit_FUSERSGROUP', this.context.connector.result.data);


    }

});


_FUSER = new Class({
    Extends: CRUN,
    initialize: function (context) {
        this.parent(context, {
            objType: '_FUSER',
            autoCreateMethods: true
        });

        $(document).on('click', '#' + this.context.name + ' a.save-new-password', [], this.changePassword.bind(this));
        this.inputHB = TH.getTplHB('fusers', 'FUSEREXTDATA_input');
        this.checkboxHB = TH.getTplHB('fusers', 'FUSEREXTDATA_checkbox');
        this.textareaHB = TH.getTplHB('fusers', 'FUSEREXTDATA_textarea');//NEW
        this.selectHB = TH.getTplHB('fusers', 'FUSEREXTDATA_select');//NEW
    },


    renderAdditionalFields: function () {
        this.context.execute({getAdditionalFields: true});

        if (typeof this.context.connector.result.additionalFields != 'undefined') {
            var selectdata = [];
            var html = '';
            var that = this;
            $(this.context.connector.result.additionalFields).each(function (k, v) {

                if (v.type == 'checkbox') {
                    html += that.checkboxHB(v);
                } else if (v.type == 'textarea')//NEW
                {
                    html += that.textareaHB(v);
                } else if (v.type == 'select')//NEW
                {

                    var mySelect = v.selectText.split('\n');
                    selectdata[v.fieldName] = [];
                    for (var i = 0, ln = mySelect.length; i < ln; ++i) {
                        selectdata[v.fieldName][i] = [];
                        var myOptions = mySelect[i].split(':');
                        selectdata[v.fieldName][i]['value'] = myOptions[0];
                        selectdata[v.fieldName][i]['text'] = myOptions[1];
                    }
                    html += that.selectHB(v);
                } else {

                    html += that.inputHB(v);
                }
            });
            this.context.mainViewPortFind('#additional_FUSEREXTDATA').html(html);
            this.insertSelectList(selectdata);
        }
    },

    insertSelectList: function (selectdata) {//NEW

        for (var key in selectdata) {
            if (selectdata.hasOwnProperty(key)) {
                for (var itemKey in selectdata[key]) {
                    if (selectdata[key].hasOwnProperty(itemKey)) {
                        $('#' + key).append($("<option></option>").attr("value", selectdata[key][itemKey]['value']).text(selectdata[key][itemKey]['text']));
                    }
                }
            }

        }
    },

    changePassword: function (e) {
        e.preventDefault();
        var data = xoad.html.exportForm('exportPassword_FUSER');
        data.id = this.selectedId;

        this.context.connector.execute({
            changeUserPassword: {
                data: data

            }
        });

        this.context.mainViewPortFind('#newPassword').val('');
    },


    create: function (data) {

        this.parent();
        xoad.html.importForm('create_FUSER', this.context.connector.result.data);
        this.renderAdditionalFields();
    },


    save: function (e) {
        e.preventDefault();
        this.parent();

        if (this.validated) {
            data = xoad.html.exportForm("create" + this.options.objType);
            additionalFields = xoad.html.exportForm('additional_FUSEREXTDATA');
            this.context.connector.execute({
                onSave_FUSER: {
                    data: data,
                    additionalFields: additionalFields
                }
            });

        }

    },


    saveEdited: function (e) {
        e.preventDefault();


        this.parent();
        if (this.validated) {
            var data = xoad.html.exportForm('edit_FUSER');
            var ishopData = xoad.html.exportForm('ishop_tunes');
            var additionalFields = xoad.html.exportForm('additional_FUSEREXTDATA');
            var result = this.context.execute({
                onSaveEdited_FUSER: {
                    id: this.selectedId,
                    data: data,
                    ishopData: ishopData,
                    additionalFields: additionalFields

                }
            });

        }

    },

    edit: function (data) {


        if (data && data.id) {
            this.selectedId = data.id;
        } else if (this.context.tree) this.selectedId = this.context.tree.getSelectedRowId();

        var tpl = TH.getTplHB(this.context.name, this.options.objType + '@edit');

        this.context.execute
        ({
            onEdit_FUSER: {
                id: data.id
            }
        });


        this.context.setMainViewPort(tpl({"ishopPrices": this.context.connector.result.ishopPrices}));

        this.form = this.context.mainViewPortFind("#edit" + this.options.objType);
        this.form.validationEngine();
        this.context.mainViewPortFind('a.save').unbind('click').click(this.saveEdited.bind(this));

        xoad.html.importForm('ishop_tunes', this.context.connector.result.ishopData);

        additionalFields = this.context.connector.result.additionalFields;
        xoad.html.importForm('edit_FUSER', this.context.connector.result.data);
        this.context.mainViewPortFind('.btn-group .btn-switch').btnSwitch();
        this.renderAdditionalFields();

        xoad.html.importForm('additional_FUSEREXTDATA', additionalFields);

    },

    deleteFuser: function () {
        this.context.deleteObjectGrid(this.gridlist, 'deleteFuser');
    },

    switchFuserActivity: function (rId, cInd, state) {
        var cell = this.gridlist.cellById(rId, 0);
        this.context.execute({switchFuserActivity: {id: cell.getValue(), state: state}});
    },


    showFusersList: function (data) {

        this.context.tabs.addTab({
            id: 'tshowFusersList',
            name: AI.translate('fusers', 'fusers-list'),
            temporal: true,
            active: true
        }, true);


        this.context.setGridView('contentsListContainer', 750, true);
        var menu = new dhtmlXMenuObject();
        menu.renderAsContextMenu();

        if (__globalLogLevel == 9) {
            menu.addNewChild(menu.topId, 0, "console-it", 'console-it', false, '', '', this.context.consoleIt.bind(this));

        }

        menu.addNewChild(menu.topId, 0, "delete", AI.translate('common', 'delete'), false, '', '', this.deleteFuser.bind(this));
        this.gridlist = new dhtmlXGridObject('contentsListContainer');

        this.gridlist.setImagePath("x4/adm/xres/ximg/grid/imgs/");
        this.gridlist.setHeader('id,' + AI.translate('fusers', 'login') + ',' + AI.translate('fusers', 'name') + ',' + AI.translate('fusers', 'surname') + ',' + AI.translate('fusers', 'email') + ',' + AI.translate('common', 'active'));

        this.gridlist.setInitWidths("80,*,240,240,190,80");
        this.gridlist.setColAlign("center,left,left,left,left,center");
        this.gridlist.attachEvent("onCheckbox", this.switchFuserActivity.bind(this));
        this.gridlist.attachEvent("onRowDblClicked", function (kid) {
            this.context.navigate('edit_FUSER', {id: kid})
        }.bind(this));
        this.gridlist.setColTypes("ro,ro,ro,ro,ro,ch");
        this.gridlist.enableAutoWidth(true);
        this.gridlist.enableDragAndDrop(true);
        this.gridlist.enableMultiselect(true);
        this.gridlist.enableContextMenu(menu);
        this.gridlist.init();
        this.gridlist.onPage = 200;
        this.gridlist.setSkin("modern");


        this.gridlist.rowToDragElement = function (id) {
            if (this.cells(id, 2).getValue() != "") {
                return this.cells(id, 2).getValue() + "/" + this.cells(id, 1).getValue();
            } else {
                return this.cells(id, 1).getValue();
            }
        };


        this.listOrders(data.id, data.page);
        var pg = new paginationGrid(this.gridlist, {
            target: this.context.mainViewPortFind('.paginator'),
            pages: this.context.connector.result.pagesNum,
            url: AI.navHashCreate(this.context.name, 'showUsersList', {id: data.id}) //,

        });

        this.gridlist.gridToTreeElement = function (tree, fakeID, gridID, treeID) {
            /*                        this.connector.execute({changeAncestor:{ancestor:treeID,id:gridID}});
             if(this.connector.result.dragOK)
             {
             XTR_main.set_result(_lang_fusers['user_moved']);
             return true;
             }else{*/
            return -1;
            /*}*/

        }.bind(this);

    },

    listOrders: function (id, page) {

        this.context.connector.execute({
            fusersTable: {
                id: id,
                page: page,
                onPage: this.gridlist.onPage
            }
        });

        if (this.context.connector.result.data_set) {

            this.gridlist.parse(this.context.connector.result.data_set, "xjson")
        }

    }


});


_PROPERTYSETFUSERS = new Class({//NEW
    Extends: CRUN,
    initialize: function (context) {
        this.parent(context, {
            objType: '_PROPERTYSETFUSERS',
            autoCreateMethods: true
        });
        this.context.addInnerRoute('propertySetsList', 'propertySetsList', this);


    },

    create: function () {
        this.parent();
        this.context.PROPERTYFUSERS.initPropertiesList();
        this.context.properties = {};

        this.context.connector.execute({
            getPropertyList: {}
        });

        if (this.context.connector.result.success) {
            this.context.properties = this.context.connector.result.additionalFields;
            this.context.PROPERTYFUSERS.renderPropertiesList();
        }
    },

    save: function (e) {

        e.preventDefault();
        propertySetData = xoad.html.exportForm(this.form.get(0).id);
        this.context.connector.execute({
            onSave_PROPERTYSETFUSERS: {
                properties: this.context.properties,
                propertySetData: propertySetData
            }
        });

        this.context.connector.execute({
            getPropertyList: {}
        });

        if (this.context.connector.result.success) {
            this.context.properties = this.context.connector.result.additionalFields;
            this.context.PROPERTYFUSERS.renderPropertiesList();
        }

    },
});
_PROPERTYFUSERS = new Class({//NEW
    Extends: CRUN,

    initialize: function (context) {
        this.parent(context, {
            objType: '_PROPERTYFUSERS',
            autoCreateMethods: true,
        });

    },

    create: function () {

        TH.getTpl(this.context.name, this.options.objType);
        this.createPropertyEditorWindow(this.options.objType);
        this.form = jQuery("#create" + this.options.objType);
        this.form.validationEngine();
        xoad.html.importForm("create" + this.options.objType, {
            type: this.context.propertiesSelector
        });
        $(this.propertyEditorContext).find('.propertyEditor .save').click(this.save.bind(this));
    },

    edit: function (data) {

        var data = {
            id: data
        };
        TH.getTpl(this.context.name, this.options.objType + '@edit');
        this.createPropertyEditorWindow(this.options.objType + '@edit');

        xoad.html.importForm("edit" + this.options.objType, {
            type: this.context.propertiesSelector
        });

        var pdata = this.context.properties[data.id];

        xoad.html.importForm("edit" + this.options.objType, pdata.params);

        if (typeof pdata.params.options == 'object') {
            xoad.html.importForm("edit" + this.options.objType, this.transformToLine('options', pdata.params.options));
        }


        xoad.html.importForm("edit" + this.options.objType, {
            basic: pdata.basic
        });

        this.id = data.id;
        this.form = jQuery("#edit" + this.options.objType);


        $(this.propertyEditorContext).find('#edit_PROPERTYFUSERS').validationEngine();

        $(this.propertyEditorContext).find('.propertyEditor .saveEdited').click(this.saveEdited.bind(this));

        var type = $('#typeProperty').val();
        if (type == 'select') {
            $('#selectTextBlock').show();
        }


    },

    save: function (e) {

        e.preventDefault();
        this.validated = $(this.propertyEditorContext).find("#create" + this.options.objType).validationEngine('validate');
        if (this.validated) {
            data = xoad.html.exportForm('create_PROPERTYFUSERS');
            id = '0' + generateGUID();
            basic = data.basic;
            delete data.basic;
            this.context.properties[id] = ({
                isNew: true,
                basic: basic,
                id: id,
                params: data
            });

            this.renderPropertiesList();
            this.propertyEditorWin.close();

        }
    },

    saveEdited: function (e) {

        e.preventDefault();
        this.validated = $(this.propertyEditorContext).find("#edit" + this.options.objType).validationEngine('validate');
        if (this.validated) {
            data = xoad.html.exportForm('edit_PROPERTYFUSERS');
            basic = data.basic;

            delete data.basic;
            this.context.properties[this.id] = ({
                isNew: this.context.properties[this.id].isNew,
                basic: basic,
                id: this.id,
                params: data
            });
            this.renderPropertiesList();
            this.propertyEditorWin.close();

        }

    },

    transformToLine: function (prefix, listTransform) {//dont use?

        var transformed = {};
        for (i in listTransform) {
            transformed[prefix + '.' + i] = listTransform[i];
        }

        return transformed;
    },

    onChangePropertyType: function (e) {//dont use?

        var v = $(e.target);
        if (val = v[0].selectedOptions[0].value) {
            this.setPropertyTypeHtml(val);
        }

    },

    setPropertyTypeHtml: function (type) {//dont use?

        $('.propertyEditor .propertyTypeEditor').html(this.context.propertiesHolder[type].backOptionsTemplate);
        this.context.fireEvent('propertyTypeEditorReady', {
            type: type
        });
    },

    createPropertyEditorWindow: function (tpl) {

        this.propertyEditorWin = AI.dhxWins.createWindow("propertyEditor", 20, 10, 600, 600, 1);
        this.propertyEditorWin.setModal(true);
        this.propertyEditorWin.setText(AI.translate('fusers', 'add_propertyFusers'));
        this.propertyEditorWin.attachEvent("onHide", function (win) {
            win.close();
        });
        this.propertyEditorWin.attachHTMLString(TH.getTpl('fusers', tpl));
        this.propertyEditorWin.button('park').hide();
        this.propertyEditorWin.centerOnScreen();
        this.propertyEditorContext = this.propertyEditorWin.dhxcont;
    },

    copyProperty: function (kid, id) {

        var newid = '0' + generateGUID();
        newObj = Object.clone(this.context.properties[id]);
        newObj.id = newid;
        newObj.isNew = true;
        newObj.basic = this.context.properties[id].basic + '_copy';
        this.context.properties[newid] = newObj;

        this.renderPropertiesList();
    },

    deleteProperty: function (id) {

        if (selected = this.gridlist.getSelectedId()) {
            selected = selected.split(',');
        } else {
            return;
        }
        if (selected.length > 1) {
            result = confirm(AI.translate('common', 'you_really_wish_to_remove_this_objects'));
        } else {
            result = confirm(AI.translate('common', 'you_really_wish_to_remove_this_object'));
        }

        if (result) {
            this.gridlist.deleteSelectedRows();

            for (var i in selected) {
                if (selected.hasOwnProperty(i)) {
                    var kid = selected[i];
                    delete this.context.properties[kid];
                    if ((this.propertiesVals) && (typeof this.propertiesVals[kid] == 'object')) delete this.propertiesVals[kid];
                }
            }

            this.context.connector.execute({
                delete_PROPERTYSETFUSERS: {
                    id: selected
                }
            });

        }

    },

    renderPropertiesList: function () {

        var dataset = [];
        Object.each(this.context.properties, function (val, id) {

            vp = val.params;
            dataset[id] = {
                data: [id, vp.alias, val.basic, AI.translate('fusers', vp.type), vp.isObligatorily, vp.isComparse, 'edit']
            }

        });
        this.gridlist.clearAll();
        this.gridlist.parse({
            rows: dataset
        }, "xjson");
    },

    propertiesListDragger: function () {

        var rowsAll = this.gridlist.getAllRowIds().split(',');
        var tempProperties = {};
        Array.each(rowsAll, function (val) {
            tempProperties[val] = this.context.properties[val];

        }.bind(this));

        this.context.properties = tempProperties;

    },

    initPropertiesList: function () {//NEW

        var menu = new dhtmlXMenuObject();
        menu.renderAsContextMenu();

        menu.addNewChild(menu.topId, 0, 'delete', AI.translate('common', "delete"), false, '', '', this.deleteProperty.bind(this));
        menu.addNewChild(menu.topId, 0, 'copy', AI.translate('common', "copy"), false, '', '', this.copyProperty.bind(this));
        menu.addNewChild(menu.topId, 0, 'refresh', AI.translate('common', "refresh"), false, '', '', this.renderPropertiesList.bind(this));

        this.gridlist = new dhtmlXGridObject('properties');
        this.gridlist.selMultiRows = true;
        this.gridlist.setImagePath("/x4/adm/xres/ximg/grid/imgs/");
        this.gridlist.setHeader('id,' + AI.translate('common', 'alias') + ',' + AI.translate('common', 'name') + ',' + AI.translate('common', 'type'));
        this.gridlist.setInitWidths("70,190,180,140,110,120");

        this.gridlist.setColAlign("center,left,left,left,center,center");
        this.gridlist.setColTypes("ro,ro,ro,ro,ch,ch");
        this.gridlist.attachEvent("onRowDblClicked", this.edit.bind(this));
        this.gridlist.attachEvent("onCheckbox", this.onPropertiesCheckbox.bind(this));
        this.gridlist.enableAutoWidth(true);
        this.gridlist.enableContextMenu(menu);
        this.gridlist.enableDragAndDrop(true);
        this.gridlist.attachEvent("onDrop", this.propertiesListDragger.bind(this));
        this.gridlist.init();
        this.gridlist.setSkin("modern");

    },

    onPropertiesCheckbox: function (id, num, val) {//dont use?

        var val = val ? 1 : 0;
        if (num == 4) this.context.properties[id].params['isObligatorily'] = val;
        if (num == 5) this.context.properties[id].params['isComparse'] = val;

    }

});
