<?php

class fusersCommon  extends xModuleCommon implements xCommonInterface
{
    public $_useTree = true;
    public static $authorizedUser = null;

    public function __construct()
    {
        parent::__construct(__CLASS__);

        $this->_tree->setLevels(5);
        $this->_tree->setObject('_ROOT', array
        (
            'defaultUnregisteredGroup',
            'defaultRegisteredGroup'
        ));

        $this->_tree->setObject('_FUSERSGROUP', array
        (
            'Name',
            'discountScheme',
            'comments'
        ), array('_ROOT'));

        $this->_tree->setObject('_FUSER', array
        (
            'lastVisit',
            'password',
            'name',
            'surname',
            'patronymic',
            'email',
            'company',
            'address',
            'discountScheme',
            'defaultPrice',
            'accessiblePrices',
            'site',
            'phone',
            'active',
            'birthDate',
            'userType',
            'verificationCode',
            'avatar',
            'comment'
        ), array('_FUSERSGROUP'));

        $this->_tree->setObject('_FUSEREXTDATA', null, array('_FUSER'));
    }


    public function isUserAuthorized()
    {
        return !empty(self::$authorizedUser);
    }


    public function logout()
    {

        xRegistry::get('SESSION')-remove('siteuser/id');

    }

    public function authorizeUserById($id)
    {
        $user = $this->_tree->getNodeInfo($id);
        return $this->checkAndLoadUser($user['basic'], $user['params']['password'], true);
    }

    public function getAuthorizedUser()
    {
        return self::$authorizedUser;
    }

    public function checkAndLoadUser($login, $password, $doNotHashPassword = false)
    {
        $user = $this->_tree->selectStruct('*')->selectParams('*')->where(array('@basic', '=', $login))->singleResult()->run();

        if (empty($user)) {
            $user = $this->_tree->selectStruct('*')->selectParams('*')->where(array('email', '=', $login))->singleResult()->run();
        }

        if (empty($user)) {
            return array('authorized' => false, 'error' => 'user-not-defined');
        }


        if ((Common::passwordVerify($password,$user['params']['password'],$doNotHashPassword)) && ($user['params']['active'])) {

            $additional = $this->_tree->selectParams('*')->childs($user['id'], 1)->singleResult()->run();

            if (!empty($additional)) {
                $additional = $additional['params'];
                $user['additionalFields'] = $additional;
            } else {
                $user['additionalFields'] = [];
            }

            self::$authorizedUser = $user;

            xRegistry::get('SESSION')->set('siteuser/id',self::$authorizedUser['id']);

            return array('authorized' => true, 'user' => $user);

        } else {
            return array('authorized' => false, 'error' => 'user-login-password-incorrect');

        }
    }


    public function checkUserLoginAndEmail($login, $email)
    {
        if (!empty($login)) {
            $isLogin = $this->_tree->selectStruct('*')->where(array
            (
                '@basic',
                '=',
                $login
            ))->singleResult()->run();
        }

        if (!empty($email)) {
            $isEmail = $this->_tree->selectStruct('*')->where(array
            (
                'email',
                '=',
                $email
            ))->singleResult()->run();
        }

        return array
        (
            'isLogin' => $isLogin,
            'isEmail' => $isEmail
        );
    }


    public function saveAdditionalFields($id, $additionalFields)
    {

        $additionalDataNode = $this->_tree->selectAll()->childs($id)->singleResult()->run();

        if (!empty($additionalDataNode['params'])) {
            $data['additionalFields'] = array_merge($additionalDataNode['params'], $additionalFields);
        }

        if (!empty($additionalFields)) {
            foreach ($this->_config['additionalFields'] as $v) {
                if (!isset($additionalFields[$v['fieldName']]) && $v['type'] == 'checkbox') {
                    $additionalFields[$v['fieldName']] = '';
                }
            }
        }

        $this->initAdditionalFields($id, $data['additionalFields']);

    }

    public function initAdditionalFields($id, $params)
    {
        $this->_tree->delete()->childs($id)->run();
        $this->_tree->initTreeObj($id, '%SAMEASID%', '_FUSEREXTDATA', $params);
    }


    public function defineFrontActions()
    {
        $this->defineAction('showAuthPanel');
        $this->defineAction('showUserPriceCategory');
        $this->defineAction('showUsersFromGroup');
        $this->defineAction('userPanel', array('serverActions' => array
        (
            'needAuth',
            'destroyUser',
            'submitUser',
            'registration',
            'forgotPassword',
            'logout',
            'editUser',
            'login',
            'saveProfile',
            'userPanel',
            'submitUser',
            'verifyUser'
        )));
    }
}


