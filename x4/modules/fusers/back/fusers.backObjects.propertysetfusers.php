<?php
use X4\Classes\XRegistry;

trait _PROPERTYSETFUSERS
{

    public function delete_PROPERTYSETFUSERS($params)//NEW
    {
        if (!empty($params['id'])) {
            if (is_array($params['id'])) {
                foreach ($params['id'] as $key => $value) {
                    $params['id'][$key] = "'$value'";
                }
                $params['id'] = implode(',', $params['id']);
                $qwery = "DELETE FROM `fusers_additional_properties` WHERE `id` IN ({$params['id']})";
            } else {
                $qwery = "DELETE FROM `fusers_additional_properties` WHERE `id` = '{$params['id']}'";
            }
            if (XRegistry::get('XPDO')->query($qwery)) {
                return new OkResult();
            }
        }
        return new BadResult('value-not-deleted');
    }

    public function onSave_PROPERTYSETFUSERS($params)//NEW
    {
        if (!empty($params['properties'])) {
            foreach ($params['properties'] as $propertyInfo) {
                $idPropertiesList[] = "'{$propertyInfo['id']}'";
                $params['properties'][$propertyInfo['id']]['params']['fieldName'] = $params['properties'][$propertyInfo['id']]['basic'];
            }

            $idPropertiesList = implode(',', $idPropertiesList);
            $updateDataArray = [];
            $updateIdList = [];
            $namesAdditionalFieldsInSql = array('alias', 'fieldName', 'type', 'selectText');

            if ($pdoResult = XRegistry::get('XPDO')->query("SELECT id FROM `fusers_additional_properties` WHERE `id` IN ($idPropertiesList)")->fetchall(PDO::FETCH_ASSOC)) {
                foreach ($namesAdditionalFieldsInSql as $name) {
                    $updateDataList = [];

                    foreach ($pdoResult as $pdoResultValue) {
                        if (!in_array($pdoResultValue['id'], $updateIdList)) {
                            $updateIdList[] = $pdoResultValue['id'];
                        }

                        $updateDataList[] = "WHEN {$pdoResultValue['id']} THEN '{$params['properties'][$pdoResultValue['id']]['params'][$name]}'";
                    }
                    $updateDataList = implode(' ', $updateDataList);
                    $updateDataArray[] = "$name = (CASE id $updateDataList END)";
                }

                if (count($updateDataArray) > 0) {
                    $updateDataArray = implode(',', $updateDataArray);
                    $updateIdListInString = implode(',', $updateIdList);
                    XRegistry::get('XPDO')->query("UPDATE `fusers_additional_properties` SET $updateDataArray WHERE id IN($updateIdListInString)");
                }

            }
            $insertDataList = [];

            foreach ($params['properties'] as $propertyInfo) {
                if (!in_array($propertyInfo['id'], $updateIdList)) {
                    $insertDataList[] = "('{$params['properties'][$propertyInfo['id']]['params']['alias']}','{$params['properties'][$propertyInfo['id']]['basic']}','{$params['properties'][$propertyInfo['id']]['params']['type']}','{$params['properties'][$propertyInfo['id']]['params']['selectText']}',0)";
                }
            }
            if (count($insertDataList) > 0) {
                $insertDataList = implode(',', $insertDataList);
                XRegistry::get('XPDO')->query("INSERT INTO `fusers_additional_properties` (alias, fieldName, type, selectText, rate) VALUES $insertDataList");
            }
        }
        return new OkResult();

    }

    public function getPropertyList()
    {
        $returnArray = [];
        $pdoResult = XRegistry::get('XPDO')->query("SELECT * FROM `fusers_additional_properties`");
        $additionalFields = $pdoResult->fetchall(PDO::FETCH_ASSOC);
        foreach ($additionalFields as $additionalFieldsInfo) {
            $returnArray[$additionalFieldsInfo['id']] = array(
                'basic' => $additionalFieldsInfo['fieldName'],
                'id' => $additionalFieldsInfo['id'],
                'isNew' => true,
                'params' => $additionalFieldsInfo
            );
        }
        $this->result['additionalFields'] = $returnArray;
        $this->result['success'] = true;
    }

}
