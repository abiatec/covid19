<?php

use X4\Classes\XRegistry;

trait _FUSERSROLES
{


    public function onEdit_FUSERSROLES($params)
    {
        $data = $this->_tree->getNodeInfo($params['id'], true);
        $data['params']['fuserGroup'] = $this->_tree->readNodeParam($data['ancestor'], 'Name');
        $data['params']['login'] = $data['basic'];
        $additional = $this->_tree->selectParams('*')->childs($params['id'], 1)->run();
        $this->result['ishopPrices'] = $this->getIshopPrices();
        $this->result['data'] = $data['params'];

        if (!empty($data['params']['accessiblePrices'])) {

            foreach ($data['params']['accessiblePrices'] as $key => $price) {
                $key = str_replace('.', '@', $key);
                $this->result['ishopData']['accessiblePrices.' . $key] = $price;
            }
        }

        $this->result['ishopData']['defaultPrice'] = str_replace('.', '@', $data['params']['defaultPrice']);

        if (!empty($additional[0])) {
            $this->result['additionalFields'] = $additional[0]['params'];
        }
    }


    public function onSaveEdited_FUSERSROLES($params)
    {
        if (!empty($params['data'])) {
            if (!empty($params['ishopData']['accessiblePrices'])) {
                foreach ($params['ishopData']['accessiblePrices'] as $key => $value) {
                    $key = str_replace('@', '.', $key);
                    $params['data']['accessiblePrices'][$key] = $value;
                }
            }

            $params['data']['defaultPrice'] = str_replace('@', '.', $params['ishopData']['defaultPrice']);

            $this->_tree->reInitTreeObj($params['id'], '%SAME%', $params['data']);

            $childs = $this->_tree->selectStruct('*')->childs($params['id'], 1)->run();

            if (!empty($childs[0])) {
                $this->_tree->reInitTreeObj($childs[0]['id'], '%SAME%', $params['additionalFields']);
            } else {
                $this->_tree->initTreeObj($params['id'], '%SAMEASID%', '_FUSEREXTDATA', $params['additionalFields']);
            }

            xRegistry::get('EVM')->fire($this->_moduleName . '.fuserEditedSaved', array('user' => $params));

            return new OkResult('fuser-edited-saved');
        } else {
            return new BadResult('fuser-not-saved');
        }
    }


    public function onSave_FUSERSROLES($params)
    {
        $this->_tree->setUniqType(2);

        $checkedData = $this->_commonObj->checkUserLoginAndEmail($params['data']['login'], $params['data']['email']);

        if ((!$checkedData['isLogin']) && (!$checkedData['isEmail'])) {

            $params['data']['password'] = Common::passwordHash($params['data']['password']);

            if ($id = $this->_tree->initTreeObj($params['data']['fuserGroup'], $params['data']['login'], '_FUSER', $params['data'])) {

                $this->_tree->initTreeObj($id, '%SAMEASID%', '_FUSEREXTDATA', $params['additionalFields']);

                xRegistry::get('EVM')->fire($this->_moduleName . '.fuserSaved', array('user' => $params));

                return new OkResult('fuser-saved');
            }

        } else {
            return new BadResult('fuser-already-exists');
        }
    }


    public function onCreate_FUSERSROLES($params)
    {

        $directory = xConfig::get('PATH', 'MODULES');
        $modules = xCore::discoverModules();

        $pathes=[];
        $processors = \Swagger\Analysis::processors();

        foreach ($modules as $module) {

            $analyser = new \Swagger\StaticAnalyser();
            $analysis = new \Swagger\Analysis();
            $analysis->addAnalysis($analyser->fromFile($directory.$module['name'].'/'.$module['name'].'.api.json.php'));
            $analysis->process($processors);

            if($analysis->swagger->paths){
                foreach($analysis->swagger->paths as $path){
                    if($path->get) {
                        $pathes[$module['name']]['get'][] = array('path'=>$path->get->path,'summary'=>$path->get->summary,'operationId'=>$path->get->operationId);
                    }

                    if($path->post) {
                        $pathes[$module['name']]['post'][] = array('path'=>$path->post->path,'summary'=>$path->post->summary,'operationId'=>$path->post->operationId);
                    }
                }
            }

        }

        $this->result['pathes']=$pathes;

    }

}
