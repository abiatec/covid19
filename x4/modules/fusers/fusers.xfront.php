<?php

use X4\Classes\XPDO;
use Ahc\Jwt\JWT;

class fusersXfront extends fusersFront
{
    public function isAuthorized($params)
    {
        return $_SESSION['siteuser']['authorized'];
    }

    public function addFavorite($params)
    {
        if (!empty($_SESSION['siteuser']['id']) && !empty($params['obj_id']) && is_numeric($params['obj_id'])) {
            $favorite = array(
                'id' => 'null',
                'user_id' => (int)$_SESSION['siteuser']['id'],
                'obj_id' => (int)$params['obj_id']
            );

            XPDO::insertIN('favorite', $favorite);
            $favid = XPDO::getLastInserted();

            if (!isset($_SESSION['siteuser']['favorites'])) {
                $_SESSION['siteuser']['favorites'] = [];
            }

            $_SESSION['siteuser']['favorites'][$favid] = $params['obj_id'];
        }

    }

    public function delFavorite($params)
    {
        if (!empty($params['id']) && is_numeric($params['id'])) {
            XPDO::deleteIN('favorite', (int)$params['id']);
            unset($_SESSION['siteuser']['favorites'][$params['id']]);
        } else if (!empty($params['obj_id']) && is_numeric($params['obj_id']) && !empty($_SESSION['siteuser']['id'])) {
            XPDO::deleteIN('favorite', 'obj_id = "' . $params['obj_id'] . '" AND user_id = "' . $_SESSION['siteuser']['id'] . '"');
            $favid = array_search($params['obj_id'], $_SESSION['siteuser']['favorites']);
            unset($_SESSION['siteuser']['favorites'][$favid]);
        }
    }

    public function delAllFavorites()
    {
        if (!empty($_SESSION['siteuser']['id'])) {
            XPDO::deleteIN('favorite', 'user_id = "' . $_SESSION['siteuser']['id'] . '"');
            unset($_SESSION['siteuser']['favorites']);
        }
    }


    public function loadUserByToken($params)
    {

        if (!$_SESSION['siteuser']['authorized']) {
            $jwt = $params['authToken'];
            if (!empty($jwt)) {
                try {
                    $payload = (new JWT(xConfig::get('GLOBAL', 'jwtSecretKey'), 'HS256', xConfig::get('GLOBAL', 'jwtMaxAge')))->decode($jwt);
                } catch (Exception $e) {
                    $payload = false;
                }
                if (!empty($payload) && is_array($payload)) {
                    $authResult = $this->_commonObj->authorizeUserById($payload['uid']);
                    $user = $authResult['user'];
                    $_SESSION['siteuser']['id'] = $user['id'];
                    $_SESSION['siteuser']['userGroup'] = $user['ancestor'];
                    $_SESSION['siteuser']['authorized'] = true;
                    $_SESSION['siteuser']['userdata'] = $user['params'];
                    $_SESSION['siteuser']['userdata']['login'] = $user['basic'];

                    $this->result['data'] = array('status' => true);

                } else {
                    $this->result['data'] = array('status' => false);
                }
            } else {
                $this->result['data'] = array('status' => false);
            }
        } else {
            $this->result['data'] = array('status' => true);
        }

    }

}
