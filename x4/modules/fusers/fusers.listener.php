<?php

use Ahc\Jwt\JWT;
use X4\Classes\XRegistry;

class fusersListener extends xListener implements xModuleListener
{

    public function __construct()
    {
        parent::__construct('fusers');
        $this->_EVM->on('boot', 'authUser', $this);
        $this->_EVM->on('apiBoot', 'authUserOnApi', $this);
    }


    public function authUser($params)
    {
        
        if (xRegistry::get('SESSION')->get('siteuser/id')) {
             $this->authorizeUserById(xRegistry::get('SESSION')->get('siteuser/id'));
        }

        if (isset($_REQUEST['redirectAfterAuth'])) {
            xRegistry::get('SESSION')->set('redirectAfterAuth',base64_decode($_REQUEST['redirectAfterAuth']));

        }

    }

    public function authUserOnApi($params)
    {
        $headers = getallheaders();
        $authHeader = $headers['Authorization'];

        if (!empty($authHeader)) {
            list($jwt) = sscanf($authHeader, 'Bearer %s');
            if (!empty($jwt)) {
                $payload = (new JWT(xConfig::get('GLOBAL', 'jwtSecretKey'), 'HS256', xConfig::get('GLOBAL', 'jwtMaxAge')))->decode($jwt);

                if (!empty($payload) && is_array($payload)) {
                    $authResult = $this->authorizeUserById($payload['uid']);
                    if (!$authResult['authorized']) {
                        header('HTTP/1.0 401 Unauthorized');
                    }

                } else {
                    header('HTTP/1.0 401 Unauthorized');
                    die();
                }

            }
        }
    }

}
