<?php

use Ahc\Jwt\JWT;


/**
 *
 * @SWG\Swagger(
 *   @SWG\Info(
 *     title="X4 Fusers module API",
 *     version="1.0.0"
 *   ),
 *     schemes={"http","https"},
 *     basePath="/~api/json/fusers",
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *
 *    @SWG\Definition(
 *     definition="loginData",
 *     type="object",
 *     allOf={
 *       @SWG\Schema(
 *           required={"login","password"},
 *           @SWG\Property(property="login", type="string"),
 *           @SWG\Property(property="password", type="string")
 *       )
 *    }
 *    ),
 *
 *   @SWG\Definition(
 *     definition="additionalFields",
 *     type="object",
 *     allOf={
 *       @SWG\Schema(
 *           required={"anyParam"},
 *           @SWG\Property(property="anyParam", type="string")
 *       )
 *    }
 *    ),
 *
 *   @SWG\Definition(
 *     definition="saveUserObject",
 *     type="object",
 *     allOf={
 *       @SWG\Schema(
 *           required={"ancestor"},
 *           @SWG\Property(property="newPassword", type="string", example=""),
 *           @SWG\Property(property="email", type="string", example="sample@email.com"),
 *           @SWG\Property(property="additionalFields", ref="#/definitions/additionalFields"),
 *           @SWG\Property(property="params", ref="#/definitions/userParams")
 *
 *       )
 *    }
 *    ),
 *
 *   @SWG\Definition(
 *     definition="userObject",
 *     type="object",
 *     allOf={
 *       @SWG\Schema(
 *           required={"ancestor"},
 *           @SWG\Property(property="login", type="string", example="sample@email.com"),
 *           @SWG\Property(property="password", type="string", example="mypassword"),
 *           @SWG\Property(property="userGroupId", type="string", example=""),
 *           @SWG\Property(property="email", type="string", example="sample@email.com"),
 *            @SWG\Property(property="additionalFields", ref="#/definitions/additionalFields"),
 *           @SWG\Property(property="params", ref="#/definitions/userParams")
 *
 *       )
 *    }
 *    ),
 *
 *  @SWG\Definition(
 *     definition="userParams",
 *     type="array",
 *     allOf={
 *       @SWG\Schema(
 *            @SWG\Property(property="name", type="string", example="ivan"),
 *            @SWG\Property(property="surname", type="string", example="ivanov"),
 *            @SWG\Property(property="patronymic", type="string", example="ivanovich"),
 *            @SWG\Property(property="phone", type="string", example="phone"),
 *            @SWG\Property(property="address", type="string", example="address"),
 *            @SWG\Property(property="avatar", type="string", example="avatar"),
 *            @SWG\Property(property="active", type="boolean", example="true"),
 *            @SWG\Property(property="site", type="string", example="site")
 *       )
 *    }
 *    ),
 *
 *    @SWG\Definition(
 *     definition="password",
 *     type="object",
 *     allOf={
 *       @SWG\Schema(
 *           required={"password"},
 *           @SWG\Property(property="password", type="string")
 *       )
 *    }
 *    ),
 *
 *     @SWG\Definition(
 *         definition="Error",
 *         required={"code", "message"},
 *         @SWG\Property(
 *             property="code",
 *             type="integer",
 *             format="int32"
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string"
 *         )
 *     )
 *
 * )
 */
class fusersApiJson extends xModuleApi
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    private function getFacebookUser($token)
    {
        $fb = new \Facebook\Facebook($this->_config['facebookAccout']);

        try {
            $response = $fb->get('/me', $token);
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            return $this->error($e->getMessage(), 400);


        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            return $this->error($e->getMessage(), 400);
        }

        return $response->getGraphUser();
    }

    /**
     * @SWG\Post(
     *     path="/loginViaFacebook",
     *     summary="Front user Login facebook",
     *     operationId="loginViaFacebook",
     *     produces={"application/json"},
     *    @SWG\Parameter(
     *         name="userObject",
     *         in="body",
     *         description="user object",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/userObject")),
     *
     *     @SWG\Response(response=200, description="Auth data")
     * )
     */

    public function loginViaFacebook($params, $data)
    {

        if (!empty($data['login'])) {
            $user = $this->_tree->selectStruct('*')->selectParams('*')->where(array('@basic', '=', $data['login']))->singleResult()->run();

            $me = $this->getFacebookUser($data['access-token']);

            if (!empty($user)) {
                $user = $this->_tree->selectStruct('*')->selectParams('*')->where(array('@basic', '=', $me['id']))->singleResult()->run();

                if (!$user) {
                    return $this->error('User does not exists', 500);
                }

                if ($user['params']['active']) {
                    return array('authorized' => true, 'additionalFields' => array(), 'user' => $user, 'userId' => $user['id'], 'authToken' => $data['access-token']);
                }

            } else {
                $dataCreate['email'] = $dataCreate['login'] = $me['id'];
                $dataCreate['password'] = uniqid();
                $dataCreate['params']['active'] = 1;
                $dataCreate['params']['name'] = $me['name'];

                return $this->createUser($params, $dataCreate);
            }

        } else {

            return $this->error('login not provided', 400);

        }
    }


    private function generateJwtToken($userId)
    {
        $jwt = new JWT(xConfig::get('GLOBAL', 'jwtSecretKey'), 'HS256', xConfig::get('GLOBAL', 'jwtMaxAge'), 10);
        $token = $jwt->encode(['uid' => $userId, 'scopes' => ['user']]);
        header('Authorization: Bearer ' . $token);
        return $token;
    }


    /**
     * @SWG\Get(
     *     path="/logout",
     *     summary="Front user logout",
     *     operationId="logout",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *       type="string",
     *          name="Authorization",
     *       in="header",
     *      example="Bearer ",
     *     required=true),
     *     @SWG\Response(response=200, description="User logged out")
     * )
     */


    public function logout($params, $data)
    {
        $user = $this->_commonObj->getAuthorizedUser();
        if (!empty($user)) {
            return array('logout' => true);
        } else {
            return $this->error('User not authorized', 400);
        }
    }

    /**
     * @SWG\Post(
     *     path="/login",
     *     summary="Front user Login",
     *     operationId="Login",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="login",
     *         in="body",
     *         description="User login",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/loginData")
     *     ),
     *
     *     @SWG\Response(response=200, description="Auth data")
     * )
     */


    public function login($params, $data)
    {

        $authData = $this->_commonObj->checkAndLoadUser($data['login'], $data['password']);

        if ($authData['authorized']) {

            $token = $this->generateJwtToken($authData['user']['id']);

            unset($authData['user']['params']['password']);

            return array('authorized' => true, 'additionalFields' => array(), 'user' => $authData['user'], 'authToken' => $token);

        } else {

            if ($authData['error'] == 'user-not-defined') {

                return $this->error('User does not exists', 500);
            }

            return $this->error('Wrong credentials', 400);
        }


    }


    /**
     * @SWG\Post(
     *     path="/createUser",
     *     summary="Creates user account",
     *     operationId="createUser",
     *     produces={"application/json"},
     *    @SWG\Parameter(
     *         name="userObject",
     *         in="body",
     *         description="user object",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/userObject"),
     *     ),
     *     @SWG\Response(response=200, description="Registration result")
     * )
     */


    public function createUser($params, $data)
    {

        if (empty($data['userGroupId'])) {

            $userGroupId = $this->_commonObj->_tree->readNodeParam(1, 'defaultRegisteredGroup');

        } else {

            $userGroupId = $data['userGroupId'];
        }

        $checkedData = $this->_commonObj->checkUserLoginAndEmail($data['login'], $data['email']);

        if ((!$checkedData['isLogin']) && (!$checkedData['isEmail'])) {

            $data['params']['password'] = Common::passwordHash($data['password']);
            $data['params']['email'] = $data['email'];
            $data['params']['active'] = (bool)$data['params']['active'];
            $avatar = $data['params']['avatar'];
            unset($data['params']['avatar']);

            if ($userId = $this->_tree->initTreeObj($userGroupId, $data['login'], '_FUSER', $data['params'])) {

                if (!empty($data['additionalFields'])) {
                    $this->_commonObj->initAdditionalFields($userId, $data['additionalFields']);
                }

                $image = ImageHandler::handleImage($avatar, 'fusers/' . $userId . '/' . basename($avatar));
                $this->_tree->writeNodeParam($userId, 'avatar', $image);
            }

            $token = $this->generateJwtToken($userId);

            $userData = $this->_tree->selectStruct('*')->selectParams('*')->where(array('email', '=', $data['params']['email']))->singleResult()->run();

            return array('registered' => true, 'userId' => $userId, 'user' => $userData, 'authToken' => $token);

        } else {

            return $this->error('User account already exists', 400);

        }
    }


    /**
     * @SWG\Post(
     *     path="/saveAdditionalFields",
     *     summary="saves additional fields only",
     *     operationId="saveAdditionalFields",
     *     produces={"application/json"},
     *
     *   @SWG\Parameter(
     *       type="string",
     *       name="Authorization",
     *       in="header",
     *      example="Bearer ",
     *     required=true),
     *
     *    @SWG\Parameter(
     *         name="additionalFields",
     *         in="body",
     *         description="additionalFields object",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/additionalFields"),
     *     ),
     *     @SWG\Response(response=200, description="Registration result")
     * )
     */

    public function saveAdditionalFields($params, $data)
    {
        $user = $this->_commonObj->getAuthorizedUser();

        if (!empty($user)) {

            if (!empty($data)) {

                $this->_commonObj->saveAdditionalFields($user['id'], $data);

                return array('result' => true);

            }
        } else {
            return $this->error('User not logged', 400);
        }
    }


    /**
     * @SWG\Post(
     *     path="/saveUser",
     *     summary="saves user account",
     *     operationId="saveUser",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *       type="string",
     *          name="Authorization",
     *       in="header",
     *      example="Bearer ",
     *     required=true),
     *    @SWG\Parameter(
     *         name="saveUserObject",
     *         in="body",
     *         description="saveUserObject object",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/saveUserObject"),
     *     ),
     *     @SWG\Response(response=200, description="Registration result")
     * )
     */


    public function saveUser($params, $data)
    {
        $user = $this->_commonObj->getAuthorizedUser();

        if (empty($user)) {
            return $this->error('User not logged', 500);
        }


        if (!empty($data['newPassword']) && !empty($data['oldPassword'])) {
            if (Common::passwordVerify($data['oldPassword'], $user['params']['oldPassword'])){
                $data['params']['password'] = Common::passwordHash($data['newPassword']);
        }else{
                return $this->error('Wrong password', 402);
            }
        }

        if (!empty($data['params']['email'])) {
            unset($data['params']['email']);
        }

        if ($this->_tree->reInitTreeObj($user['id'], '%SAME%', $data['params'])) {

            if (!empty($data['additionalFields'])) {
                $this->_commonObj->saveAdditionalFields($user['id'], $data['additionalFields']);
            }

            return array('saved' => true);

        } else {

            return $this->error('Internal error', 400);
        }

    }

    /**
     * @SWG\Get(
     *     path="/getUser",
     *     summary="Gets user ",
     *     operationId="getUser",
     *     produces={"application/json"},
     *     @SWG\Response(response=200, description="gets user by id"),
     *     @SWG\Parameter(
     *       type="string",
     *          name="Authorization",
     *      example="Bearer ",
     *       in="header",
     *     required=true)
     * )
     */

    public function getUser($params)
    {

        $user = $this->_commonObj->getAuthorizedUser();

        if (!empty($user)) {
            unset($user['params']['password']);
            return $user;
        }

        return $this->error('user not authorized', 401);

    }


}
