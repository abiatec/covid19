<?php

use X4\Classes\XRegistry;


class showUserPriceCategoryAction extends xAction
{

    public $_props;

    public function __construct()
    {
        parent::__construct('fusers');
    }



    public function build($params)
    {
        $userPrices = [];

        if (xRegistry::get('fusersFront')->isUserAuthorized()) {

            $this->setupPriceCategory();

            $userData = xRegistry::get('fusersFront')->getAuthorizedUser();

            if (!empty($userData['params']['accessiblePrices'])) {

                $catalog = xCore::moduleFactory('catalog.front');

                $accessiblePrices = json_decode($userData['params']['accessiblePrices'], true);

                foreach ($accessiblePrices as $priceKey => $price) {

                    if ($price == false) {
                        continue;
                    }

                    $explKey = explode('.', $priceKey);

                    $pset = $catalog->_commonObj->_propertySetsTree->singleResult()->selectParams('*')->where(array('@ancestor', '=', 1), array('@basic', '=', $explKey[0]))->run();

                    if (!empty($pset)) {
                        $property = $catalog->_commonObj->_propertySetsTree->singleResult()->selectParams('*')->where(array('@ancestor', '=', $pset['id']), array('@basic', '=', $explKey[1]))->run();

                        if ($priceKey == xRegistry::get('SESSION')->get('userPriceCategory')) {
                            $selected = true;
                            xRegistry::get('SESSION')->set('userPriceCategoryAlias', $property['params']['alias']);

                        } else {
                            $selected = false;
                        }

                        $userPrices[$priceKey] = array('alias' => $property['params']['alias'], 'selected' => $selected);

                    }
                }
                $this->_props['userPrices'] = $userPrices;
            }
        }

    }

    private function setupPriceCategory()
    {
        if (isset($_GET['userPriceCategory'])) {

            xRegistry::get('SESSION')->set('userPriceCategory', $_GET['userPriceCategory']);
            XRegistry::get('EVM')->fire($this->_moduleName . '.setupPriceCategory:after', array('userPriceCategory' => $_SESSION['userPriceCategory']));

        } elseif (empty(xRegistry::get('SESSION')->get('userPriceCategory'))) {

            xRegistry::get('SESSION')->set('userPriceCategory', $_GET['userPriceCategory']);
            $userData = xRegistry::get('fusersFront')->getAuthorizedUser();
            xRegistry::get('SESSION')->set('userPriceCategory', $userData['params']['defaultPrice']);

        }
    }



    public function run($params)
    {
        $this->loadModuleTemplate($params['params']['Template']);
        $this->_TMS->addMassReplace('showUserPriceCategory', $this->_props);
        return $this->_TMS->parseSection('showUserPriceCategory');
    }

}
