<?php

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class usersSetSuperAdminCommand extends Command
{
    protected function configure()
    {
        $this->setName('users:setSuperAdmin')
            ->setDescription('Sets superAdmin user')
            ->setHelp('Use  users:setSuperAdmin --login={name} --password={password}')
            ->addOption('login', null, InputOption::VALUE_REQUIRED, 'User login', 0)
            ->addOption('password', null, InputOption::VALUE_REQUIRED, 'User password', 0);
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $node = $this->_commonObj->_tree->selectStruct('*')->where(['@obj_type', '=', '_SUPERADMIN'])->singleResult()->jsonDecode()->run();
        if (!empty($node)) {
            $login = $input->getOption('login');
            $password = $input->getOption('password');
            if ($login && $password) {
                $this->_commonObj->_tree->reInitTreeObj($node['id'], $login, ['password' => Common::passwordHash($password)], '_SUPERADMIN');
                $output->writeln(['SuperAdmin data set success.', '', '',]);
            }
        }

    }
}








