<?php

class usersBack extends xModuleBack
{
    use _USER, _USERSGROUP;

    public function __construct()
    {
        parent::__construct(__CLASS__);
    }


    public function onSearchInModule($params)
    {
        $params['word'] = urldecode($params['word']);

        $resultBasic = $this->_tree->selectParams(array('name', 'surname', 'email'))->selectStruct(array
        (
            'id',
            'basic',
            'obj_type'
        ))->where(array
        (
            'email',
            'LIKE',
            '%' . $params['word'] . '%'
        ))->format('keyval', 'id')->run();


        $resultLogin = $this->_tree->selectParams(array('name', 'surname', 'email'))->selectStruct(array
        (
            'id',
            'basic',
            'obj_type'
        ))->where(array
        (
            '@basic',
            'LIKE',
            '%' . $params['word'] . '%'
        ))->format('keyval', 'id')->run();

        $resultName = $this->_tree->selectParams(array('name', 'surname', 'email'))->selectStruct(array
        (
            'id',
            'basic',
            'obj_type'
        ))->where(array
        (
            'surname',
            'LIKE',
            '%' . $params['word'] . '%'
        ))->format('keyval', 'id')->run();

        XARRAY::arrayMergePlus($resultBasic, $resultName, true);
        XARRAY::arrayMergePlus($resultBasic, $resultLogin, true);

        $this->result['searchResult'] = Common::gridFormatFromTree($resultBasic, array
        (
            'id',
            'obj_type',
            'basic',
            'name',
            'surname',
            'email'

        ));

    }


    public function deleteUser($params)
    {
        $this->deleteObj($params, $this->_tree);
    }

    public function getGroups()
    {
        $groups = $this->_tree->selectStruct(array('id', 'basic'))->selectParams('*')->childs(1, 1)->run();
        return $this->result['groups'] = XARRAY::arrToLev($groups, 'id', 'params', 'Name');
    }


    public function savePermissionsMatrix($params)
    {

        if (!empty($params['form'])) {
            foreach ($params['form'] as $key => $permission) {
                $this->_commonObj->setPermission($key, $params['module'], (int)$params['objId'], $permission);
            }

        }

    }

    public function getObjectPermissionsMatrix($params)
    {
        $objPermissions = $this->_commonObj->rolesTree->selectAll()->where(array('@basic', '=', $params['objId'], 'module' => $params['module']))->run();
        if (!empty($objPermissions)) {
            $itemExt = [];
            foreach ($objPermissions as $item) {
                $basic = $this->_commonObj->rolesTree->getNodeStruct($item['path'][1]);
                $itemExt[$basic['basic'] . '.read'] = $item['params']['read'];
            }
        }

        $this->result['permissionsMatrix'] = $itemExt;

    }

    public function getObjectPermissions($params)
    {

        $permissions = $this->_commonObj->getPermissions();

        if (!empty($permissions)) {
            $user = $this->_commonObj->getUser();
            $objectId = (int)$params['objId'];
            $node = $this->_commonObj->rolesTree->selectStruct('*')->singleResult()->where(array('@basic', '=', $user['ancestor']))->run();

            $modules = $permissions->fetchArray($node['id']);

            if (!empty($modules)) {
                foreach ($modules as $module) {
                    if ($module['basic'] == $params['module']) {
                        $permissions = $permissions->fetchArray($module['id']);
                        foreach ($permissions as $permission) {
                            if ($permission['basic'] == $objectId) {
                                return $permission['params'];
                            }
                        }
                    }
                }

            }
        }
    }


    public function treeDynamicXLSUsers($params)
    {

        $source = new X4\Classes\TreeJsonSource($this->_tree);
        $connectAttributes = function ($set) {
            if (!isset($set['Name'])) {
                $set['Name'] = $set['name'] . ' [' . $set['surname'] . ' ' . $set['email'] . ']';
            }
            return $set;
        };


        $options = array
        (
            'imagesIcon' => array('_USER' => 'leaf.gif', '_USERSGROUP' => 'folder.gif'),
            'gridFormat' => true,
            'onRecord' => $connectAttributes,
            'showNodesWithObjType' => array
            (
                '_ROOT',
                '_USERSGROUP',
                '_USER'
            ),
            'columns' => array('>Name' => array(), '>name' => array(), '>surname' => array(), '>email' => array())

        );

        $source->setOptions($options);
        $this->result = $source->createView($params['id'], $params['page']);

    }


    public function treeDynamicXLS($params)
    {

        $source = new X4\Classes\TreeJsonSource($this->_tree);
        $options = array
        (
            'imagesIcon' => array('_USERSGROUP' => 'folder.gif'),
            'gridFormat' => true,
            'showNodesWithObjType' => array
            (
                '_ROOT',
                '_USERSGROUP'
            ),
            'columns' => array('>Name' => array())
        );

        $source->setOptions($options);
        $this->result = $source->createView($params['id'], $params['page']);
    }

}