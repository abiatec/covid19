<?php

use X4\Classes\XRegistry;

class usersCommon extends xModuleCommon implements xCommonInterface
{
    public $_useTree = true;

    public function __construct()
    {
        parent::__construct(__CLASS__);

        $this->_tree->setObject('_ROOT', array('LastModified'));

        $this->_tree->setObject('_USERSGROUP', array
        (
            'Name'

        ), array('_ROOT'));

        $this->_tree->setObject('_SUPERADMIN', array
        (
            'password',
            'name',
            'email',
            'surname',
            'patronymic',
            'phone',
            'avatar',
            'active'
        ), array('_ROOT'));

        $this->_tree->setObject('_USER', array
        (
            'password',
            'name',
            'surname',
            'patronymic',
            'phone',
            'avatar',
            'active',
            'email',
            'permissions',
            'orderTypeAccess'

        ), array('_USERSGROUP'));


        $this->rolesTree = new X4\Classes\XTreeEngine('usersroles_container', XRegistry::get('XPDO'));

        $this->rolesTree->setObject('_ROOT', array('LastModified'));

        $this->rolesTree->setObject('_ROLE', array('Name'), array('_ROOT'));

        $this->rolesTree->setObject('_MODULE', array('is_accesible'), array('_ROLE'));

        $this->rolesTree->setObject('_PERMISSION', array('module', 'objId', 'read', 'write', 'delete', 'deep'), array('_MODULE'));

        $this->rv = array('update' => 1, 'delete' => 2, 'add' => 4, 'read' => 8);


    }

    public function getPermissions()
    {

        $user = $this->getUser();

        $node = $this->rolesTree->selectStruct('*')->singleResult()->showNodeChanged(false)->where(array('@basic', '=', $user['ancestor']))->run();

        if (!empty($node)) {

            $tree = $this->rolesTree->selectAll()->childs($node['id'])->asTree()->run();

            return $tree;
        } else {
            return null;
        }

    }


    public function setPermission($ancestor, $module, $objId, $permissionArray)
    {

        $id = $this->rolesTree->initTreeObj(1, $ancestor, '_ROLE', $ancestor);

        if (empty($id)) {
            $id = $this->rolesTree->lastNonUniqId;
        }

        $id = $this->rolesTree->initTreeObj($id, $module, '_MODULE', $module);

        if (empty($id)) {
            $id = $this->rolesTree->lastNonUniqId;
        }

        $permissionArray['module'] = $module;

        $id = $this->rolesTree->initTreeObj($id, $objId, '_PERMISSION', $permissionArray);

        if (empty($id)) {

            $this->rolesTree->reInitTreeObj($this->rolesTree->lastNonUniqId, $objId, $permissionArray, '_PERMISSION');
        }

    }


    public function getUser($userId = null)
    {
        static $users;

        if (!$userId) {
            $userId = $_SESSION['user']['id'];
        }

        if (!empty($users[$userId])) {
            return $users[$userId];
        }

        return $users[$userId] = $this->_tree->getNodeInfo($userId);

    }

    public function loadRoles($node)
    {
        return $node['params']['permissions'];
    }


    public function defineFrontActions()
    {

    }

    public function checkAndLoadUser($login, $password)
    {
        $user = $this->_tree->selectParams('*')->selectStruct('*')->where(array('active', '=', 1), array('@basic', '=', $login), array('@obj_type', '=', array('_USER', '_SUPERADMIN')))->singleResult()->jsonDecode()->run();

        if (!empty($user)) {
            if (Common::passwordVerify($password,$user['params']['password'])) {

                $_SESSION['user'] = array('id' => $user['id'], 'login' => $user['basic'], 'type' => $user['obj_type'], 'email' => $user['params']['email']);
                $_SESSION['authcode'] = md5($user['params']['email'] . $user['obj_type']);

                if ($user['obj_type'] != '_SUPERADMIN') {
                    $_SESSION['user']['moduleAccess'] = $this->loadRoles($user);
                    $_SESSION['user']['permissions'] = $this->getPermissions();
                    $_SESSION['user']['orderTypeAccess'] = $user['params']['orderTypeAccess'];
                }
                return true;
            }

        } else {
            return false;
        }

    }
}


