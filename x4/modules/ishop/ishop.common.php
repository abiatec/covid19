<?php

use X4\Classes\XPDO;

class ishopCommon extends xModuleCommon implements xCommonInterface
{
    public $_useTree = true;

    public function __construct()
    {
        parent::__construct(__CLASS__);

        $this->_tree->setLevels(5);

        $this->_tree->setObject('_ROOT', array(
            'Name'
        ));

        $this->_tree->setObject('_TUNESBRANCH', array(
            'Name'
        ), array(
            '_ROOT'
        ));

        $this->_tree->setObject('_STORE', null, array(
            '_TUNESBRANCH'
        ));

        $this->_tree->setObject('_DELIVERY', null, array(
            '_TUNESBRANCH'
        ));

        $this->_tree->setObject('_PAYSYSTEM', null, array(
            '_TUNESBRANCH'
        ));

        $this->_tree->setObject('_STATUS', null, array(
            '_TUNESBRANCH'
        ));

        $this->_tree->setObject('_TUNES', null, array(
            '_TUNESBRANCH'
        ));

        $this->_tree->setObject('_CURRENCY', null, array(
            '_TUNESBRANCH'
        ));

        if (xConfig::get('GLOBAL', 'currentMode') == 'front') {

            $this->_tree->cacheState(true);
        }

    }

    public function getBranchId($branch)
    {
        $result = $this->_tree->selectStruct('*')->where(array(
            '@basic',
            '=',
            $branch
        ))->run();

        return $result[0]['id'];
    }

    public function createTunesBranch($branchName)
    {
        $id = $this->getBranchId($branchName);
        if (!$id) {
            $id = $this->_tree->initTreeObj(1, $branchName, '_TUNESBRANCH');
        }

        return $id;
    }


    public function proccessOrderSum($orderData)
    {
        $result['goodsTotalSum'] = $orderData['total_sum'];

        if (!empty($orderData['delivery_price'])) {
            $result['orderTotalSumWithoutDiscount'] = $result['orderTotalSum'] = $orderData['delivery_price'] + $orderData['total_sum'];
        } else {
            $result['orderTotalSum'] = $orderData['total_sum'];
        }

        if (!empty($orderData['discount_sum'])) {
            $result['orderTotalSum'] -= $orderData['discount_sum'];
            $result['orderTotalSumWithDiscount'] = $result['goodsTotalSum'] - $orderData['discount_sum'];
        }

        return $result;

    }


    public function getTunes()
    {
        if ($tunes = $this->_tree->selectParams('*')->where(array(
            '@obj_type',
            '=',
            '_TUNES'
        ), array(
            '@basic',
            '=',
            'tunesObject'
        ))->singleResult()->run()
        ) {
            return $tunes['params'];
        }
    }


    public function setupMainCurrency()
    {
        if ($_GET['setCurrentCurrency']) {
            $_SESSION['currency'] = $this->_models->Currencies->getMainCurrency($_GET['setCurrentCurrency']);
        }

        if (!isset($_SESSION['currency'])){
            
            $_SESSION['currency'] = $this->_models->Currencies->getMainCurrency();
            
        }
    }

    public function getCurrentCurrency()
    {
        if (isset($_SESSION['currency'])) {

            $mainCurrency[$_SESSION['currency']['id']] = $_SESSION['currency']['params'];

        } else {
            $mainCurrency = $this->_tree->selectStruct(array(
                'id'
            ))->selectParams('*')->where(array(
                '@obj_type',
                '=',
                '_CURRENCY'
            ), array(
                'isMain',
                '=',
                '1'
            ))->format('valval', 'id', 'params')->run();
            if (!$mainCurrency) {
                throw new Exception('main-currency-is-not-set-in-ishop-module;');
            }
        }
        return $mainCurrency;
    }


    public function getOrderData($id)
    {
        $order = XPDO::selectIN('*', 'ishop_orders', "id=" . $id . "");
        return $order[0];
    }


    public function getOrderDataByHash($hash)
    {
        $order = XPDO::selectIN('*', 'ishop_orders', "hash='" . $hash . "'");
        return $order[0];
    }


    public function getRegisteredClientsRange($clients)
    {
        $fusers = xCore::loadCommonClass('fusers');
        if ($clients = array_filter($clients)) {
            return $clients = $fusers->_tree->selectParams('*')->where(array(
                '@id',
                '=',
                $clients
            ))->run();

        }

    }

    public function getGuestCustomer($id)
    {
        if (!empty($id)) {
            if ($user = XPDO::selectIN('*', 'ishop_orders_clients_guest', (int)$id)) {
                return $user[0];
            }
        }

    }


    public function getGoodOrders($orderId)
    {
        if ($goods = XPDO::selectIN('*', 'ishop_orders_goods', 'order_id=' . $orderId)) {
            foreach ($goods as &$good) {
                if (!empty($good['sku_serialized'])) {
                    $good['skuObject'] = unserialize($good['sku_serialized']);
                    unset($good['sku_serialized']);
                }
                $good['priceSum'] = $good['price'] * $good['count'];
            }
            return $goods;
        }


    }



    static function getOrderLeadData($orderID)
    {
        $orderLead = XPDO::selectIN('*', 'ishop_orders_lead', "order_id='" . $orderID . "'");

        if(!empty($orderLead[0]))
        {
            return $orderLead[0];
        }
    }

    public function getOrderByPaysystem($id)
    {
        $order = XPDO::selectIN('*', 'ishop_orders', "paysystem_order_num='" . $id . "'");

        if (!empty($order)) {

            if ($order[0]['client_guest_id']) {
                $user = $this->getGuestCustomer($order[0]['client_guest_id']);
            }

            $order[0]['client'] = $user;
            $order[0]['orderSums'] = $this->proccessOrderSum($order[0]);
            $order[0]['deliveryData'] = $this->getDeliveryData($order[0]['delivery_id']);

            return $order[0];
        }

        return false;
    }


    public function getGuestClientsRange($range)
    {
        return XPDO::selectIN('*', 'ishop_orders_clients_guest', $range);
    }


    public function getOrderGoods($orderId)
    {
        if (!empty($orderId)) {
            $ishopGoods = XPDO::selectIN('*', 'ishop_orders_goods', ' order_id = ' . $orderId);
            if (!empty($ishopGoods)) {
                foreach ($ishopGoods as $k => &$v) {
                    if ($v['sku_serialized']) {
                        $v['sku'] = unserialize($v['sku_serialized']);
                        $v['sum'] = $v['price'] * $v['count'];
                        $ishopGoods[$k] = $v;
                    }
                }

                return $ishopGoods;
            }
        }
    }

    public function defineFrontActions()
    {
        $this->defineAction('showBasketStatus');
        $this->defineAction('showCurrencyList');
        $this->defineAction('showBasket', array(
            'serverActions' => array(
                'order',
                'remove',
                'removeall',
                'cart',
                'addtocart',
                'paymentSubmit',
                'paymentSuccess',
                'paymentFail',
                'submitOrder',
                'finishEditedOrder',
                'finishEditedOrderSubmit'
            )
        ));
    }
}
