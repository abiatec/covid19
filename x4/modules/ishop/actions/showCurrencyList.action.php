<?php

class showCurrencyListAction extends xAction
{
    public $_props;

    public function build($params)
    {
        $this->_props['currenciesList'] = $this->_commonObj->_models->Currencies->getCurrenciesList(true);

        if (!empty($this->_props['currenciesList'])) {

            if (isset($_SESSION['currency']['basic'])) {
                $selected = $_SESSION['currency']['basic'];
            }
            unset($params['request']['getData']['setCurrentCurrency']);

            $z = http_build_query($params['request']['getData']);
            if ($z) {
                $z = '&' . $z;
            }

            foreach ($this->_props['currenciesList'] as $key => &$currency) {

                if (!$currency['showOnFront']) {
                    unset($this->_props['currenciesList'][$key]);
                    continue;
                }

                if ($currency['currencyId'] == $selected) {
                    $currency['selected'] = true;
                }

                $url = xConfig::get('PATH', 'fullBaseUrl');
                $currency['link'] = CHOST . $params['request']['pageLink'] . $params['request']['requestActionPath'] . '/?setCurrentCurrency=' . $currency['currencyId'] . $z;
            }

        }

    }

    public function runHeadless($params)
    {
        return $this->_props;
    }

    public function run($params)
    {
        $this->loadModuleTemplate($params['params']['Template']);

        $this->_TMS->addReplace('showCurrencyList', 'currencies', $this->_props['currencies']);

        return $this->_TMS->parseSection('showCurrencyList');

    }


}
