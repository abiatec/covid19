<?php

class showBasketStatusAction extends xAction
{
    public $_props;

    public function build($params)
    {
        $pages = xCore::loadCommonClass('pages');
        $this->_props['cartPageLink'] = $pages->createPagePath($params['params']['basketPage']);
        $this->_props['order'] = $this->_calculateOrder();
        $this->_props['objects'] = $this->cartStorage->get();

    }

    public function runHeadless($params)
    {
        return $this->_props;
    }

    public function run($params)
    {
        $this->loadModuleTemplate($params['params']['Template']);
        $this->_TMS->addMassReplace('showBasketStatus',
            array('cartPageLink' => $this->_props['cartPageLink'],
                'objects' => $this->_props['order']));

        $this->_TMS->addMassReplace('showBasketStatus', $this->_props['order']);
        return $this->_TMS->parseSection('showBasketStatus');

    }


}
