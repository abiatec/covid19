<?php

namespace X4\Modules\ishop\models;

class Stock extends \xModuleCommonModel
{

    public function __construct($commonObj)
    {
        parent::__construct($commonObj);
    }


    public function getStockData($objId){
        $PDO = xRegistry::get('XPDO');
        $query = $PDO->prepare("select store_id,items from `ishop_stock_items` where obj_id=?");
        $pdoResult = $query->execute([$objId]);
        $result = [];

        while ($row = $pdoResult->fetch(\PDO::FETCH_ASSOC)) {
            $result[$row['store_id']] = $row['items'];
        }

        return $result;
    }

    public function deleteStock($id)
    {
        $this->_tree->delete()->where(['@id', '=', $id])->run();
    }

    public function saveStock($data)
    {
        $ancestor = $this->_commonObj->createTunesBranch('STORE');
        $basic = $data['storeId'];
        unset($data['storeId']);

        if ($this->_tree->initTreeObj($ancestor, $basic, '_STORE', $data)) {
            return true;
        }
    }


    public function saveEditedStock($data)
    {
        $basic = $data['data']['storeId'];
        unset($data['data']['storeId']);

        if ($this->_tree->reInitTreeObj($data['id'], $basic, $data['data'], '_STORE')) {
            return true;
        }
    }


    /**
     * Get stock list saved in  STORE branch
     * @return array
     */

    public function getStocksList($getStockBy = null, $sign = '=')
    {

        $id = $this->_commonObj->getBranchId('STORE');

        if (!empty($id)) {

            $items = $this->_tree->selectStruct('*')->selectParams('*')->childs($id, 1);

            if (!empty($getStockBy)) {

                foreach ($getStockBy as $criteria => $value) {
                    $items->addWhere([$criteria, $sign, $value]);
                }

            }

            return $items->run();

        } else {
            return [];
        }

    }

    /**
     * Get aggregated stock data from stock table
     * @param $objId
     * @return array
     */

    public function getStockAgregated($objId)
    {
        $items = $this->getStocksList();

        if (!empty($items)) {

            $stockData = $this->getStockData($objId);

            foreach ($items as $item) {
                $outItem['stockValue'] = $stockData[$item['storeId']] ? $stockData[$item['storeId']] : 0;
                $outItem['innerId'] = $item['id'];
                $outItem['stockId'] = $item['basic'];
                $outItem['stockName'] = $item['params']['Name'];
                $outItem['stockAddress'] = $item['storeAddress'];
                $stockState[$item['basic']] = $outItem;
            }

        }

        return $stockState;
    }

    /**
     * Set stock array data into table
     * @param $objId
     * @param $stockData
     * @param string $itemsType
     */

    public function setStockData($objId, $stockData, $itemsType = 'f')
    {
        $currentStock = $this->getStockData($objId);

        if (!empty($currentStock)) {
            foreach ($currentStock as $stockItem) {
                if (!empty($stockData[$stockItem['store_id']])) {
                    $updateStock[$stockItem['store_id']] = $stockData[$stockItem['store_id']];
                }
            }

            if (!empty($updateStock)) {
                $insertStock = array_diff_key($stockData, $updateStock);
            } else {
                $insertStock = $stockData;
            }


        }

        if (!empty($updateStock)) {
            foreach ($updateStock as $stockId => $value) {
                $express = ' store_id=' . $stockId . ' and obj_id=' . $objId;
                XPDO::updateIN('ishop_stock_items', $express, array('items_type' => $itemsType, 'items' => $value));

            }
        }


        if (!empty($insertStock)) {
            $stockArray = [];
            foreach ($insertStock as $stockId => $value) {
                $stockArray[] = array('id' => 'NULL', 'store_id' => $stockId, 'obj_id' => $objId, 'items_type' => $itemsType, 'items' => $value);
            }

            XPDO::multiInsertIN($stockArray);
        }


    }


}
