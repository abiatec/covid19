<?php

namespace X4\Modules\ishop\models;

class PaymentSystem extends \xModuleCommonModel
{

    public function __construct($commonObj)
    {
        parent::__construct($commonObj);
    }


    public function setOrderPaysystem($id, $system)
    {
        XPDO::updateIN('ishop_orders', (int)$id, array(
            'paysystem' => $system
        ));
    }

    public function getPaysystemsList()
    {
        $ancestor = $this->_commonObj->createTunesBranch('PAYSYSTEM');

        return $data = $this->_tree->selectStruct('*')->selectParams('*')->where([
            '@ancestor',
            '=',
            $ancestor
        ])->run();

    }

    public function getPaysystemData($basic)
    {
        $ancestor = $this->_commonObj->createTunesBranch('PAYSYSTEM');

        return $data = $this->_tree->selectStruct('*')->selectParams('*')->where(array(
            '@ancestor',
            '=',
            $ancestor
        ), array(
            '@basic',
            '=',
            $basic
        ))->singleResult()->run();
    }

    public function getPaySystemName($paySystemName)
    {
        static $paySystems;

        if (!$paySystems) {

            $paysystemPath = \xConfig::get('PATH', 'MODULES') . 'ishop/paysystems/';

            $systems = \XFILES::filesList(\xConfig::get('PATH', 'MODULES') . 'ishop/paysystems/', $types = 'directories');

            if (!empty($systems)) {
                foreach ($systems as $system) {
                    $file = $paysystemPath . $system . '/' . $system . '.paysystem.html';
                    $paySystems[$system . '.paysystem.html'] = \xModuleBack::getTemplateAlias($file);
                }
            }

        }

        return $paySystems[$paySystemName . '.paysystem.html'];
    }


}
