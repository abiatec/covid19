<?php

namespace X4\Modules\ishop\models;
use X4\Classes\XPDO;

class SaleStats extends \xModuleCommonModel
{

    public function __construct($commonObj)
    {
        parent::__construct($commonObj);
    }

    public function getMostBuyingProducts($basic = null)
    {
        $sales = XPDO::selectIN('name,SUM(count) as ccount', 'ishop_orders_goods', '', 'group by cat_id ORDER BY ccount desc LIMIT 100');
        return $sales;
    }


}
