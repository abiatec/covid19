<?php

namespace X4\Modules\ishop\models;

class Currencies extends \xModuleCommonModel
{

    public function __construct($commonObj)
    {
        parent::__construct($commonObj);
    }

    public function getMainCurrency($basic = null)
    {

        $currencySelector = $this->_tree->selectStruct('*')->selectParams('*');

        if (!$basic) {
            return $currencySelector->where(array(
                '@obj_type',
                '=',
                '_CURRENCY'
            ), array(
                'isMain',
                '=',
                '1'
            ))->singleResult()->run();

        } else {

            return $currencySelector->where(array(
                '@obj_type',
                '=',
                '_CURRENCY'
            ), array(
                '@basic',
                '=',
                $basic
            ))->singleResult()->run();
        }

    }


    public function getCurrenciesList($getAllData = false, &$mainCurrency = false)
    {

        $currInstance = $this->_tree->selectStruct('*')->selectParams('*')->where(array(
            '@obj_type',
            '=',
            '_CURRENCY'
        ));

        if ($getAllData) {
            $currInstance->format('keyval', 'id');

        } else {
            $currInstance->format('valval', 'id', 'basic');
        }

        if ($currency = $currInstance->run()) {

            foreach ($currency as $key => $cur) {
                if (!empty($cur['isMain']) && $mainCurrency) {
                    $mainCurrency = $cur;
                }

                if (!empty($cur['basic']))
                    $basic = $cur['basic'];

                if (!$getAllData) {
                    $extCurrency[$key] = $cur;
                } else {
                    $extCurrency[$key] = $cur['params'];
                    $extCurrency[$key]['currencyId'] = $basic;
                }
            }


            return $extCurrency;
        }
    }


}
