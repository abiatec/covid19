<?php

namespace X4\Modules\ishop\models;

class OrderStatus extends \xModuleCommonModel
{

    public function __construct($commonObj)
    {
        parent::__construct($commonObj);
    }


    public function getOrderStatus($id, $status)
    {
        $order = XPDO::selectIN(array('status'), 'ishop_orders', "id=" . (int)$id . "");
        return $order[0];
    }

    public function setOrderStatus($id, $status)
    {
        XPDO::updateIN('ishop_orders', (int)$id, array(
            'status' => $status
        ));
    }

    public function getStatusesList($getFullData = false)
    {
        $list = $this->_tree->selectStruct('*')->selectParams('*')->where(array(
            '@obj_type',
            '=',
            '_STATUS'
        ));

        if (!$getFullData) {
            $list->format('valval', 'id', 'basic');
        }

        if ($statuses = $list->run()) {
            return $statuses;
        }
    }


}
