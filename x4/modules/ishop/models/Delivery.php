<?php

namespace X4\Modules\ishop\models;

class Delivery extends \xModuleCommonModel
{

    public function __construct($commonObj)
    {
        parent::__construct($commonObj);
    }


    public function getDeliveryData($basic = null)
    {
        $ancestor = $this->_commonObj->getBranchId('DELIVERY');

        if ($delivery = $this->_tree->selectStruct('*')->selectParams('*')->where(array(
            '@ancestor',
            '=',
            $ancestor
        ), array(
            '@basic',
            '=',
            $basic
        ))->singleResult()->run()
        ) {
            return $delivery;
        }
    }


    public function getDeliveryList()
    {
        $id = $this->_commonObj->getBranchId('DELIVERY');

        if ($deliveryList = $this->_tree->selectStruct('*')->selectParams('*')->childs($id)->format('keyval', 'basic', 'params')->run()) {
            return $deliveryList;
        }

    }


}
