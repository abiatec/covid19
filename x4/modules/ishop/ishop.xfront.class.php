<?php

class ishopXfront extends ishopFront
{


    public function addProductToCart($params)
    {
       
        $this->addToCart($params['id'], $params['count'], $params['isSku'], $params['extendedData']);
        $this->result['cart'] = $this->_calculateOrder();
        
        
    }

    public function getCartInfo($params)
    {
        $this->result['cartInfo'] = $this->_calculateOrder();

    }


    public function getCart($params)
    {
        $cart = $this->cartStorage->get();

        if ($cart) {
            $this->result['cartItems'] = $cart;

        }
    }

    public function getLastCartItems($params)
    {
        if (isset($_SESSION['ishop']['lastAdded'])) {
            $this->result['cartItems'] = $_SESSION['ishop']['lastAdded'];

        } else {

            $this->result['cartItems'] = [];
        }
    }

    public function calculateCartWithDelivery($params)
    {
        $this->result['delivery'] = $this->calculateDelivery($params['id']);

    }

    public function setOrderData($params)
    {

        if (!isset($_SESSION['siteuser']['orderData'])) {
            $_SESSION['siteuser']['orderData'] = [];
        }

        $_SESSION['siteuser']['orderData'] = array_merge($_SESSION['siteuser']['orderData'], $params['data']);

    }


    public function getCurrentCurrency()
    {
        $this->result['currency'] = $_SESSION['currency'];
    }


    public function submitOrderAsync($params)
    {
        $this->result['orderSubmited'] = true;
    }

    public function removeById($params)
    {
        if(!empty($params['id'])){
            $removeId = $params['id'];
            if (isset($this->cartStorage[$removeId])) {
                unset ($this->cartStorage[$removeId]);
            }
            $owners = [];
            foreach ($this->cartStorage as $id=>$item){
                $owner =$item['skuObject']['params']['owner'];
                $owners[$owner][] =$id;
            }
            $res = array('allCount'=>count($this->cartStorage));
            if(!empty($params['ownerId'])){
                if(count($owners[$params['ownerId']])==0){
                    $res['deletedOwnerOrder'] = true;
                }else{
                    $res['deletedOwnerOrder'] = false;
                }
            }else{
                $res['deletedOwnerOrder'] = false;
            }
            $this->result['data'] = $res;
        }

    }


    public function getInfoItemInCart($params)
    {
        if(!empty($params['id'])){


            $this->result['data'] = $this->cartStorage[$params['id']];
        }

    }



}


