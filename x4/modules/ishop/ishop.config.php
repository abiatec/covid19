<?php

xConfig::pushConfig(array(

    'iconClass' => 'i-cart',
    'actionable' => 1,
    'admSortIndex' => 60,
    'goodsOnPage'=>500,
    'cartStorage' => 'cartSessionStorage',
    'orderTypes'=>array(    
        'test1'=>'Test 1',
        'test2'=>'Test 2'
    )

));