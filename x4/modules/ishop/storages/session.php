<?php


class cartSessionStorage implements ArrayAccess, Iterator, Countable
{

    public $_container = [];
    public $_position = 0;

    public function __construct()
    {
        $this->_container =& $_SESSION['siteuser']['cart'];
    }


    public function clear()
    {
        unset($this->_container);
        unset($_SESSION['siteuser']['cart']);
        $this->_container =& $_SESSION['siteuser']['cart'];
    }

    public function count()
    {
        return count($this->_container);
    }

    public function get()
    {
        return $this->_container;
    }


    public function offsetExists($offset)
    {
        return isset($this->_container[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->offsetExists($offset) ? $this->_container[$offset] : null;
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->_container[] = $value;
        } else {
            $this->_container[$offset] = $value;
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->_container[$offset]);
    }


    public function rewind()
    {
        reset($this->_container);
    }

    public function current()
    {
        return current($this->_container);
    }

    public function key()
    {
        return key($this->_container);
    }

    public function next()
    {
        next($this->_container);
    }

    public function valid()
    {
        return key($this->_container) !== null;
    }

}

