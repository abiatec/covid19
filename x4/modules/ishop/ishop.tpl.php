<?php

class ishopTpl extends xTpl implements xModuleTpl
{

    public function getDeliveryList()
    {
        return $this->_commonObj->_models->Delivery->getDeliveryList();
    }


    public function getCurrenciesList($params)
    {
        return $this->_commonObj->_models->Currencies->getCurrenciesList(true);

    }

    public function getStocksList()
    {
        return $this->_commonObj->_models->Stock->getStocksList();
    }

    public function toCurrency($params)
    {
        if (!empty($params['to']) && !empty($params['from'])) {
            $currencies = $this->_commonObj->_models->Currencies->getCurrenciesList(true);

            $currenciesMap = XARRAY::asKeyVal($currencies, 'currencyId');
            $currenciesMap = array_flip($currenciesMap);
            $to = $currenciesMap[$params['to']];
            $from = $currenciesMap[$params['from']];
            return ($currencies[$to]['rate'] / $currencies[$from]['rate']) * $params['value'];

        }
    }

    public function getCart($params)
    {
        return $this->cartStorage->get();
    }

    public function getPaysystemsList()
    {
        $ancestor = $this->_commonObj->getBranchId('PAYSYSTEM');
        $data = $this->_tree->selectStruct('*')->selectParams('*')->where(array('@ancestor', '=', $ancestor), array('active', '=', 1))->sortby('priority', 'desc')->run();
        return $data;
    }


    public function calculateCart($params)
    {
        return $this->calculateCart();
    }

    public function getCurrencyById($params)
    {
        if (!empty($params['id'])) {
            $currency = $this->_tree->selectStruct('*')->selectParams('*')->where(array('@obj_type', '=', '_CURRENCY'), array('@id', '=', $params['id']))->singleResult()->run();
            return $currency;
        }
    }

    public function getDeliveryByBasic($params)
    {
        if (!empty($params['basic'])) {
            $delivery = $this->_tree->selectStruct('*')->selectParams('*')->where(array('@obj_type', '=', '_DELIVERY'), array('@basic', '=', $params['basic']))->singleResult()->run();
            return $delivery;
        }
    }

    public function getCurrentCurrency()
    {
        return $_SESSION['currency'];
    }

    public function getUserOrderGoods($params)
    {
        return $this->_commonObj->getOrderGoods($params['id']);
    }


    public function transformToCurrencyFormat($params)
    {
        if (!empty($params['toMain'])) {
            $currency = $this->_commonObj->getCurrentCurrency();
            $currency = current($currency);
            $decimals = $params['decimals'] ? $params['decimals'] : $currency['divider'];
            $decPoint = $currency['separator'];
        } else {
            $decimals = !empty($params['decimals']) ? $params['decimals'] : 0;
            $decPoint = !empty($params['decPoint']) ? $params['decPoint'] : '.';
        }

        $thousandsSep = !empty($params['thousandsSep']) ? $params['thousandsSep'] : ' ';

        return number_format($params['value'], $decimals, $decPoint, $thousandsSep);
    }


    public function incart($params)
    {
        return !empty($_SESSION['siteuser']['cart'][$params['id']]);

    }

    public function getPaySystemName($params)
    {
        if (!empty($params['basic']) && $paySystemName = $this->_commonObj->_models->PaymentSystem->getPaySystemName(trim($params['basic']))) {
            return $paySystemName;
        }
    }


}
