<?php

trait _STORE
{

    public function deleteStore($params)
    {
        $this->deleteObj($params, $this->_tree);
    }


    public function storeList($params)
    {

        $source = new X4\Classes\TreeJsonSource(
            $this->_tree
        );

        $opt = array(
            'showNodesWithObjType' => array(
                '_STORE'
            ),
            'columns' => array(

                'id' => array(),
                '>Name' => array(),
                'basic' => array(),
                '>storeAddress' => array(),
                '>storeOwner' => array()
            )
        );
        $source->setOptions($opt);
        $id = $this->_commonObj->getBranchId('STORE');
        $this->result = $source->createView($id);
    }


    public function onSave_STORE($params)
    {
        if ($this->_models->Stock->saveStock($params['data'])) {
            $this->pushMessage('store-saved');
        }
    }

    public function onSaveEdited_STORE($params)
    {

        if ($this->_models->Stock->saveEditedStock($params)) {
            $this->pushMessage('store-saved');
        }

    }

    public function onEdit_STORE($params)
    {
        $node = $this->_tree->getNodeInfo($params['id']);
        $node['params']['storeId'] = $node['basic'];
        $this->result['data'] = $node['params'];

    }


}
