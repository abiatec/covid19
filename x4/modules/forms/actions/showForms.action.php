<?php

class showFormsAction extends xAction
{

    public $_props;

    public function build($params)
    {
        if (!empty($params['params'])) {
            $slotFormParams = $params['params'];
            $this->_props['form'] = $this->_tree->getNodeInfo((int)$slotFormParams['formsSourceId']);

            if (!empty($this->_props['form']) && !$this->_props['form']['params']['Disable']) {

                if (empty($this->_props['form']['params']['Template'])) {
                    return false;
                }

                $this->fieldsetsData = $this->getFormData($this->_props['form'], $slotFormParams);

                if (!empty($this->fieldsetsData)) {
                    $this->_props['form']['formKey'] = 'form' . $this->_props['form']['id'];
                    $this->_props['form']['action'] = $params['request']['pageLinkHost'] . '/~submitForm';

                    if (isset($_SESSION[$this->_props['form']['formKey']]['error'])) {

                        $this->_props['form']['error'] = $_SESSION[$this->_props['form']['formKey']]['error'];
                        unset($_SESSION[$this->_props['form']['formKey']]['error'], $_SESSION[$this->_props['form']['formKey']]['requestData']);
                    }

                    if ($this->_props['form']['params']['useCaptcha']) {
                        $this->_props['captcha'] = $this->getCaptcha($this->_props['form']);
                    }

                } else {
                    return false;
                }
            }
        }
    }

    public function run($params)
    {
        $this->loadModuleTemplate($this->_props['form']['params']['Template']);
        $this->_TMS->addReplace('forms', 'object', $this->_props['form']);
        $this->_TMS->addReplace('forms', 'error', $this->_props['form']['error']);
        $this->_TMS->addReplace('forms', 'captcha', $this->_props['captcha']);
        return $this->_TMS->parseSection('forms');
    }

    public function runHeadless($params)
    {
        return $this->_props['form'];
    }


}
