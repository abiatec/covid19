<?php

use X4\Classes\XPDO;

class formsTpl extends xTpl implements xModuleTpl
{

    public function valuesCount($params)
    {
        if (is_string($params[0])) {
            $delimiter = ($params[1]) ? $params[0] : "\n";
            $size = sizeof(explode($delimiter, $params[0]));
        } else if (is_array($params[0])) {
            $size = sizeof($params[0]);
        }

        return ($size > 4) ? 4 : $size;
    }

    public function checkUserForms($formData)
    {

        if ($formData['id']) {
            $ip = getenv("REMOTE_ADDR");
            $where = ' `formId` = ' . $formData['id'] . ' AND `ip`="' . $ip . '"';
            $res = XPDO::selectIN('count(id)', 'from_server_ip', $where);
            $count = (int)$res[0]['count(id)'];
            if ($count > 0) {
                return array('status' => false);
            }
        }
        return array('status' => true);


    }
}
