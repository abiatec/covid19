<?php

use X4\Classes\XNameSpaceHolder;
use X4\Classes\XPDO;
use X4\Classes\XRegistry;

class formsFront extends xModule
{
    public $fieldsetsData = [];

    public function __construct()
    {
        parent::__construct(__CLASS__);

        if (xConfig::get('GLOBAL', 'currentMode') == 'front') {
            $this->_tree->cacheState($this->_config['cacheTree']['tree']);
        }

        XNameSpaceHolder::addMethodsToNS('forms', array('fieldset'), $this);
    }

    private function returnFieldsets($key)
    {
        if (isset($this->fieldsetsData[$key])) {
            return $this->fieldsetsData[$key];
        }
    }

    public function fieldset($params, $context)
    {
        return $this->returnFieldsets($context['return']);
    }

    public function getFormData($form, $params)
    {
        if (empty($params['fieldStyles'])) {
            return false;
        }

        if (!isset($form['id'])) {
            $form = $this->_tree->getNodeInfo((int)$params['formsSourceId']);
        }

        if (!empty($form['id'])) {
            $this->loadModuleTemplate($params['fieldStyles']);
            $nodes = $this->_tree->selectStruct('*')->selectParams('*')->childs((int)$form['id'])->asTree()->run();
            $fieldsets = array();

            if (!is_numeric($form['basic'])) {
                $group = $form['basic'];
            }

            foreach ($nodes->tree[$form['id']] as $k => $node) {
                if (!empty($group)) {
                    $fieldsets[$group][$node['basic']] = $node;
                } else {
                    $fieldsets[$node['basic']] = $node;
                }

                foreach ($nodes->tree[$k] as $key => $value) {

                    $nodes->tree[$k][$key]['params']['settings'] = unserialize($value['params']['settings']);

                    if ($nodes->tree[$k][$key]['params']['settings']['required']) {
                        if ($validationEngineClass = $this->greateValidationEngineClass($nodes->tree[$k][$key]['params']['settings']['restriction'])) {
                            $nodes->tree[$k][$key]['params']['settings']['validationEngineClass'] = $validationEngineClass;
                        }
                    }

                    if (($nodes->tree[$k][$key]['params']['type'] == 'SingleSelect' || $nodes->tree[$k][$key]['params']['type'] == 'MultipleSelect') && !empty($nodes->tree[$k][$key]['params']['settings']['value'])) {
                        $nodes->tree[$k][$key]['params']['options'] = $this->selectOptionsParse($nodes->tree[$k][$key]['params']['settings']['value']);

                    }

                    $nodes->tree[$k][$key]['formId'] = $form['id'];
                    $nodes->tree[$k][$key]['fieldsetId'] = $node['id'];
                    $nodes->tree[$k][$key]['attr'] = array(
                        'id' => $form['id'] . $node['id'] . $value['id'],
                        'name' => ($form['params']['Async'] == '1') ? 'form' . $form['id'] . '.' . $value['id'] : 'form[' . $form['id'] . '][' . $value['id'] . ']'
                    );
                    $nodes->tree[$k][$key]['fieldHTML'] = $this->fieldParse($nodes->tree[$k][$key]);
                }

                if (isset($group)) {
                    $fieldsets[$group][$node['basic']]['fields'] = array_values($nodes->tree[$k]);
                } else {
                    $fieldsets[$node['basic']]['fields'] = array_values($nodes->tree[$k]);
                }
            }

            return $fieldsets;
        } else {
            return false;
        }
    }

    private function fieldParse($fieldData)
    {
        if (!empty($fieldData)) {
            $formKey = 'form' . $fieldData['formId'];
            $replaceData = array(
                'id' => $fieldData['id'],
                'formId' => $fieldData['formId'],
                'fieldsetId' => $fieldData['fieldsetId'],
                'Name' => $fieldData['params']['Name'],
                'NameValue' => $fieldData['params']['settings']['basic'] ? $fieldData['params']['settings']['basic'] : $fieldData['fieldsetId'],
                'description' => $fieldData['params']['settings']['description'],
                'required' => $fieldData['params']['settings']['required'],
                'validationEngineClass' => $fieldData['params']['settings']['validationEngineClass'],
                'params' => $fieldData['params'],
                'attr' => $fieldData['attr']
            );

            switch ($fieldData['params']['type']) {
                case 'PlainText':
                    $replaceData['text'] = $fieldData['params']['settings']['text'];
                    break;

                case 'SingleLineText':
                case 'Textarea':
                    $replaceData['placeholder'] = $fieldData['params']['settings']['placeholder'];

                    if (!empty($fieldData['params']['settings']['type'])) {
                        $replaceData['type'] = $fieldData['params']['settings']['type'];
                    }

                    if (isset($_SESSION[$formKey]['requestData'][$fieldData['id']])) {
                        $replaceData['defaultValue'] = $_SESSION[$formKey]['requestData'][$fieldData['id']];
                    } else {
                        $replaceData['defaultValue'] = $fieldData['params']['settings']['value'];
                    }
                    break;

                case 'SingleSelect':
                    if (isset($_SESSION[$formKey]['requestData'][$fieldData['id']])) {
                        foreach ($fieldData['params']['options'] as $k => $option) {
                            if ($option['value'] == $_SESSION[$formKey]['requestData'][$fieldData['id']]) {
                                $replaceData['params']['options'][$k]['selected'] = 'selected';
                            }
                        }
                    }
                    break;

                case 'MultipleSelect':
                    $replaceData['attr']['name'] .= '[]';

                    if (isset($_SESSION[$formKey]['requestData'][$fieldData['id']])) {
                        foreach ($fieldData['params']['options'] as $k => $option) {
                            if (in_array($option['value'], $_SESSION[$formKey]['requestData'][$fieldData['id']])) {
                                $replaceData['params']['options'][$k]['selected'] = 'selected';
                            }
                        }
                    }
                    break;

                case 'SingleCheckbox':
                    $replaceData['defaultValue'] = $fieldData['params']['settings']['value'];

                    if (isset($_SESSION[$formKey]['requestData'][$fieldData['id']])) {
                        $replaceData['checked'] = 1;
                    }
                    break;
            }

            $this->_TMS->addMassReplace($fieldData['params']['type'], $replaceData);
            return $this->_TMS->parseSection($fieldData['params']['type']);

        } else {
            return '';
        }
    }

    private function selectOptionsParse($optionsValue)
    {
        $optionsValue = explode("\n", $optionsValue);
        $options = array();

        foreach ($optionsValue as $option) {
            $optData = explode(':', $option);
            array_push($options, array(
                'value' => $optData[0],
                'name' => ($optData[1]) ? $optData[1] : $optData[0]
            ));
        }

        unset($optionsValue);

        return $options;
    }

    private function greateValidationEngineClass($restriction)
    {
        $validate = array('required');

        if (!empty($restriction)) {
            switch ($restriction) {
                case 'lettersonly':
                case 'onlyLetterSp':
                    array_push($validate, 'custom[onlyLetterSp]');
                    break;

                case 'onlyNumberSp':
                    array_push($validate, 'custom[onlyNumberSp]');
                    break;

                case 'alphanumeric':
                case 'onlyLetterNumber':
                    array_push($validate, 'custom[onlyLetterNumber]');
                    break;

                case 'letterswithbasicpunc':
                    array_push($validate, 'custom[lettersWithBasicPunc]');
                    break;

                case 'onlyLetterCyrillicSp':
                    array_push($validate, 'custom[onlyLetterCyrillicSp]');
                    break;

                case 'phone':
                    array_push($validate, 'custom[phone]');
                    break;

                case 'email':
                    array_push($validate, 'custom[email]');
                    break;

                case 'url':
                    array_push($validate, 'custom[url]');
                    break;
            }
        }

        return 'validate[' . implode(',', $validate) . ']';
    }

    public function submitForm($params)
    {
        if (!empty($params['request']['requestData']['form'])) {
            if (!isset($params['request']['requestData']['form']['id']) || is_numeric($params['request']['requestData']['form']['id']) == false || $params['request']['requestData']['form']['id'] == false) {
                $form = $this->_tree->getNodeInfo((int)$params['params']['formsSourceId']);
            } else {
                $form = $this->_tree->getNodeInfo((int)$params['request']['requestData']['form']['id']);
            }

            $formKey = 'form' . $form['id'];

            if (!$form['params']['submitTemplate']) {
                return $this->_TMS->parseSection('failed_template');
            }
            if (!empty($params['request']['requestData']['form'][$form['id']])) {
                return $this->_TMS->parseSection('failed_data');
            }
            if (!isset($_SESSION[$formKey])) {
                $_SESSION[$formKey] = array();
            }

            $this->loadModuleTemplate($form['params']['submitTemplate']);

            if (isset($params['request']['requestData']['form'][$form['id']]['captcha'])) {
                if (!$this->validateCaptcha($form, $params)) {
                    $_SESSION[$formKey]['error'] = $this->_TMS->parseSection('captcha');
                    $_SESSION[$formKey]['requestData'] = $params['request']['requestData']['form'][$form['id']];
                    $this->redirectToFormPage($params);
                } else {
                    unset($params['request']['requestData']['form'][$form['id']]['captcha']);
                }
            }

            $timeout = $this->timeout($form);

            if (gettype($timeout) == 'boolean' && $timeout == true) {
                if ($this->sendMessage($form, $params['request']['requestData']['form'][$form['id']])) {
                    $this->_TMS->addReplace('success', 'messageAfter', $form['params']['messageAfter']);
                    unset($params['request']['requestData']['form'], $form);
                    return $this->_TMS->parseSection('success');
                } else {

                    $_SESSION[$formKey]['error'] = $this->_TMS->parseSection('failed');
                    $_SESSION[$formKey]['requestData'] = $params['request']['requestData']['form'][$form['id']];
                    $this->redirectToFormPage($params);
                }
            } else if (gettype($timeout) == 'double') {
                $params['error'][$formKey] = array('error_type' => 'timeout', 'time_left' => $timeout);
                $this->_TMS->addReplace('timeout', 'sec', $timeout);
                $_SESSION[$formKey]['error'] = $this->_TMS->parseSection('timeout');
                $_SESSION[$formKey]['requestData'] = $params['request']['requestData']['form'][$form['id']];
                $this->redirectToFormPage($params);
            }
        } else {
            $this->redirectToFormPage($params);
        }
    }

    private function constructMessage($form, $fieldsData)
    {
        $nodeChilds = $this->_tree->selectStruct('*')->selectParams('*')->childs((int)$form['id'])->asTree()->run();
        $message = [];
        $messageText = '';

        $this->messageDataTree = array(
            'Name' => $form['params']['Subject'],
            'groups' => array()
        );

        XRegistry::get('EVM')->fire($this->_moduleName . '.onConstructMessage', array('values' => $fieldsData, 'fields' => $nodeChilds->tree[$form['id']]));

        foreach ($nodeChilds->tree[$form['id']] as $k => $node) {

            $rows = '';
            $groupKey = $node['id'];

            if (!isset($this->messageDataTree['groups'][$groupKey])) {
                $this->messageDataTree['groups'][$groupKey] = array(
                    'Name' => $node['params']['Name'],
                    'fields' => array()
                );
            }

            foreach ($nodeChilds->tree[$k] as $key => $value) {

                $value['params']['settings'] = unserialize($value['params']['settings']);
                if (isset($fieldsData[$value['params']['settings']['basic']])) {
                    $fieldsData[$value['id']] = $fieldsData[$value['params']['settings']['basic']];
                }


                if (isset($fieldsData[$value['id']])) {
                    if (is_array($fieldsData[$value['id']])) {
                        $val = implode(', ', $fieldsData[$value['id']]);
                    } else if (gettype($fieldsData[$value['id']]) == 'boolean' && $fieldsData[$value['id']] == true) {
                        $value['params']['settings'] = unserialize($value['params']['settings']);
                        $val = $value['params']['settings']['value'];
                    } else {
                        $val = $fieldsData[$value['id']];
                    }

                    array_push($this->messageDataTree['groups'][$groupKey]['fields'], array(
                        'Name' => $value['params']['Name'],
                        'basicName' => $value['params']['settings']['basic'] ? $value['params']['settings']['basic'] : $value['id'],
                        'value' => $val,
                        'HOST' => ‌‌HOST
                    ));

                    $this->_TMS->addMassReplace('row', array(
                        'name' => $value['params']['Name'],
                        'basicName' => $value['params']['settings']['basic'] ? $value['params']['settings']['basic'] : $value['id'],
                        'value' => $val
                    ));
                    $rows .= $this->_TMS->parseSection('row');
                }
            }

            $this->_TMS->addMassReplace('group', array(
                'Name' => $node['params']['Name'],
                'rows' => $rows
            ));
            $messageText .= $this->_TMS->parseSection('group');
        }


        $updateMessageText = XRegistry::get('EVM')->fire($this->_moduleName . '.onConstructMessage:before', array('this' => $this, 'nodeChilds' => $nodeChilds, 'fieldsData' => $fieldsData, 'form' => $form));

        if (!empty($updateMessageText['messageText'])) {
            $messageText = $updateMessageText['messageText'];
            if (!empty($updateMessageText['Subject'])) {
                $message['Subject'] = $updateMessageText['Subject'];
            }

            if (!empty($updateMessageText['attachFile'])) {
                $message['attachFile'] = $updateMessageText['attachFile'];
            }

            if (!empty($updateMessageText['CcValue'])) {
                $message['CcValue'] = $updateMessageText['CcValue'];
            }

            if (!empty($updateMessageText['addReplyTo'])) {
                $message['addReplyTo'] = $updateMessageText['addReplyTo'];
            }

            if (!empty($updateMessageText['changeFromEmail'])) {
                $message['changeFromEmail'] = $updateMessageText['changeFromEmail'];
                $message['changeFromEmailValue'] = $updateMessageText['changeFromEmailValue'];
            }


            if (!empty($updateMessageText['Subject'])) {
                $form['params']['Subject'] = $message['Subject'];
                $form['params']['Name'] = $updateMessageText['Subject'];
            }
        }

        $this->_TMS->addMassReplace('message', array(
            'Name' => $form['params']['Name'],
            'charset' => $form['params']['charset'],
            'subject' => $form['params']['Subject'],
            'text' => $messageText
        ));


        $this->_TMS->addMassReplace('saved_message', array(
            'Name' => $form['params']['Name'],
            'subject' => $form['params']['Subject'],
            'text' => $messageText
        ));

        $message['to_email'] = $this->_TMS->parseSection('message');
        $message['to_save'] = $this->_TMS->parseSection('saved_message');

        return $message;
    }

    public function sendMessage($form, $fieldsData)
    {
        $message = $this->constructMessage($form, $fieldsData);

        $fromEmail = xConfig::get('GLOBAL', 'from_email');

        $addCc = false;
        $addReplyTo = false;
        if (!empty($message['Subject'])) {
            $form['params']['Subject'] = $message['Subject'];
            $form['params']['Name'] = $message['Subject'];
            if (!empty($message['CcValue'])) {
                $addCc = true;
                $CcValue = $message['CcValue'];

            }
            if (!empty($message['addReplyTo'])) {
                $addReplyTo = true;
            }
        }

        if ($message['changeFromEmail'] == true) {
            $fromEmail = $message['changeFromEmailValue'];
        }


        $emails = explode(',', $form['params']['Emails']);


        if ($form['params']['saveToServer']) {
            $this->saveMessageOnServer($form, $message['to_save']);
            $this->saveMessageDataInTree($form);
        }

        return $this->sendEmail($form,$fromEmail,$emails,$addReplyTo,$addCc,$CcValue);

    }

    private function sendEmail($form,$fromEmail,$emails,$addReplyTo,$addCc,$CcValue){

        $transport = XRegistry::get('SwiftMailerTransport');
        $mailer = new Swift_Mailer($transport);
        $message = new Swift_Message();
        $message->setFrom($fromEmail);

        if ($addReplyTo) {
            $message->setReplyTo($fromEmail);
        }

        if ($addCc) {
            $message->Cc($CcValue);
        }
        $message->setSubject($form['params']['Subject']);
        $message->setBody($message['to_email'], 'text/html', $form['params']['charset']);

        if (!empty($message['attachFile'])) {
            foreach ($message['attachFile'] as $file) {
                $message->attach(Swift_Attachment::fromPath(PATH_ . $file));
            }
        }

        $message->setTo(array_map(function ($to) {
            return trim($to);
        }, $emails));

        $message->setPriority(2);
        return $mailer->send($message);

    }

    private function saveMessageOnServer($form, $message)
    {
        XPDO::insertIN('forms_messages', array(
                'id' => 'null',
                'form_id' => (int)$form['id'],
                'Name' => $form['params']['Name'],
                'date' => date("d.m.Y H:i:s"),
                'message' => $message,
                'status' => 0,
                'archive' => 0)
        );
    }

    private function saveMessageDataInTree($form)
    {
        $messageGroup = false;

        if (!empty($this->messageDataTree['groups'])) {
            //1th level
            if (empty($form['id'])) {
                $basic = '%SAMEASID%';
            } elseif (is_numeric($form['id'])) {
                $basic = $form['id'];
                if ($messageGroup = $this->_commonObj->_treeMessages->selectStruct(array('id'))->where(array('@ancestor', '=', 1), array('@basic', '=', $basic))->run()) {
                    $messageGroup = $messageGroup[0]['id'];
                } else {
                    $messageGroup = false;
                }
            } else {
                $basic = '%SAMEASID%';
            }

            $messageGroupData = array(
                'Name' => $form['params']['Name'],
                'description' => $form['params']['comment']
            );

            if (empty($messageGroup)) {
                $messageGroupData['id'] = $this->_commonObj->_treeMessages->initTreeObj(1, $basic, '_MESSAGEGROUP', $messageGroupData);
            } else {
                $this->_commonObj->_treeMessages->reInitTreeObj((int)$messageGroup, $basic, $messageGroupData);
                $messageGroupData['id'] = $messageGroup;
            }

            if ($messageGroupData['id']) {
                //2th level
                $messageId = $this->_commonObj->_treeMessages->initTreeObj($messageGroupData['id'], '%SAMEASID%', '_MESSAGE', array(
                    'Name' => $this->messageDataTree['Name']
                ));

                if ($messageId) {
                    //3th level
                    foreach ($this->messageDataTree['groups'] as $key => $group) {
                        $groupId = $this->_commonObj->_treeMessages->initTreeObj($messageId, '%SAMEASID%', '_MESSAGEFIELDSET', array(
                            'Name' => $group['Name']
                        ));

                        if ($groupId) {
                            //4th level
                            foreach ($group['fields'] as $k => $field) {
                                $fieldId = $this->_commonObj->_treeMessages->initTreeObj($groupId, '%SAMEASID%', '_MESSAGEFIELD', array(
                                    'Name' => $field['Name'],
                                    'value' => $field['value'],
                                    'basicName' => $field['basicName']
                                ));
                            }
                        }
                    }
                }
            }

            unset($this->messageDataTree);
        }
    }

    public function getCaptcha($form)
    {
        if (!empty($form['id'])) {
            $formKey = 'form' . $form['id'];
            $_SESSION['captchaSettings'] = explode('-', $form['params']['captchaSettings']);
            $this->_TMS->addMassReplace('captcha', array(
                'formId' => $form['id'],
                'length' => $_SESSION['captchaSettings'][0],
                'attr' => array(
                    'id' => 'captcha_form' . $form['id'],
                    'name' => ($form['params']['Async'] == '1') ? $formKey . '.captcha' : 'form[' . $form['id'] . '][captcha]'
                )
            ));
            return $this->_TMS->parseSection('captcha');
        } else {
            return '';
        }
    }

    public function validateCaptcha($form, $params)
    {
        $formKey = 'form' . $form['id'];

        if (!empty($form['id']) && !empty($_SESSION['captcha'][$formKey])) {
            $captcha = $_SESSION['captcha'][$formKey];
        } else {
            $captcha = $_SESSION['captcha'];
        }

        if (isset($params['request']['requestData']['form'][$form['id']]['captcha']) && $captcha == $params['request']['requestData']['form'][$form['id']]['captcha']) {
            $this->result['captcha'] = true;
            return true;
        } else if (isset($params['formData'][$formKey]['captcha']) && $captcha == $params['formData'][$formKey]['captcha']) {
            $this->result['captcha'] = true;
            return true;
        } else if (isset($params['captcha']) && $captcha == $params['captcha']) {
            $this->result['captcha'] = true;
            return true;
        } else {
            $this->result['captcha'] = false;
            return false;
        }
    }

    public function timeout($form)
    {
        $formKey = 'form' . $form['id'];
        $user_ip = getenv('REMOTE_ADDR');

        if (isset($_SESSION[$formKey]['timeout']) && array_key_exists($user_ip, $_SESSION[$formKey]['timeout'])) {
            $mtime = (int)$form['params']['Timeout'];
            $curtime = ceil(Common::getmicrotime());
            $timeLeft = (int)$mtime - ($curtime - $_SESSION[$formKey]['timeout'][$user_ip]);

            if ($timeLeft <= 0) {
                $_SESSION[$formKey]['timeout'][$user_ip] = ceil(Common::getmicrotime());
                return true;
            } else {
                return $timeLeft;
            }
        } else {
            $_SESSION[$formKey]['timeout'][$user_ip] = ceil(Common::getmicrotime());
            return true;
        }
    }


    private function redirectToFormPage($params)
    {
        if (isset($params['request']['pageLinkHost'])) {
            header('Location: ' . $params['request']['pageLinkHost']);
        } else {
            header('Location: ' . HOST);
        }
        exit;
    }
}
