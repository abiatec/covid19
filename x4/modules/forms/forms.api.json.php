<?php



/**
 *
 * @SWG\Swagger(
 *   @SWG\Info(
 *     title="Forms api ",
 *     version="1.0.0",
 *     author="Sankevich S.S."
 *
 *   ),
 *
 *   schemes={"http","https"},
 *   basePath="/~api/json/forms",
 *   consumes={"application/json"},
 *   produces={"application/json"},
 *
 *
 *   @SWG\Definition(
 *     definition="attachFile",
 *     type="object",
 *     allOf={
 *       @SWG\Schema(
 *            required={"word"},
 *            @SWG\Property(property="keyword", type="string",example="Search word"),
 *            @SWG\Property(property="pageNum", type="string",example="1"),
 *        )
 *    }
 *   )
 * )
 */
class formsApiJson
    extends xModuleApi
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    /**
     * @SWG\Post(
     *     path="/postFile",
     *     summary="posts  file",
     *     operationId="postFile",
     *     produces={"application/json"},
     *
     *
     *     @SWG\Parameter(
     *         name="id",
     *         in="body",
     *         description="id",
     *         type="integer",
     *         required=true
     *     ),
     *
     *     @SWG\Response(response=200, description="postFile")
     * )
     */

    public function postFile($params, $data)
    {
        $_POST['id'] = intval($_POST['id']);

        if (isset($_FILES["uploadedFile"]["name"])) {
            $name = $_FILES["uploadedFile"]["name"];
            $tmpName = $_FILES['uploadedFile']['tmp_name'];

            $location = xConfig::get('PATH', 'MEDIA') . 'uploadsFrom/';

            $date = date("m_d_y");

            if (!is_dir($location) || !is_dir($location . $date)) {

                mkdir($location, xConfig::get('GLOBAL', 'defaultModeFiles'), true);
                if (!is_dir($location . $date)) {

                    mkdir($location . $date, xConfig::get('GLOBAL', 'defaultModeFiles'), tre);
                }
            }

            $location = $location . $date . '/';

            if (!empty($name)) {
                $name = xString::cyrillicTranslit($name);

                $destination = $location . $name;

                if (!is_dir($location)) {
                    mkdir($location);
                }

                if (move_uploaded_file($tmpName, $location . $name)) {

                    if ($_FILES["uploadedFile"]["type"] == 'image/png' || $_FILES["uploadedFile"]["type"] == 'image/jpeg') {
                        $imageTransform = '/' . str_replace(PATH_, '', $destination);
                        $isImg = true;
                    } else {
                        $isImg = false;
                    }
                    return array(
                        'success' => true,
                        'path' => $destination,
                        'shortPath' => '/' . str_replace(PATH_, '', $destination),
                        'originalName' => $_FILES["uploadedFile"]["name"],
                        'imageTransform' => $imageTransform,
                        'isImg' => $isImg,
                        'fileId' => $_POST['fileId']
                    );
                }

            } else {

                return array('error' => true, 'fileId' => $_POST['fileId']);
            }
        } else {

            return array('error' => true, 'fileId' => $_POST['fileId'], 'detail' => 'file or id is not specified');
        }

    }


}


