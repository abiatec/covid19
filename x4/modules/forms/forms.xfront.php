<?php

use X4\Classes\XPDO;

class formsXfront extends formsFront
{
    public function sendFormData($params)
    {
        $formKey = 'form'.$params['formId'];

        if(isset($params['formId']) && (int)$params['formId'] > 0 && !empty($params['formData'][$formKey])) {
            $form = $this->_tree->getNodeInfo((int)$params['formId']);

            if(!$form['params']['submitTemplate']) {
                $this->result['error'] = 2;
                return false;
            }

            $this->loadModuleTemplate($form['params']['submitTemplate']);

            if(isset($params['formData'][$formKey]['captcha'])) {
                if(!$this->validateCaptcha($form, $params)) {
                    $this->result['error'] = $this->_TMS->parseSection('captcha');
                    return false;
                } else {
                    unset($params['formData'][$formKey]['captcha']);
                }
            }

            $timeout = $this->timeout($form);

            if(gettype($timeout) == 'boolean' && $timeout) {
                if($this->sendMessage($form,$params['formData'][$formKey])) {
                    $this->_TMS->addReplace('success','messageAfter',$form['params']['messageAfter']);

                    $this->result['success'] = $this->_TMS->parseSection('success');
                    unset($params,$form);
                } else {
                    unset($params,$form);
                    $this->result['error'] = $this->_TMS->parseSection('failed');
                }
            } else if(gettype($timeout) == 'double') {
                $this->_TMS->addReplace('timeout', 'sec', $timeout);
                $this->result['error'] = $this->_TMS->parseSection('timeout');
                $this->result['error'] = $this->_TMS->parseSection('timeout');
                return false;
            }

        } else {
            $this->result['error'] = 1;
            return false;
        }
    }

    public function checkCaptcha($params)
    {
        if(empty($params['form']) && empty($params['params'])) {
            $this->result['captcha'] = false;
            return false;
        } else {
            return $this->validateCaptcha($params['form'],$params['params']);
        }
    }

    public function addUserForms($formId) {

        if(!empty($formId['formId'])){
            $ip = getenv("REMOTE_ADDR");
            $res =  XPDO::insertIN('from_server_ip',array('ip'=>$ip,'formId'=>$formId['formId'],'value'=>0));

            if($res) {
                $this->result['add'] = true;
            }else{
                $this->result['add'] = false;
            }

        }
        $this->result['add'] = false;

    }
}
