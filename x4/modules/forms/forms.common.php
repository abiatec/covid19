<?php

use X4\Classes\XRegistry;
use X4\Classes\XTreeEngine;

class formsCommon extends xModuleCommon implements xCommonInterface
{
    public $_useTree = true;

    public function __construct()
    {
        parent::__construct(__CLASS__);

        $this->_tree->setObject('_ROOT', array('Name'));

        $this->_tree->setObject('_FORM', array
        (
            'Name',
            'Title',
            'Disable',
            'Subject',
            'Template',
            'submitTemplate',
            'Emails',
            'charset',
            'saveToServer',
            'useCaptcha',
            'captchaSettings',
            'Async',
            'Timeout',
            'Description',
            'Author',
            'messageAfter'
        ), array('_ROOT'));

        $this->_tree->setObject('_FIELDSET', array(
            'Name',
            'Disable',
            'Description'
        ), array('_FORM'));

        $this->_tree->setObject('_FIELD', null, array('_FORM', '_FIELDSET'));


        $this->_treeMessages = new XTreeEngine('forms_messages', XRegistry::get('XPDO'));
        $this->_treeMessages->setLevels(5);
        $this->_treeMessages->setUniqType(1);
        $this->_treeMessages->setObject('_ROOT', array('Name'));
        $this->_treeMessages->setObject('_MESSAGEGROUP', null, array('_ROOT'));
        $this->_treeMessages->setObject('_MESSAGE', null, array('_MESSAGEGROUP'));
        $this->_treeMessages->setObject('_MESSAGEFIELDSET', null, array('_MESSAGE'));
        $this->_treeMessages->setObject('_MESSAGEFIELD', null, array('_MESSAGEFIELDSET'));
    }


    public function defineFrontActions()
    {
        $this->defineAction('showForms', array('serverActions' => array('submitForm'))); //One Form
    }
}

