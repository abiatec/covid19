<?php

class catalogListener extends xListener implements xModuleListener
{


    public function __construct()
    {
        parent::__construct('catalog');
        $this->setDefaultUserPrice();
        $this->_EVM->on('zero-boot', 'urlReverseTransform', $this);
        $this->_EVM->on('agregator:onUrlRewrited', 'urlReverseTransformOnRewrite', $this);
        $this->_EVM->on('AdminPanel:afterInit', 'serializeAllPropertyGroup', $this);
        $this->_EVM->on('AdminPanel:afterCacheClear', 'serializeAllPropertyGroup', $this);
        $this->_EVM->on('AdminPanel:afterCacheClear', 'refreshUrlTransform', $this);
        $this->_EVM->on('ishop:afterCurrencyChange', 'rebuildIshopPrices', $this);
        $this->_EVM->on('ishop:afterCurrentCoursesChange', 'rebuildIshopPrices', $this);
        $this->_EVM->on('catalog.paginator:onPage', 'onPaginatorPage', $this);
        $this->_EVM->on('catalog.paginator:onPrefixSuffixLink', 'onPaginatorSuffixPage', $this);
        $this->_EVM->on('pages.back:slotModuleInitiated', 'onSlotModuleSave', $this);

    }

    public function boostTreeListener($params)
    {
        $this->boostTree($params);
    }

    public function refreshUrlTransform($params)
    {

        $this->truncateTransformList();
        $this->rebuildUrlTransformMatrix();


    }


    public function onSlotModuleSave($params)
    {
        if ('showCatalogServer' == $params['data']['module']['params']['_Action']) {
            $pages = xCore::loadCommonClass('pages');
            $contentPageLink = $pages->createPagePath($params['data']['pageId'], true);
            $source = $contentPageLink . '/(?!~)(.*?)';
            $destination = $contentPageLink . '/~show$1/';
            $pages->createNewRoute($source, $destination);
        }
    }


    public function onPaginatorPage($params)
    {
        $data = $params['data']['data'];
        $data['link'] = $this->getInstance()->_commonObj->buildUrlTransformation($data['link']);
        $data['link'] = X4\Classes\XRegistry::get('TPA')->reverseRewrite($data['link']);
        return $data;
    }

    public function onPaginatorSuffixPage($params)
    {
        $data = $params['data'];
        $data['linker'] = $this->getInstance()->_commonObj->buildUrlTransformation($data['linker']);
        return X4\Classes\XRegistry::get('TPA')->reverseRewrite($data['linker']);

    }


    public function urlReverseTransformOnRewrite($params)
    {

        $catalog = xCore::moduleFactory('catalog.front');
        $request = $catalog->_commonObj->buildUrlReverseTransformation($params['data']['url']);

        if (!strstr($params['data']['url'], '~search') && !empty($request)) {
            $parsedUrl = parse_url($request);
            if (!empty($parsedUrl['query'])) {
                parse_str($parsedUrl['query'], $parsedRequest);
                $_GET = array_replace_recursive($_GET, $parsedRequest);
                $_REQUEST = array_replace_recursive($_REQUEST, $parsedRequest);
            }

            return array('url' => $request, 'rewritten' => ($request != $params['data']['url']));
        }

    }

    public function urlReverseTransform($params)
    {
        $catalog = xCore::moduleFactory('catalog.front');

        if (!$_REQUEST['xoadCall']) {

            $transformed = $catalog->_commonObj->buildUrlTransformation($_SERVER['REQUEST_URI']);

            if (!empty($transformed) && $_SERVER['REQUEST_URI'] !== $transformed) {
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: $transformed");
                exit();
            }
        }

        $_SERVER['REQUEST_URI'] = $request = $catalog->_commonObj->buildUrlReverseTransformation($_SERVER['REQUEST_URI']);


        if (!strstr($_SERVER['REQUEST_URI'], '~search')) {
            $parsedUrl = parse_url($request);
            if (!empty($parsedUrl['query'])) {

                parse_str($parsedUrl['query'], $parsedRequest);
                $_GET = array_replace_recursive($_GET, $parsedRequest);
                $_REQUEST = array_replace_recursive($_REQUEST, $parsedRequest);
            }
        }

    }

    public function rebuildIshopPrices($params)
    {
        $this->rebuildIcurrencyFields($params);
    }


    public function serializeAllPropertyGroup($params)
    {

        if (!$_REQUEST['xoadCall'] || ($params['event'] == 'AdminPanel:afterCacheClear')) {
            $this->createPropertyGroupSerializedAll();
        }
    }


}