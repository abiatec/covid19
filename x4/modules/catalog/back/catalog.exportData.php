<?php

use X4\Classes\XRegistry;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;


class exportData
{
    const  EXPORTABLE_TYPES = ['input', 'image'];
    const  MAXIMUM_FIELDS = 200;
    private $propertyGroupsAll = [];
    private $propertyGroupsSelected = [];
    private $propertiesMap = ['', 'exportId', 'Name', 'seo.Title', 'seo.Keywords', 'seo.Description'];
    private $spreadsheet = null;
    private $propertiesMapInverted;
    private $rows = 1;
    private $propertiesMapSize = 5;
    private $exportLevelDeep;

    public function fetchAllPropertyGroups()
    {
        $propertyGroupList = XRegistry::get('catalogBack')->_commonObj->_propertySetsTreeGroup->selectStruct('*')->where(['@obj_type', '=', '_PROPERTYSETGROUP'])->run();
        foreach ($propertyGroupList as $item) {
            $this->propertyGroupsAll[$item['id']] = XRegistry::get('catalogBack')->_commonObj->getPropertyGroupSerialized($item['id']);
        }

    }

    public function addToPropertiesMap($psetId)
    {
        static $addedPsets;

        if (!$addedPsets[$psetId]) {

            $propsList = [];
            foreach ($this->propertyGroupsAll[$psetId] as $item) {
                foreach ($item as $groupKey => $setGroup) {
                    foreach ($setGroup as $propKey => $prop) {
                        if (in_array($prop['params']['type'], self::EXPORTABLE_TYPES)) {
                            $propsList[] = $groupKey . '.' . $propKey;
                        }
                    }
                }
            }

            $this->propertiesMap = array_unique(array_merge($this->propertiesMap, $propsList));
            $this->propertiesMapInverted = array_flip($this->propertiesMap);
            $addedPsets[$psetId] = true;
        }


    }

    public function prepareSpreadSheet()
    {


        $this->instanceSpreadsheet = new Spreadsheet();
        $this->spreadsheet = $this->instanceSpreadsheet->setActiveSheetIndex(0);

        /*
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="01simple.xlsx"');
        header('Cache-Control: max-age=0');

        header('Cache-Control: max-age=1');


        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');*/
        //exit;

    }

    private function getAlphas()
    {
        for ($i = 0; $i < self::MAXIMUM_FIELDS; $i++) {
            $this->alphas[] = Coordinate::stringFromColumnIndex($i);
        }
    }

    private function setHeaders()
    {
        $this->spreadsheet->getColumnDimension('A')->setWidth(30);
        $this->spreadsheet->getColumnDimension('B')->setWidth(50);
        $this->spreadsheet->getColumnDimension('C')->setWidth(50);
        $this->spreadsheet->getColumnDimension('D')->setWidth(50);
        $this->spreadsheet->getColumnDimension('E')->setWidth(50);
        $z = 0;


        foreach ($this->propertiesMap as $key => $value) {
            $z++;
            if ($z > count($this->propertiesMapSize)) {
                $this->spreadsheet->getColumnDimension($this->alphas[$key])->setWidth(50);
            }
            if (!empty($value)) {
                $fieldData = [];

                $value = json_encode([$value => $fieldData]);
                $this->spreadsheet->setCellValue($this->alphas[$key] . '1', $value);
            }
        }
    }

    public function exportDataXLSX($params)
    {

        $path = xConfig::get('PATH', 'MEDIA') . 'export/';
        $webPath = xConfig::get('WEBPATH', 'MEDIA') . 'export/';
        $fileHash = Common::generateHash() . '_' . time().'.xlsx';
        $filePath = $path .$fileHash;
        $this->exportParams = $params;
        if (XFILES::isWritable($path)) {

            $this->getAlphas();
            $this->prepareSpreadSheet();
            $this->fetchAllPropertyGroups();
            $this->getExportDataXLSX((int)$params['id']);
            $this->setHeaders();
            $writer = IOFactory::createWriter($this->instanceSpreadsheet, 'Xlsx');
            $writer->save($filePath);
            $this->catalogInstance->pushMessage('file ' . $filePath . ' written');
            return $webPath.$fileHash;
        } else {

            $this->catalogInstance->pushError('folder ' . $path . ' is-not-writable');
        }

    }

    public function __construct($catalogInstance)
    {
        $this->catalogInstance = $catalogInstance;
    }

    public function getExportDataXLSX($id)
    {

        $categoriesTree = XRegistry::get('catalogBack')->_tree->selectStruct('*')->selectParams('*')->childs((int)$id)->where(['@obj_type', '=', '_CATGROUP']);

        if ($this->exportParams['exportData']['exportFullPath']) {
            $categoriesTree->getParamPath('Name');
        }

        $categoriesTree = $categoriesTree->asTree()->run();


        if ($this->exportParams['exportData']['exportFullPath']) {
            $this->exportLevelDeep = $categoriesTree->findDeepest() + 1;
            for ($i = 1; $i <$this->exportLevelDeep; $i++) {
                array_splice($this->propertiesMap, 1 + $i, 0, 'path' . $i);
                $alpha = $this->alphas[$i + 1];
                $this->spreadsheet->getColumnDimension($alpha)->setWidth(50);

            }
            $this->propertiesMapSize = $this->propertiesMapSize + $this->exportLevelDeep;
            $this->propertiesMapInverted = array_flip($this->propertiesMap);

        }


        if (!empty($categoriesTree)) {
            $categoriesTree->recursiveStep($id, $this, 'iterateExportXLSX');
        } else {
            $node = XRegistry::get('catalogBack')->_tree->getNodeInfo($id);
            $this->processGroup($node);
        }

    }

    public function setDataToXls($id, $item, $category = false)
    {
        $this->rows++;
        $this->spreadsheet->setCellValue('A' . $this->rows, $id);

        foreach ($item as $key => $value) {
            $z = $this->propertiesMapInverted[$key];
            if ($z) {
                $this->spreadsheet->setCellValue($this->alphas[$z] . $this->rows, $value);
                if ($category) {
                    $this->spreadsheet->getStyle($this->alphas[$z] . $this->rows)->getFont()->setBold(true);
                }
            }
        }
    }

    private function convertToPath($node)
    {
        unset($node['paramPath'][1]);
        for ($i = 1; $i <= $this->exportLevelDeep; $i++) {
            $node['params']['path' . $i] = array_shift($node['paramPath']);
        }
        return $node;
    }

    private function processGroup($node)
    {
        if ($node['obj_type'] == '_CATGROUP') {
            $this->addToPropertiesMap($node['params']['PropertySetGroup']);
            $items = XRegistry::get('catalogBack')->_tree->selectStruct('*')->selectParams('*')->childs($node['id'])->where(['@obj_type', '=', '_CATOBJ']);

            if ($this->exportParams['exportData']['exportFullPath']) {
                $items->getParamPath('Name');
            }
            $items = $items->run();


            if ($this->exportParams['exportData']['exportFullPath']) {
                $node = $this->convertToPath($node);
            }


            $this->setDataToXls($node['id'], $node['params'], true);

            if (!empty($items)) {
                foreach ($items as $item) {

                    if ($this->exportParams['exportData']['exportFullPath']) {
                        $item = $this->convertToPath($item);
                    }

                    $this->addToPropertiesMap($item['params']['PropertySetGroup']);
                    $this->setDataToXls($item['id'], $item['params']);

                }
            }
        }
    }

    public function iterateExportXLSX($node, $ancestor, $tContext, $extdata)
    {
        $this->processGroup($node);
    }


}


