<?php

class tableProperty extends catalogProperty
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }


    public function handleTypeOnSave($property, $value, $paramSet, $paramPath)
    {
        return $value;

    }

 
    public function handleTypeFront($value = null, $property = null, $object = null, $setName = null)
    {

        if (is_string($value)) {
            return json_decode($value, true);
        } else {
            return $value;
        }
    }


}
