<?php

use X4\Classes\XRegistry;


class textareaProperty extends catalogProperty
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function onListingView($value, $propertyInfo, $clmn)
    {
        $catalog = XRegistry::get('catalogBack');

        return  XSTRING::findnCutSymbolPosition($value, " ", $catalog->_config['cutWordsTextAreaListing']);

    }
}
