<?php

use X4\Classes\XRegistry;


class inputProperty extends catalogProperty
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function handleSearchFilterSorting($matrix, $field)
    {

        $sorting = XARRAY::asKeyVal($matrix, 'value');
        if ($field['sort'] == 'asc') {
            asort($sorting);
        } else {

            arsort($sorting);
        }
        foreach ($sorting as $key => $sortItem) {
            $sorted[$key] = $matrix[$key];

        }
        return $sorted;
    }


    public function handleRegularReverseUrlTransformation($transform, $explodedQuery)
    {
        $regex = str_replace('{%F:value%}', '(\d+(\.\d+)?)', $transform['transform_url']);

        if ($transform['tree'] == 's') {
            $field = explode('.', $transform['field']);
            $field = $field[1];
        } else {
            $field = $transform['field'];
        }

        foreach ($explodedQuery as $query) {
            preg_match('/' . $regex . '/', $query, $matches);

            if (is_numeric($matches[1])) {

                return $transform['tree'] . '[' . $transform['comparsion'] . ']' . '[' . $field . ']=' . $matches[1];
            }
        }


    }


    public function handleUrlTransformation($data, $property)
    {

        $catalog = xCore::moduleFactory('catalog.back');

        $catalog->_commonObj->_tree->cacheState(true);
        $catalog->_commonObj->_sku->cacheState(true);


        if ($data['comparsion'] == 'sort') {
            $hash = Common::generateHash($property['options']['srcGroupId'] . Common::getmicrotime());

            $catalog->_TMS->generateSection($data['transform_url'], $hash);

            $field = $data['tree'] . "[{$data[comparsion]}][{$data[field]}]";

            $typeItems = array('signed', 'float', 'char');
            $sortItems = array('asc', 'desc');

            foreach ($sortItems as $item) {
                foreach ($typeItems as $itemType) {
                    $elementValue = $item . '-' . $itemType;
                    $catalog->_TMS->addMassReplace($hash, array('value' => $elementValue));
                    $to = trim($catalog->_TMS->parseSection($hash));

                    if (!empty($to)) {
                        $output[] = array('rule_id' => $data['id'], 'from' => "$field=" . $elementValue, 'to' => $to);
                    }
                }
            }


            $catalog->_commonObj->clearFieldsUrlTransform($data['id']);

            return $output;
        }

        if (empty($data['value']) && !empty($data['transform_url'])) {


            if ($data['tree'] == 's') {
                $tree = $catalog->_commonObj->_sku;
                $field = $property['basic'];
            } else {
                $tree = $catalog->_tree;
                $field = $data['field'];
            }

            if (!$this->dataCatalogTree[$data['tree']]) {

                $dataCatalog = $tree->selectStruct('*')->selectParams('*')->where(array($field, '<>', ''))->run();

                if ($data['tree'] == 's') {
                    $this->dataCatalogTree[$data['tree']] = $catalog->_commonObj->skuHandleFront($dataCatalog);

                } else {
                    $this->dataCatalogTree[$data['tree']] = $catalog->_commonObj->convertToPSGAll($dataCatalog, array('doNotExtractSKU' => true));
                }
            }


            if (!empty($this->dataCatalogTree[$data['tree']])) {

                $result = $this->dataCatalogTree[$data['tree']];

                $hash = Common::generateHash($property['options']['srcGroupId'] . Common::getmicrotime());

                $catalog->_TMS->generateSection($data['transform_url'], $hash);

                $fieldFull = $data['tree'] . "[{$data[comparsion]}][{$field}]";

                if ($data['multi']) {
                    $fieldFull .= '[]';
                }

                $fieldExploded = explode('.', $data['field']);

                $elementStack = [];

                foreach ($result as $element) {

                    if ($data['tree'] == 's') {
                        $elementValue = $element['params'][$field];

                    } else {

                        $elementValue = $element[$fieldExploded[0]][$fieldExploded[1]];
                    }

                    if (in_array($elementValue, $elementStack)) {
                        continue;

                    } else {

                        $elementStack[] = $elementValue;
                    }


                    $catalog->_TMS->addMassReplace($hash, array('object' => $element));

                    $to = trim($catalog->_TMS->parseSection($hash));


                    if (!empty($to)) {
                        $to = $this->safeUrlTransform($to);

                        $output[] = array('rule_id' => $data['id'], 'from' => "$fieldFull=" . $elementValue, 'to' => $to);
                    }

                }

                $catalog->_commonObj->clearFieldsUrlTransform($data['id']);
                return $output;
            }
        }


    }


    public function handleSearchFilterGetFilterInfo($matrix, &$field, $outerLink = false)
    {

        $item = catalogProperty::handleSearchFilterCreatePrototypeItem($field);

        $filterItem = $item['filterItem'];
        switch ($field['comparsionType']) {
            case  'equal' :


                foreach ($matrix as $key => $matrixItem) {

                    $filter = $item['item'];
                    $filter['value'] = $matrixItem['value'];
                    $filterItem->addArray($filter);

                    $matrix[$key]['_filter']['link'] = XRegistry::get('catalogFront')->createFilter($filterItem, !$field['useAsDirectLink'], $outerLink);
                    $matrix[$key]['_filter']['filterName'] = "{$filterItem->type}[{$filter[type]}][{$filter['property']}][]";
                    $matrix[$key]['_filter']['inFilter'] = XRegistry::get('catalogFront')->checkInFilter($filterItem->type, $filter['type'], $field['gpth'], $filter['value']);
                }

                break;


            case  'interval' :

                $minFilter = $item['item'];
                $minFilter['type'] = 'from';
                $minFilter['value'] = $matrix['min'];
                $filterItem->addArray($minFilter);

                $maxFilter = $item['item'];
                $maxFilter['type'] = 'to';
                $maxFilter['value'] = $matrix['max'];
                $filterItem->addArray($maxFilter);

                $field['_filter']['link'] = XRegistry::get('catalogFront')->createFilter($filterItem, !$field['useAsDirectLink'], $outerLink);
                $field['_filter']['filterNameMax'] = "{$filterItem->type}[to][{$maxFilter['property']}]";
                $field['_filter']['filterNameMin'] = "{$filterItem->type}[from][{$minFilter['property']}]";

                $field['_filter']['inFilterMax'] = XRegistry::get('catalogFront')->checkInFilter($filterItem->type, 'to', $field['gpth']);
                $field['_filter']['inFilterMin'] = XRegistry::get('catalogFront')->checkInFilter($filterItem->type, 'from', $field['gpth']);

                break;


            case 'sort':

                $filter = $item['item'];
                $filter['type'] = $field['comparsionType'];
                $filter['value'] = $field['sort'];
                $filter['override'] = true;


                if ($active = XRegistry::get('catalogFront')->checkInFilter($filterItem->type, $field['comparsionType'], $item['item']['property'])) {

                    if (strstr($active, $field['sort'])) {
                        $field['_filter']['active'] = true;

                    }

                }


                $filterItem->addArray($filter);
                $field['_filter']['link'] = XRegistry::get('catalogFront')->createFilter($filterItem, !$field['useAsDirectLink'], $outerLink);
                $field['_filter']['filterName'] = "{$filterItem->type}[{$filter[type]}][{$filter['property']}]";
                break;


        }

        return $matrix;

    }

}