<?php

use X4\Classes\XRegistry;


class connectionProperty extends catalogProperty
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }


    public function handleSearchFilterSet($matrix, $field, $paramSet)
    {

        $elementStack = [];
        $elementStackActive = [];

        if (!empty($matrix)) {

            foreach ($matrix as $element) {
                if (is_array($element['value'])) {
                    $elementStack = array_merge($elementStack, $element['value']);
                    if ($element['_filter']['active']) {
                        $elementStackActive = array_merge($elementStackActive, $element['value']);
                    }
                }


            }


            if (isset($elementStack)) {
                $elementStack = array_unique($elementStack);
            }
            if (isset($elementStackActive)) {
                $elementStackActive = array_unique($elementStackActive);
            }


            $selectorElements = XRegistry::get('catalogFront')->_commonObj->_tree->selectStruct('*')->selectParams('*')->where(array('@id', '=', $elementStack))->run();


            if (!empty($selectorElements)) {

                $selectorElements = XRegistry::get('catalogFront')->_commonObj->convertToPSGAll($selectorElements);
                $exitMatrix = [];
                $j = 0;
                foreach ($selectorElements as $value) {

                    $exitMatrix[$j]['value'] = $value;

                    $exitMatrix[$j]['_filter']['counter'] = $field['_filter']['counter'][$value['_main']['id']];

                    $exitMatrix[$j]['_filter']['counterActive'] = $field['_filter']['counterActive'][$value['_main']['id']] ? $field['_filter']['counterActive'][$value['_main']['id']] : $field['_filter']['counter'][$value['_main']['id']];

                    if (in_array($value['_main']['id'], $elementStackActive)) {
                        $exitMatrix[$j]['_filter']['active'] = 1;
                    }

                    $j++;

                }

                return $exitMatrix;

            }

        }

    }


    public function handleSearchFilterValue($value, $options, $field)
    {
        $gradedValue = null;
        if (is_array($value)) {
            if (isset($value['_main']['id'])) {
                $gradedValue[] = $value['_main']['id'];
            } else {
                foreach ($value as $v) {
                    if (isset($v['_main']['id'])) {
                        $gradedValue[] = $v['_main']['id'];
                    }

                }

            }
            return $gradedValue;

        } elseif ($value) {
            return json_decode($value, true);
        }
    }


    public function handleSearchFilterGetFilterInfo($matrix, &$field, $outerLink = false)
    {
        $item = catalogProperty::handleSearchFilterCreatePrototypeItem($field);

        $filterItem = $item['filterItem'];


        if ($field['comparsionType'] == 'equal') {
            foreach ($matrix as $key => $matrixItem) {

                $filterItem->clear();
                $filterItem = $item['filterItem'];


                $filter = $item['item'];
                $filter['type'] = 'like';
                $filter['value'] = $matrixItem['value']['_main']['id'];
                $filterItem->addArray($filter);

                $matrix[$key]['_filter']['link'] = XRegistry::get('catalogFront')->createFilter($filterItem, !$field['useAsDirectLink'], $outerLink);
                $matrix[$key]['_filter']['filterName'] = "{$filterItem->type}[{$filter[type]}][{$filter['property']}][]";

                $matrix[$key]['_filter']['inFilter'] = XRegistry::get('catalogFront')->checkInFilter($filterItem->type, $filter['type'], $filter['property'], $filter['value']);

            }


        }

        return $matrix;

    }


    public function handleOnBeforeImport($key, $columns, $importContext)
    {
        $tableName = $importContext->import->processor->tableName;

        $query = "select `$key` from `$tableName` group by `$key`";
        $options = $columns[$key];
        $catalog = XRegistry::get('catalogBack');
        $result = $this->_PDO->query($query);

        $importContext->import->processor->addColumnCopy('old.' . $key, $key);

        if ($connections = $result->fetchAll(PDO::FETCH_COLUMN, 0)) {
            $connections = array_filter($connections);
            $connectionsStack = [];
            foreach ($connections as $connect) {
                $subConnections = explode(',', $connect);
                $subConnections = array_map('trim', $subConnections);
                $connectionsExploded[$connect] = $subConnections;
                $connectionsStack = array_merge($connectionsStack, $subConnections);
            }


            if (count($connectionsStack) > 0) {

                $promt = $catalog->_commonObj->_tree->selectStruct('*')->selectParams('*')->where(array
                (
                    $options['property'],
                    '=',
                    $connectionsStack
                ))->format('paramsval', $options['property'], 'id');


                if ($options['folderId']) {
                    $promt->childs($options['folderId']);
                }

                $existedConnections = $promt->run();


                if (!empty($existedConnections)) {
                    foreach ($connectionsExploded as $connKey => $oneConnect) {
                        foreach ($oneConnect as $connect) {
                            if ($existedConnections[$connect]) {
                                $outputConnections[$connKey][] = $existedConnections[$connect];
                            }
                        }
                        $outputConnections[$connKey] = json_encode($outputConnections[$connKey]);
                    }

                }


                if (isset($outputConnections)) {
                    foreach ($outputConnections as $id => $element) {

                        $query = "update `$tableName` set `{$key}`='{$element}'   where `{$key}`='{$id}' ";
                        $this->_PDO->exec($query);

                    }

                }
            }
        }
    }


    public function handleTypeFront($value = null, $property = null, $object = null, $setName = null)
    {
        if (!is_array($value) && is_string($value)) {
            $json = json_decode($value, true);
        }

        if ($json) {
            $value = $json;
        }

        $catalog = XRegistry::get('catalogBack');

        $resultEvent = $this->_EVM->fire('catalog.property.connection:beforeHandleTypeFront', array('instance' => $this, 'object' => $object, 'property' => $property, 'value' => $value));

        if (!empty($resultEvent['value'])) {
            $value = $resultEvent['value'];
        }

        if ($property['options']['calculate'] && is_array($value)) {


            $objects = $catalog->_tree->selectStruct('*')->selectParams('*')->where(array('@id', '=', $value))->run();

            if (!empty($objects)) {

                $objects = $catalog->_commonObj->convertToPSGAll($objects);
            }

            return $objects;

        }

        return $value;
    }

    public function handleTypeBack($property, $value = null)
    {
        return $property;
    }

    public function handleTypeOnEdit($property, $value, $object)
    {
        $catalog = XRegistry::get('catalogBack');

        if (is_array($value)) {
            if ($nodesPath = $catalog->_commonObj->_tree->selectStruct(array('id'))->getParamPath('Name')->where(['@id', '=', $value])->run()) {
                $value = [];

                foreach ($nodesPath as $node) {
                    $value[] = array
                    (
                        'sid' => $node['id'],
                        'name' => $node['paramPathValue']
                    );
                }

            } else {
                return false;
            }

            return $value;
        }

    }
}