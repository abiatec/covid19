<?php

use X4\Classes\XRegistry;


class imageProperty extends catalogProperty
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function handleOnImport($value, $row, $rowName, $schemeRowName, $columns, $psetData, $oldRow)
    {
        $options = $columns[$schemeRowName];
        foreach ($columns as $keyRow => $val) {
            $replacment[] = '{' . $val['realName'] . '}';
            $replacer[] = $oldRow[$keyRow];
        }

        if ($folder = $options['getFirstFromFolder']) {
            $shortFolder = str_replace($replacment, $replacer, $folder);
            $folder = PATH_ . $shortFolder;
            if (is_dir($folder)) {
                foreach (new DirectoryIterator($folder) as $file) {
                    if ($file->isFile()) {
                        $value = '/' . $shortFolder . $file->getFilename();

                    }
                }
            }

        } elseif ($image = $options['image']) {

            $value = str_replace($replacment, $replacer, $image);
        }

        $row[$rowName] = $value;
        return $row;

    }


    public function handleTypeFront($value = null, $property = null, $object = null, $setName = null)
    {

        if ($property['options']['width'] || $property['options']['height']) {

            if (Common::isFileExists($value)) {
                $value = XRegistry::get('ENHANCE')->imageTransform(array('r' => array
                (
                    'w' => $property['options']['width'],
                    'h' => $property['options']['height'],
                    'r' => $property['options']['proportions']

                )), array('value' => $value));

            } else {
                $value = '';
            }
        }

        return $value;

    }


    public function onListingView($value, $propertyInfo, $clmn)
    {
        $catalog = XRegistry::get('catalogBack');

        if (Common::isFileExists($value)) {
            $value = XRegistry::get('ENHANCE')->imageTransform(array('r' => array
            (
                'w' => $catalog->_config['imageListingSizeWidth'],
                'h' => $catalog->_config['imageListingSizeHeight']

            )), array('value' => $value));
        } else {
            $value = '';
        }

        return $value;
    }
}