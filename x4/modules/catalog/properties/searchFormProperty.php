<?php

use X4\Classes\XRegistry;


class searchFormProperty extends catalogProperty
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function handleTypeBack($property, $value = null)
    {

        $catalog = XRegistry::get('catalogBack');

        if ($childs = $catalog->_commonObj->_searchForms->selectStruct(array('id'))->selectParams('*')->childs(1, 1)->format('valparams', 'id', 'Name')->run()) {

            $property['defaultValues'] = XHTML::arrayToXoadSelectOptions($childs, false, true);

        }

        return $property;
    }


}