<?php

class stockProperty extends catalogProperty
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }


    public function handleTypeFront($value = null, $property = null, $object = null, $setName = null)
    {
        if (!empty($object['params'])) {
            if ($object['obj_type'] != '_CATOBJ') {
                $path = $property['basic'];
            } else {
                $path = $setName . '.' . $property['basic'];
            }

            $stocks = [];
            foreach ($object['params'] as $key => $value) {
                if (strstr($key, $path . '__')) {
                    $store = explode('__', $key);
                    $stocks[$store[1]] = $value;
                }

            }

            return $stocks;
        }

    }

    public function handleTypeBack($property, $value = null)
    {

        $ishop = xCore::moduleFactory('ishop.back');

        $stockDataSource = $ishop->_commonObj->_models->Stock->getStocksList();

        $catalog = xCore::moduleFactory('catalog.back');

        $node = $catalog->_commonObj->_propertySetsTree->getNodeInfo($property['ancestor']);

        foreach ($stockDataSource as $row) {
            $stockData[$row['basic']] = array('innerId' => $row['id'], 'stockName' => $row['params']['Name'], 'pName' => $node['basic'] . '.' . $property['basic'], 'stockId' => $row['basic'], 'stockAddress' => $row['params']['storeAddress']);
        }


        $property['defaultValues'] = $stockData;


        return $property;
    }

    public function handleTypeOnEdit($property, $value, $object)
    {
        $ishop = xCore::moduleFactory('ishop.back');

        return  $ishop->_commonObj->_models->Stock->getStockAgregated($object['id']);

    }
}