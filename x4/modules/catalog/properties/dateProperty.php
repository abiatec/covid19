<?php

use X4\Classes\XRegistry;


class dateProperty extends catalogProperty
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function handleTypeBack($property, $value = null)
    {
        return $property;

    }


    public function handleSearchFilterSorting($matrix, $field)
    {

        $sorting = XARRAY::asKeyVal($matrix, 'value');
        if ($field['sort'] == 'asc') {
            asort($sorting);
        } else {

            arsort($sorting);
        }
        foreach ($sorting as $key => $sortItem) {
            $sorted[$key] = $matrix[$key];

        }
        return $sorted;
    }

    public function handleSearchFilterGetFilterInfo($matrix, &$field, $outerLink = false)
    {

        $item = catalogProperty::handleSearchFilterCreatePrototypeItem($field);

        $filterItem = $item['filterItem'];
        switch ($field['comparsionType']) {
            case  'equal' :

                foreach ($matrix as $key => $matrixItem) {

                    $filter = $item['item'];
                    $filter['value'] = $matrixItem['value'];
                    $filterItem->addArray($filter);

                    $matrix[$key]['_filter']['link'] = XRegistry::get('catalogFront')->createFilter($filterItem, !$field['useAsDirectLink'], $outerLink);
                    $matrix[$key]['_filter']['filterName'] = "{$filterItem->type}[{$filter[type]}][{$filter['property']}][]";
                    $matrix[$key]['_filter']['inFilter'] = XRegistry::get('catalogFront')->checkInFilter($filterItem->type, $filter['type'], $field['gpth'], $filter['value']);
                }

                break;


            case  'interval' :

                $minFilter = $item['item'];
                $minFilter['type'] = 'from';
                $minFilter['value'] = $matrix['min'];
                $filterItem->addArray($minFilter);

                $maxFilter = $item['item'];
                $maxFilter['type'] = 'to';
                $maxFilter['value'] = $matrix['max'];
                $filterItem->addArray($maxFilter);

                $field['_filter']['link'] = XRegistry::get('catalogFront')->createFilter($filterItem, !$field['useAsDirectLink'], $outerLink);
                $field['_filter']['filterNameMax'] = "{$filterItem->type}[to][{$maxFilter['property']}]";
                $field['_filter']['filterNameMin'] = "{$filterItem->type}[from][{$minFilter['property']}]";

                $field['_filter']['inFilterMax'] = XRegistry::get('catalogFront')->checkInFilter($filterItem->type, 'to', $field['property']);
                $field['_filter']['inFilterMin'] = XRegistry::get('catalogFront')->checkInFilter($filterItem->type, 'from', $field['property']);

                break;


            case 'sort':


                $filter = $item['item'];
                $filter['type'] = $field['comparsionType'];
                $filter['value'] = $field['sort'];
                $filter['override'] = true;


                if ($active = XRegistry::get('catalogFront')->checkInFilter($filterItem->type, $field['comparsionType'], $item['item']['property'])) {


                    if (strstr($active, $field['sort'])) {
                        $field['_filter']['active'] = true;

                    }

                }


                $filterItem->addArray($filter);
                $field['_filter']['link'] = XRegistry::get('catalogFront')->createFilter($filterItem, !$field['useAsDirectLink'], $outerLink);
                $field['_filter']['filterName'] = "{$filterItem->type}[{$filter[type]}][{$filter['property']}]";
                break;


        }

        return $matrix;

    }


    public function handleTypeOnSave($property, $value, $paramSet, $paramPath)
    {

        return strtotime($value);

    }

    public function handleTypeOnEdit($property, $value, $object)
    {
        $catalog = XRegistry::get('catalogBack');
        return date($catalog->_config['dateListingFormat'], $value);
    }

    public function onListingView($value, $propertyInfo, $clmn)
    {
        $catalog = XRegistry::get('catalogBack');
        return date($catalog->_config['dateListingFormat'], $value);

    }
}