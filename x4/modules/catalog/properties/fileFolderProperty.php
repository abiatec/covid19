<?php

use X4\Classes\XRegistry;
use X4\Classes\XNameSpaceHolder;

class fileFolderProperty  extends catalogProperty
{
    public function __construct()
    {
        parent::__construct(__CLASS__);

        XNameSpaceHolder::addMethodsToNS('module.catalog.back', array('folderImageNavigate'), $this);

    }

    public function folderImageNavigate($params)
    {

        $catalog = XRegistry::get('catalogBack');

        $shortFolder = $params['folder'];
        $folder = PATH_ . $shortFolder;
        $this->result['files'] = null;

        if (is_dir($folder)) {
            foreach (new DirectoryIterator($folder) as $file) {
                if ($file->isFile()) {
                    $value = '/' . $shortFolder . $file->getFilename();

                    if (in_array('.' . $file->getExtension(), xConfig::get('GLOBAL', 'fileImagesExt'))) {
                        $this->result['files'][] = XRegistry::get('ENHANCE')->imageTransform(array('r' => array
                        (
                            'w' => $catalog->_config['imageListingSizeWidth'],
                            'h' => $catalog->_config['imageListingSizeHeight']

                        )), array('value' => $value));

                    }
                }
            }


        }
    }


    public function handleOnImport($value, $row, $rowName, $schemeRowName, $columns, $psetData, $oldRow)
    {

        $options = $columns[$schemeRowName];
        $folder = $options['fileFolder'];

        foreach ($columns as $keyRow => $val) {
            $replacment[] = '{' . $val['realName'] . '}';
            $replacer[] = $oldRow[$keyRow];
        }

        $shortFolder = str_replace($replacment, $replacer, $folder);

        $value = '/' . $shortFolder;

        $row[$rowName] = $value;
        return $row;

    }

}