<?php

use X4\Classes\XRegistry;


class connectionSKUProperty extends catalogProperty
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }


    public function handleTypeFront($value = null, $property = null, $object = null, $setName = null)
    {

        if ($property['options']['calculate'] && is_array($value)) {

            $catalog = XRegistry::get('catalogFront');
            $skuObjects = null;

            $sku = $catalog->_sku->selectStruct('*')->selectParams('*')->where(array('@id', '=', $value))->run();

            if (!empty($sku)) {

                $skuObjects = $catalog->_commonObj->skuHandleFront($sku);
            }

            return $skuObjects;

        }

        return $value;
    }


    public function handleOnBeforeImport($key, $columns, $importContext)
    {
        $tableName = $importContext->import->processor->tableName;

        $query = "select `$key` from `$tableName` group by `$key`";
        $options = $columns[$key];
        $catalog = XRegistry::get('catalogBack');
        $result = $this->_PDO->query($query);

        $importContext->import->processor->addColumnCopy('old.' . $key, $key);

        if ($connections = $result->fetchAll(PDO::FETCH_COLUMN, 0)) {
            $connections = array_filter($connections);
            $connectionsStack = [];
            foreach ($connections as $connect) {
                $subConnections = explode(',', $connect);
                $subConnections = array_map('trim', $subConnections);
                $connectionsExploded[$connect] = $subConnections;
                $connectionsStack = array_merge($connectionsStack, $subConnections);
            }


            if (count($connectionsStack) > 0) {

                $existedConnections = $catalog->_commonObj->_sku->selectStruct('*')->selectParams('*')->where(array
                (
                    $options['property'], '=', $connectionsStack
                ))->format('paramsval', $options['property'], 'id')->run();


                if ($existedConnections) {
                    foreach ($connectionsExploded as $connKey => $oneConnect) {
                        foreach ($oneConnect as $connect) {
                            if ($existedConnections[$connect]) {
                                $outputConnections[$connKey][] = $existedConnections[$connect];
                            }

                        }
                        $outputConnections[$connKey] = json_encode($outputConnections[$connKey]);
                    }

                }


                if (isset($outputConnections)) {
                    foreach ($outputConnections as $id => $element) {

                        $query = "update `$tableName` set `{$key}`='{$element}'   where `{$key}`='{$id}' ";
                        $this->_PDO->exec($query);

                    }

                }

            }

        }

    }


    public function handleTypeOnEdit($property, $value, $object)
    {

        $catalog = XRegistry::get('catalogBack');

        if (is_array($value)) {
            if ($nodesSku = $catalog->_commonObj->_sku->selectStruct('*')->selectParams(array('Name'))->where(array
            (
                '@id',
                '=',
                $value
            ))->format('keyval', 'id')->run()
            ) {


                $catObjConnection = XARRAY::arrToLev($nodesSku, 'netid', 'params', 'Name');
                if ($nodesObj = $catalog->_commonObj->_tree->selectStruct(array('id'))->getParamPath('Name')->where(array
                (
                    '@id',
                    '=',
                    array_keys($catObjConnection)
                ))->run()
                ) ;


                $nodesObj = XARRAY::arrToKeyArr($nodesObj, 'id', 'paramPathValue');


                foreach ($value as $node) {
                    $exvalue[] = array
                    (
                        'sid' => $node,
                        'name' => $nodesObj[$nodesSku[$node]['netid']] . '/[' . $nodesSku[$node]['params']['Name'] . ']'
                    );
                }
            }
        }

        return $exvalue;
    }
}