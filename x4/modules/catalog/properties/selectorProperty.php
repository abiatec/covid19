<?php

use X4\Classes\XRegistry;


class selectorProperty extends catalogProperty
{

    public function __construct()
    {
        parent::__construct(__CLASS__);
    }


    public function handleUrlTransformation($data, $property)
    {

        $catalog = xCore::moduleFactory('catalog.back');

        if (empty($data['value'])) {


            $dataCatalog = $catalog->_tree->selectStruct('*')->selectParams('*')->childs((int)$property['options']['srcGroupId'], 1)->run();
            $result = $catalog->_commonObj->convertToPSGAll($dataCatalog, array('doNotExtractSKU' => true));


            if (!empty($result)) {

                $hash = Common::generateHash($property['options']['srcGroupId'] . Common::getmicrotime());

                $catalog->_TMS->generateSection($data['transform_url'], $hash);

                $fieldExploded = explode('.', $data['field']);

                if ($data['tree'] == 'f') {
                    $field = $data['tree'] . "[{$data[comparsion]}][{$data[field]}]";

                } else {
                    $field = $data['tree'] . "[{$data[comparsion]}][{$fieldExploded[1]}]";
                }


                foreach ($result as $element) {

                    $catalog->_TMS->addMassReplace($hash, array('object' => $element));
                    $to = trim($catalog->_TMS->parseSection($hash));

                    if (!empty($to)) {
                        $to = $this->safeUrlTransform($to);
                        $output[] = array('rule_id' => $data['id'], 'from' => "$field=" . $element['_main']['id'], 'to' => $to);

                        if ($data['multi']) {
                            $output[] = array('rule_id' => $data['id'], 'from' => "{$field}[]=" . $element['_main']['id'], 'to' => $to);
                        }
                    }

                }

                $catalog->_commonObj->clearFieldsUrlTransform($data['id']);
                return $output;
            }
        }


    }

    public function handleSearchFilterSorting($matrix, $field)
    {


        if (empty($field['sortKey'])) {
            $field['sortKey'] = '_main.Name';
        }

        $sortKeyExpl = explode('.', $field['sortKey']);

        $sorting = XARRAY::asKeyVal($matrix, 'value');


        foreach ($sorting as $k => $sortItem) {
            $sortingItems[$k] = $sortItem[$sortKeyExpl[0]][$sortKeyExpl[1]];
        }

        if ($field['sort'] == 'asc') {
            asort($sortingItems);

        } else {

            arsort($sortingItems);
        }


        foreach ($sortingItems as $key => $sortItem) {
            $sorted[$key] = $matrix[$key];

        }
        return $sorted;
    }

    public function buildFacet($facet, $field)
    {
        if (!empty($facet['facet'])) {

            $innerSpace = $facetStack = [];

            foreach ($facet['facet'] as $key => $values) {
                $values = json_decode($values, true);

                $facetStack = array_merge($facetStack, $values);

                if (!empty($values)) {
                    foreach ($values as $value) {
                        $innerSpace[$value] = $facet['space'][$key];
                    }
                }

            }

            if (!empty($facetStack)) {

                $facetStack = XRegistry::get('catalogBack')->_tree->selectStruct('*')->selectParams('*')->where(array('@id', '=', array_unique($facetStack)))->run();
                return array('facet' => XRegistry::get('catalogBack')->_commonObj->convertToPSGAll($facetStack), 'space' => $innerSpace);

            }
        }
    }

    public function handleTypeBack($property, $value = null)
    {
        $catalog = XRegistry::get('catalogBack');

        if ($id = $property['options']['srcGroupId']) {
            $property['defaultValues'] = $catalog->_commonObj->_tree->selectStruct(array('id'))->selectParams(
                array('Name'))->childs($id, 1)->format('valparams', 'id',
                'Name')->run();
            $property['defaultValues'] = XHTML::arrayToXoadSelectOptions($property['defaultValues']);

        } elseif ($deep = $property['options']['deep'] && $deepBasic = $property['options']['deepBasic']) {

            $itemId = $catalog->_commonObj->_tree->selectStruct(array('id'))->childs($value['id'], (int)$deep)->where(array('@basic', '=', $deepBasic))->run();

            if (!empty($itemId)) {
                $property['defaultValues'] = $catalog->_commonObj->_tree->selectStruct(array('id'))->selectParams(array('Name'))->childs($itemId[0]['id'], 1)->format('valparams', 'id', 'Name')->run();


                $property['defaultValues'] = XHTML::arrayToXoadSelectOptions($property['defaultValues']);
            }

        }

        return $property;
    }

    public function onListingView($values, $name, $clmn)
    {
        static $nameCollector = [];

        $values = json_decode($values, true);

        if (isset($values)) {
            if (!empty($nameCollector)) {
                $grabFromDb = array_diff($values, array_keys($nameCollector));
            } else {
                $grabFromDb = $values;
            }

            $catalog = XRegistry::get('catalogBack');

            if (!empty($grabFromDb)) {
                $dta = $catalog->_tree->selectParams(array('Name'))->where(array
                (
                    '@id',
                    '=',
                    $grabFromDb
                ))->format('valparams', 'id', 'Name')->run();

                if (isset($dta) && is_array($dta)) {
                    $nameCollector = $nameCollector + $dta;
                }
            }

            foreach ($values as $val) {
                $nc[] = $nameCollector[$val];
            }

            return implode(',', $nc);
        }
    }


    public function handleOnBeforeImport($key, $columns, $importContext)
    {
        $tableName = $importContext->import->processor->tableName;

        $query = "select `$key` from `$tableName` group by `$key`";
        $options = $columns[$key];
        $catalog = xRegistry::get('catalogBack');
        $result = $this->_PDO->query($query);

        if (empty($options['separator'])) {
            $options['separator'] = ',';
        }

        $importContext->import->processor->addColumnCopy('old.' . $key, $key);

        if ($selectors = $result->fetchAll(PDO::FETCH_COLUMN, 0)) {


            $initialSelectors = array_filter($selectors);
            $selectorStack = [];

            if (!empty($initialSelectors)) {

                foreach ($initialSelectors as &$selector) {
                    $separated = explode($options['separator'], $selector);
                    $separated = array_map('trim', $separated);
                    $selectorStack = array_merge($selectorStack, $separated);
                    $selectorHash[md5($selector)] = $separated;
                }

                $selectorStack = array_unique($selectorStack);

                $existedSelectors = $catalog->_tree->selectStruct('*')->selectParams('*')->childs($options['source'], 1)->where(array
                (
                    $options['key'],
                    '=',
                    $selectorStack
                ))->format('paramsval', $options['key'], 'id')->run();


                foreach ($selectorStack as $selectorObject) {
                    if (!isset($existedSelectors[$selectorObject])) {
                        $paramSet = array('PropertySetGroup' => $options['PropertySetGroup']);
                        $paramSet[$options['key']] = $selectorObject;

                        $objId = $catalog->_tree->initTreeObj($options['source'], '%SAMEASID%', '_CATOBJ', $paramSet);

                        $map[$selectorObject] = $objId;

                    } else {
                        $map[$selectorObject] = $existedSelectors[$selectorObject];

                    }

                }


                foreach ($selectorHash as &$item) {
                    $parts = [];

                    foreach ($item as $part) {

                        $parts[] = $map[$part];
                    }

                    $item = json_encode($parts);
                }


                if (isset($selectorHash)) {
                    foreach ($initialSelectors as $id => $element) {
                        $element = str_replace('\'', "''", $element);
                        $id = $selectorHash[md5($element)];
                        $query = "update `$tableName` set `{$key}`='{$id}'   where `{$key}`='{$element}' ";
                        $this->_PDO->exec($query);

                    }

                }


            }

        }

    }


    public function handleTypeFront($value = null, $property = null, $object = null, $setName = null)
    {
        static $chosenSelectors;

        if ($value) {
            if (is_array($value)) {
                $json = $value;
            } else {
                $json = json_decode($value, true);
            }

            if (!empty($json)) {
                $groupId = $property['options']['srcGroupId'];
                $catalog = xCore::moduleFactory('catalog.front');

                if ($deep = $property['options']['deep'] && $deepBasic = $property['options']['deepBasic']) {

                    $itemId = $catalog->_commonObj->_tree->selectStruct(array('id'))->singleResult()->childs($object['id'], (int)$deep)->where(array('@basic', '=', $deepBasic))->run();
                    $groupId = $itemId['id'];
                }

                if (!$chosenSelectors[$groupId] && isset($groupId) && $groupId) {

                    $chosenSelectors[$groupId] = $catalog->_tree->selectStruct('*')->selectParams('*')->where(array
                    ('@ancestor', '=', (int)$groupId))->format('keyval', 'id')->run();
                    if ($chosenSelectors[$groupId]) {

                        $chosenSelectorsNew = [];
                        foreach ($chosenSelectors[$groupId] as $valKey => $val) {
                            $val = $catalog->_commonObj->convertToPSG($val);
                            $valKey = '0' . $valKey;
                            $chosenSelectorsNew[$groupId][$valKey] = $val;
                        }

                        $chosenSelectors = $chosenSelectorsNew;
                    }
                }

                if (count($json) == 1) {
                    if (is_array($json)) {
                        return $chosenSelectors[$groupId]['0' . $json[0]];
                    } else {
                        return $chosenSelectors[$groupId]['0' . $json];
                    }
                } else {
                    foreach ($chosenSelectors[$groupId] as $selectorKey => $selectorValue) {
                        if (in_array((int)$selectorKey, $json)) {
                            if ($property['options']['assoc'] && is_array($value)) {
                                $result[$selectorKey] = $chosenSelectors[$groupId][$selectorKey];
                            } else {
                                $result[] = $chosenSelectors[$groupId][$selectorKey];
                            }
                        }
                    }
                    return $result;
                }
            }
        }
    }

    public function handleOptions($options)
    {
        $catalog = XRegistry::get('catalogBack');

        if ($options['srcGroupId']) {
            $node = $catalog->_commonObj->_tree->selectStruct(
                array('id'))->getParamPath('Name')->where(array
            (
                '@id',
                '=',
                $options['srcGroupId']
            ))->run();

            $options['srcGroup'] = $node['paramPathValue'];
        }

        return $options;
    }


    public function handleSearchFilterValue($value, $options, $field)
    {

        if (is_array($value)) {
            if (isset($value['_main']['id'])) {
                $gradedValue[] = $value['_main']['id'];
            } elseif ($field['propertyData']['options']['onlyOne'] != '1') {
                foreach ($value as $v) {
                    if (isset($v['_main']['id'])) {
                        $gradedValue[] = $v['_main']['id'];
                    }

                }

            }

            return $gradedValue;

        } elseif ($value) {
            return json_decode($value, true);
        }
    }


    public function handleSearchFilterGetFilterInfo($matrix, &$field, $outerLink = false)
    {
        $item = catalogProperty::handleSearchFilterCreatePrototypeItem($field);

        $filterItem = $item['filterItem'];


        switch ($field['comparsionType']) {
            case  'equal' :
                foreach ($matrix as $key => $matrixItem) {

                    $filterItem->clear();
                    $filterItem = $item['filterItem'];


                    $filter = $item['item'];
                    $filter['type'] = 'like';
                    $filter['value'] = $matrixItem['value']['_main']['id'];
                    $filterItem->addArray($filter);


                    $matrix[$key]['_filter']['link'] = XRegistry::get('catalogFront')->createFilter($filterItem, !$field['useAsDirectLink'], $outerLink);
                    $matrix[$key]['_filter']['filterName'] = "{$filterItem->type}[{$filter[type]}][{$filter['property']}][]";

                    $matrix[$key]['_filter']['inFilter'] = XRegistry::get('catalogFront')->checkInFilter($filterItem->type, $filter['type'], $filter['property'], $filter['value']);

                }

                break;

        }

        return $matrix;

    }

    public function handleTypeOnSave($property, $value, $paramSet, $paramPath)
    {

        if (!empty($value) && $property['options']['onlyOne'] == 1) {

            $value = '["' . $value . '"]';
        }

        return $value;


    }


    public function handleSearchFilterSet($matrix, $field, $paramSet)
    {

        $elementStack = [];
        $elementStackActive = [];

        if (!empty($matrix)) {

            foreach ($matrix as $element) {
                if (is_array($element['value'])) {
                    $elementStack = array_merge($elementStack, $element['value']);
                    if ($element['_filter']['active']) {
                        $elementStackActive = array_merge($elementStackActive, $element['value']);
                    }
                }
            }


            if (!empty($elementStack)) {
                $elementStack = array_unique($elementStack);
            }
            if (!empty($elementStackActive)) {
                $elementStackActive = array_unique($elementStackActive);
            }


            $selectorElements = XRegistry::get('catalogFront')->_commonObj->_tree->selectStruct('*')->selectParams('*')->where(array('@id', '=', $elementStack))->run();


            if (!empty($selectorElements)) {

                $selectorElements = XRegistry::get('catalogFront')->_commonObj->convertToPSGAll($selectorElements);
                $exitMatrix = [];
                $j = 0;
                foreach ($selectorElements as $value) {

                    $exitMatrix[$j]['value'] = $value;

                    $exitMatrix[$j]['_filter']['counter'] = $field['_filter']['counter'][$value['_main']['id']];

                    $exitMatrix[$j]['_filter']['counterActive'] = $field['_filter']['counterActive'][$value['_main']['id']] ? $field['_filter']['counterActive'][$value['_main']['id']] : $field['_filter']['counter'][$value['_main']['id']];

                    if (in_array($value['_main']['id'], $elementStackActive)) {
                        $exitMatrix[$j]['_filter']['active'] = 1;
                    }

                    $j++;

                }

                return $exitMatrix;

            }

        }

    }
}
