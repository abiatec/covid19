<?php

use X4\Classes\XRegistry;


class checkboxProperty extends catalogProperty
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }


    public function handleUrlTransformation($data, $property)
    {

        $catalog = XRegistry::get('catalogBack');

        if (empty($data['value']) && !empty($data['transform_url'])) {
            $dataCatalog = $catalog->_tree->selectStruct('*')->selectParams('*')->childs(1)->where(array($data['field'], '<>', ''))->run();

            if (!empty($dataCatalog)) {
                $result = $catalog->_commonObj->convertToPSGAll($dataCatalog, array('doNotExtractSKU' => true));

                $hash = Common::generateHash($property['options']['srcGroupId'] . Common::getmicrotime());

                $catalog->_TMS->generateSection($data['transform_url'], $hash);

                $field = $data['tree'] . "[{$data[comparsion]}][{$data[field]}]";

                if ($data['multi']) {
                    $field .= '[]';
                }

                $fieldExploded = explode('.', $data['field']);

                $elementStack = [];

                foreach ($result as $element) {
                    $elementValue = $element[$fieldExploded[0]][$fieldExploded[1]];

                    if (in_array($elementValue, $elementStack)) {
                        continue;

                    } else {

                        $elementStack[] = $elementValue;
                    }

                    $catalog->_TMS->addMassReplace($hash, array('object' => $element));

                    $to = trim($catalog->_TMS->parseSection($hash));


                    if (!empty($to)) {
                        $to = $this->safeUrlTransform($to);

                        $output[] = array('rule_id' => $data['id'], 'from' => "$field=" . $elementValue, 'to' => $to);
                    }

                }


                $catalog->_commonObj->clearFieldsUrlTransform($data['id']);
                return $output;
            }
        }


    }

    public function handleSearchFilterGetFilterInfo($matrix, &$field, $outerLink = false)
    {

        $item = catalogProperty::handleSearchFilterCreatePrototypeItem($field);

        $filterItem = $item['filterItem'];
        switch ($field['comparsionType']) {
            case  'equal' :


                foreach ($matrix as $key => $matrixItem) {

                    $filter = $item['item'];
                    $filter['value'] = $matrixItem['value'];
                    $filterItem->addArray($filter);

                    $matrix[$key]['_filter']['link'] = XRegistry::get('catalogFront')->createFilter($filterItem, !$field['useAsDirectLink'], $outerLink);
                    $matrix[$key]['_filter']['filterName'] = "{$filterItem->type}[{$filter[type]}][{$filter['property']}][]";
                    $matrix[$key]['_filter']['inFilter'] = XRegistry::get('catalogFront')->checkInFilter($filterItem->type, $filter['type'], $field['gpth'], $filter['value']);
                }

                break;

        }

        return $matrix;

    }

}