<?php

use X4\Classes\XRegistry;


class currencyIshopProperty extends catalogProperty
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function handleRegularReverseUrlTransformation($transform, $explodedQuery)
    {
        $regex = str_replace('{%F:value%}', '(\d+(\.\d+)?)', $transform['transform_url']);

        $field = explode('.', $transform['field']);
        foreach ($explodedQuery as $query) {
            preg_match('/' . $regex . '/', $query, $matches);

            if (is_numeric($matches[1])) {
                return $transform['tree'] . '[' . $transform['comparsion'] . ']' . '[' . $field[1] . ']=' . $matches[1];
            }
        }


    }

    public function handleTypeBack($property, $value = null)
    {
        $ishop = xCore::moduleFactory('ishop.back');

        if ($property['defaultValues'] = $ishop->_commonObj->_models->Currencies->getCurrenciesList()) {

            $property['defaultValues'] = XHTML::arrayToXoadSelectOptions($property['defaultValues']);
        }
        if ($property['options']['units']) {
            $property['options']['units'] = explode(',', $property['options']['units']);
        }
        return $property;
    }


    public function prepareFacet($fieldName, $object, $field, &$matrix)
    {
        if ($matrix[$fieldName]['facet']['min'] === null) {
            $matrix[$fieldName]['facet']['min'] = $object['params'][$fieldName];
        }

        if ($object['params'][$fieldName] < $matrix[$fieldName]['facet']['min']) {
            $matrix[$fieldName]['facet']['min'] = $object['params'][$fieldName];
        }

        if ($object['params'][$fieldName] > $matrix[$fieldName]['facet']['max']) {
            $matrix[$fieldName]['facet']['max'] = $object['params'][$fieldName];
        }

    }


    public function handleOnImport($value, $row, $rowName, $schemeRowName, $columns, $psetData, $oldRow)
    {
        static $currenciesList;

        if (empty($currenciesList)) {
            $ishop = xCore::moduleFactory('ishop.back');
            $currencies = $ishop->_commonObj->_models->Currencies->getCurrenciesList();
            $currencies = array_flip($currencies);
        }

        $optionsData = $columns[$schemeRowName];

        $value = str_replace(array(' ', ','), array('', ''), $value);

        if ($optionsData['option']) {
            $option = $optionsData['option'];

            if ($option == 'currency') {
                $value = $currencies[$value];
            }

            $row[$rowName . '__' . $option] = $value;
        } else {

            $row[$rowName] = $value;
        }

        return $row;

    }


    public function handleSearchFilterValue($value, $options, $field)
    {
        if (is_array($value)) {
            return $value['nonDiscountedValue'];
        } else {
            $field['basic'] = $field['property'];
            $object['params'] = $options;
            $object['obj_type'] = '_CATOBJ';
            $value = $this->handleTypeFront($value, $field, $object, $field['propertySet']);
            return $value['nonDiscountedValue'];
        }
    }


    public function handleSearchFilterGetFilterInfo($matrix, &$field, $outerLink = false)
    {
        $item = catalogProperty::handleSearchFilterCreatePrototypeItem($field);

        $filterItem = $item['filterItem'];
        switch ($field['comparsionType']) {
            case  'interval' :

                $minFilter = $item['item'];
                $minFilter['type'] = 'from';
                $minFilter['value'] = $matrix['min']['nonDiscountedValue'];
                $filterItem->addArray($minFilter);
                $maxFilter = $item['item'];
                $maxFilter['type'] = 'to';
                $maxFilter['value'] = $matrix['max']['nonDiscountedValue'];
                $filterItem->addArray($maxFilter);


                $field['_filter']['link'] = XRegistry::get('catalogFront')->createFilter($filterItem, !$field['useAsDirectLink'], $outerLink);
                $field['_filter']['filterNameMax'] = "{$filterItem->type}[priceto][{$maxFilter['property']}]";
                $field['_filter']['filterNameMin'] = "{$filterItem->type}[pricefrom][{$minFilter['property']}]";

                //todo her the problem with price settings $setName .$field['property']
                $field['_filter']['inFilterMax'] = XRegistry::get('catalogFront')->checkInFilter($filterItem->type, 'priceto', $field['property']);
                $field['_filter']['inFilterMin'] = XRegistry::get('catalogFront')->checkInFilter($filterItem->type, 'pricefrom', $field['property']);

                break;


            case 'sort':

                $item['item']['property'] = $item['item']['property'] . '__in__' . $_SESSION['currency']['basic'];
                $filter = $item['item'];
                $filter['override'] = true;
                $filter['type'] = $field['comparsionType'];
                $filter['value'] = $field['sort'];


                if ($active = XRegistry::get('catalogFront')->checkInFilter($filterItem->type, $field['comparsionType'], $item['item']['property'])) {

                    if ($active == $field['sort']) {
                        $field['_filter']['active'] = true;

                    }

                }

                $filterItem->addArray($filter);

                $field['_filter']['link'] = XRegistry::get('catalogFront')->createFilter($filterItem, !$field['useAsDirectLink'], $outerLink);

                break;

        }

        return $matrix;

    }

    public function toCurrency($value, $fromCurrency, $toCurrency)
    {
        return ($fromCurrency / $toCurrency) * $value;
    }

    public function tierHandle()
    {

    }

    public function handlePricing($priceObject, $basicPath, $value = null, $tiers = null)
    {

        static $currencyList;
        static $mainCurrency;

        $ishop = xCore::moduleFactory('ishop.back');

        $mainCurrency = $ishop->_commonObj->getCurrentCurrency();

        if (empty($currencyList)) {
            $currencyList = $ishop->_commonObj->_models->Currencies->getCurrenciesList(true);
        }

        if (empty($value)) {
            $value = $priceObject[$basicPath];
        }

        $svalue = [];

        $currencyId = $priceObject[$basicPath . '__currency'];
        $svalue['currency'] = $currencyList[$currencyId];
        $svalue['mainCurrency'] = current($mainCurrency);
        $svalue['realValueBeforeDiscount'] = $value;
        $svalue['value'] = $this->toCurrency($value, $svalue['currency']['rate'], $svalue['mainCurrency']['rate']);
        $svalue['nonDiscountedValue'] = $svalue['value'];

        $svalue['discounted'] = false;

        if ($discount = (float)$priceObject[$basicPath . '__discount']) {
            $value = $discount;
            $discountInCurrentCurrency = $this->toCurrency($discount, $svalue['currency']['rate'], $svalue['mainCurrency']['rate']);
            $svalue['discountPercent'] = round((100 * ($svalue['value'] - $discountInCurrentCurrency) / $svalue['value']));
            $svalue['discounted'] = true;
            $svalue['value'] = $discountInCurrentCurrency;

        }

        $svalue['currency'] = $currencyList[$currencyId];
        $svalue['mainCurrency'] = current($mainCurrency);
        $svalue['realValue'] = $value;

        $svalue['nonDiscountedValueFormatted'] = number_format($svalue['nonDiscountedValue'], $svalue['mainCurrency']['divider'], $svalue['mainCurrency']['separator'], ' ');
        $svalue['valueFormatted'] = number_format($svalue['value'], $svalue['mainCurrency']['divider'], $svalue['mainCurrency']['separator'], ' ');

        if ($tiers) {
            $svalue['tier'] = $tiers;
        }

        $sv = $this->_EVM->fire('catalog.property.currencyIshopProperty:afterHandleTypeFront', array('instance' => $this, 'value' => $svalue));

        if (!empty($sv)) {
            $svalue = $sv;
        }

        return $svalue;

    }

    public function handleTypeFront($value = null, $property = null, $object = null, $setName = null)
    {

        if ($object['obj_type'] != '_CATOBJ') {
            $basicPath = $property['basic'];

        } else {
            $basicPath = $setName . '.' . $property['basic'];
        }

        $resultEvent = $this->_EVM->fire('catalog.property.currencyIshopProperty:beforeHandleTypeFront',
            array('instance' => $this,
                'basicPath' => $basicPath,
                'object' => $object,
                'property' => $property,
                'value' => $value));

        if (!empty($resultEvent)) {
            $object = $resultEvent['object'];
            $value = $resultEvent['value'];
        }

        if (!empty($object['params'][$basicPath . '__tier'])) {
            $tiers = $object['params'][$basicPath . '__tier'] = json_decode($object['params'][$basicPath . '__tier'], true);
            foreach ($tiers as &$tier) {
                $tier = $this->handlePricing($tier, $basicPath);
            }
        }


        return $this->handlePricing($object['params'], $basicPath, $value, $tiers);
    }
}