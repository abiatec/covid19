<?php


class fileProperty extends catalogProperty
{
    public function __construct()
    {
        parent::__construct(__CLASS__);
    }

    public function onListingView($value, $propertyInfo, $clmn)
    {
        return $value;
    }


    public function handleOnImport($value, $row, $rowName, $schemeRowName, $columns, $psetData, $oldRow)
    {

        $options = $columns[$schemeRowName];

        foreach ($columns as $keyRow => $val) {
            $replacment[] = '{' . $val['realName'] . '}';
            $replacer[] = $oldRow[$keyRow];
        }

        if ($file = $options['file']) {

            $value = str_replace($replacment, $replacer, $file);
        }

        $row[$rowName] = $value;
        return $row;

    }

}