<?php

class showFilterApplyCatalogAction extends xAction
{

    public function run($params)
    {
        $this->loadModuleTemplate($params['params']['Template']);

        $section = 'showFilterApplyCatalog';

        $filter['applyFilterOnSku'] = $params['params']['applyFilterOnSku'];

        if (!empty($params['params']['filter'])) {

            $filter['filterPack'] = json_decode($params['params']['filter'], true);

            if ($filter['filterPack']['onpage']) {
                $filter['onpage'] = $filter['filterPack']['onpage'];
                unset($filter['filterPack']['onpage']);

            }

            $filter['serverPageDestination'] = $this->createPageDestination($params['params']['DestinationPage']);
            $pages = xCore::loadCommonClass('pages');

            if ($module = $pages->getModuleByAction($params['params']['DestinationPage'], 'showCatalogServer')) {
                $filter['showBasicPointId'] = $module['params']['showBasicPointId'];
            }

        } else {

            return false;
        }


        $catObjects = $this->selectObjects($filter);

        if ($catObjects['count'] > 0) {

            $this->_TMS->addMassReplace($section,
                array(
                    'count' => $catObjects['count'],
                    'objects' => $catObjects['objects']
                )
            );
        } else {
            $this->_TMS->parseSection('catalogEmpty', true);

        }

        return $this->_TMS->parseSection($section);

    }

}
