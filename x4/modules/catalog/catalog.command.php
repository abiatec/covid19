<?php

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use X4\Classes\XRegistry;

class catalogReindexCommand extends Command
{
    protected function configure()
    {
        XRegistry::get('EVM')->on('catalog:onObjectIndex', 'advance', $this);

        $this->setName('catalog:reindex')
            ->setDescription('Makes catalog reindex')
            ->setHelp('This command allows you reindex catalog')
            ->addOption('categoryId', null, InputOption::VALUE_REQUIRED, 'category ID', 0)
            ->addOption('isFullIndex', null, InputOption::VALUE_OPTIONAL, 'make full index', 0)
            ->addOption('indexMoveStep', null, InputOption::VALUE_OPTIONAL, 'set index step ', 500);
    }


    public function advance($params, $data)
    {

        if ($params['data']['z'] == 1) {
            $this->progressBar->advance();
        }
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['Begin reindex', '', '',]);
        $output->writeln(['IndexMoveStep value = ' . $input->getOption('indexMoveStep'), '']);


        $node = $this->_commonObj->_tree->getNodeInfo($input->getOption('categoryId'));

        if (!$node) {
            $output->writeln('<error>undefined --categoryId</error>');
            exit;
        }

        if (!empty($node)) {

            if (!empty($node['params']['IndexParams'])) {
                $indexParams = explode(',', $node['params']['IndexParams']);
                $skuExtract = array('doNotExtractSKU' => true);

                $output->writeln(['IndexParams value = ' . $node['params']['IndexParams'], '']);
            }

            if (!empty($node['params']['IndexParamsSku'])) {
                $indexParamsSku = explode(',', $node['params']['IndexParamsSku']);
                $skuExtract = array('doNotExtractSKU' => false);
                $output->writeln(['IndexParamsSku value = ' . $node['params']['IndexParamsSku'], '']);
            }

            $this->progressBar = new ProgressBar($output);
            $this->progressBar->start();

            $params['start'] = 0;

            $this->result = $this->_commonObj->fastIndexing($params['id'], $indexParams, $params['start'], $input->getOption('indexMoveStep'), $skuExtract, $indexParamsSku, $input->getOption('isFullIndex'));

            $this->progressBar->finish();
        } else {
            $output->writeln(['', 'Index node does not exists!']);
        }


        $output->writeln(['', '', 'reindexed']);
    }
}


class catalogRefreshUrlCommand extends Command
{
    protected function configure()
    {
        $this->setName('catalog:refreshUrlTransform')
            ->setDescription('Makes catalog urls transform')
            ->setHelp('This command allows fresh all urls');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['Begin refresh', '', '',]);

        $this->_commonObj->truncateTransformList();
        $this->_commonObj->rebuildUrlTransformMatrix();


        $output->writeln(['', '', 'Refresh ready']);
    }
}


class catalogReindexPricesCommand extends Command
{
    protected function configure()
    {

        $this->setName('catalog:reindexPrices')
            ->setDescription('Makes catalog reindex')
            ->setHelp('This command allows you reindex catalog');
    }


    public function advance($params, $data)
    {

        $this->progressBar->advance();

    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['Begin reindex price', '', '',]);

        XRegistry::get('EVM')->on('catalog:onPricesReindex', 'advance', $this);

        $this->progressBar = new ProgressBar($output);
        $this->progressBar->start();
        $this->_commonObj->rebuildIcurrencyFields();
        $this->progressBar->finish();
        $output->writeln(['', '', 'Reindexed']);
    }
}


class catalogPSGConvertIndexCommand extends Command
{
    protected function configure()
    {

        $this->setName('catalog:reindexPSG')
            ->setDescription('Makes catalog PSG reindex')
            ->setHelp('This command allows you PSG reindex catalog');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['Begin reindex PSG', '', '',]);

        $this->_commonObj->_tree->cacheState(false);
        $this->_commonObj->_sku->cacheState(false);
        $this->_commonObj->_searchForms->cacheState(true);
        $this->_commonObj->_propertySetsTree->cacheState(true);


        $step = $this->_config['reindexPSGStepConsole'];
        $this->progressBar = new ProgressBar($output);
        $this->progressBar->start();
        $start = 0;
        $iterate = true;

        while ($iterate) {
            $items = $this->_commonObj->_tree->selectAll()->where(array('@obj_type', '=', '_CATOBJ'))->limit($start, $step)->run();
            $start += $step;

            if (!empty($items)) {
                foreach ($items as $item) {
                    $tParams = XCacheFileDriver::serializedRead('PSG-param', $item['obj_type'] . $item['params']['__nodeChanged'] . $item['id'], false);

                    if (!$tParams) {
                        $itemPSG = $this->_commonObj->psgParamsCreator($item);
                        XCacheFileDriver::serializedWrite($itemPSG, 'PSG-param', $item['obj_type'] . $item['params']['__nodeChanged'] . $item['id']);

                        $skus = $this->_commonObj->_sku->selectStruct('*')->selectParams('*')->where(array('@netid', '=', $item['id']))->run();

                        if (!empty($skus)) {
                            foreach ($skus as $sku) {
                                $skuPSG = $this->_commonObj->skuHandleSingle($sku);
                                $output->writeln($item['id'] . ' ' . $sku['params']['Name'] . ' SKU');
                                XCacheFileDriver::serializedWrite($skuPSG, 'skuPSG', $sku['id'] . $sku['params']['__nodeChanged']);
                            }
                        }


                        $output->writeln($item['id'] . ' ' . $item['params']['Name'] . ' OBJECT');
                        $this->progressBar->advance();

                    }
                }

            } else {
                $iterate = false;
            }

        }


        $this->progressBar->finish();
        $output->writeln(['', '', 'Reindexed']);
    }
}


class catalogBoostTreeCommand extends Command
{
    protected function configure()
    {

        $this->setName('catalog:boostTree')
            ->setDescription('Makes catalog tree boosting')
            ->setHelp('This command allows you do tree boosting');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['Begin tree boost', '', '',]);

        $this->_commonObj->boostTree(null);

        $output->writeln(['', '', 'Booster ready']);
    }
}


class catalogFixCatalogCommand extends Command
{
    protected function configure()
    {

        $this->setName('catalog:fix')
            ->setDescription('Fixes catalog - removes empty values and sku')
            ->setHelp('This command allows you do fix catalog');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['Begin fix', '', '',]);

        $sku = $this->_commonObj->_sku->selectStruct('*')->where(array('@netid', '>', 0))->run();
        $this->progressBar = new ProgressBar($output);
        $this->progressBar->start();

        if (!empty($sku)) {
            $delArr = [];
            foreach ($sku as $s) {
                $c = $this->_commonObj->_tree->getNodeStruct($s['netid']);
                if (empty($c)) {
                    $delArr[] = $s['id'];
                    $this->progressBar->advance();
                }
            }

            if (!empty($delArr)) {
                $this->_commonObj->_sku->delete()->where(array('@id', '=', $delArr))->run();
            }
        }

        $step = $this->_config['fixCatalogStepConsole'];

        $start = 0;
        $iterate = true;
        $delArr = [];

        while ($iterate) {
            $items = $this->_commonObj->_tree->selectAll()->where(array('@obj_type', '<>', '_ROOT'))->limit($start, $step)->run();
            $start += $step;

            if (!empty($items)) {
                foreach ($items as $item) {
                    if (!$item['params']['PropertySetGroup']) {
                        $delArr[] = $item['id'];
                        $this->progressBar->advance();
                    }
                }

            } else {
                $iterate = false;
            }

        }

        if (!empty($delArr)) {
            $this->_commonObj->_tree->delete()->where(array('@id', '=', $delArr))->run();
        }

        $this->progressBar->finish();
        $output->writeln(['', '', 'fixed']);

    }
}









