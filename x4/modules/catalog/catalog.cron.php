<?php


class catalogCron extends catalogBack
{

    public function rebuildPrices($params)
    {
        $this->_commonObj->rebuildIcurrencyFields($params);
        return true;
    }


    private function clearCache()
    {
        $adm = new X4\AdminBack\AdminPanel();
        $adm->clearCache(true);
    }

    public function catalogIndex($params)
    {

        $this->fastIndexing($params);
        $this->clearCache();

        return !$this->result['ready'];
    }


    public function catalogAutoImport($params)
    {
        $autoLoadPath = xConfig::get('PATH', 'MEDIA') . 'import/' . $params['folder'] . '/';

        $shortPath = str_replace(PATH_, '', $autoLoadPath);

        if (file_exists($autoLoadPath)) {

            $files = XFILES::filesList($autoLoadPath, 'all', array('.xlsx'), 0, 1);

            if (!empty($files)) {
                foreach ($files as $file) {
                    $params['filename'] = $shortPath . $file;

                    if ($this->importData($params)) {

                        if (empty($params['doNotDeleteFile'])) {
                            $unset = $autoLoadPath . $file;
                            unlink($unset);
                            $this->clearCache();
                            return true;
                        }

                    }

                }
            }

        }

    }

}
