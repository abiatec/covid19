<?php
use X4\Classes\XRegistry;

abstract class catalogProperty extends x4class
{
    public $tpl;
    public $_moduleLink = 'catalog';
    public $propertyName = [];
    public $psetStorage = [];
    public $psetIdToNameStorage = [];
    public $psetInfoStorage = [];
    static $dataCatalogPSG;

    public function __construct($class)
    {
        parent::__construct();
        $this->propertyName = $class;
    }

    public function safeUrlTransform($url)
    {
        return strtolower(str_replace(array('?', '&', '/', ' ', '--'), array('-', '-', '-', '-'), $url));

    }

    public function loadDefaultTpl()
    {
        static $loaded;

        if (!$loaded) {
            $this->_TMS->addFileSection(
                XRegistry::get('ADM')->loadModuleTpls($this->_moduleLink, array(array('tplName' => $this->propertyName)), true),
                true);
            $loaded = true;
        }
    }


    public static function handleSearchFilterCreatePrototypeItem($fieldData)
    {

        if ($fieldData['propertyData']['isSKU']) {
            $filterItem = new filterItem('s');
            $propertyPath = $fieldData['property'];

        } else {

            $filterItem = new filterItem('f');
            $propertyPath = $fieldData['propertySet'] . '.' . $fieldData['property'];
        }

        return array('filterItem' => $filterItem,
            'item' => array("type" => $fieldData['comparsionType'], "property" => $propertyPath));

    }


    public function renderBackOptionsTemplate()
    {
        $this->loadDefaultTpl();
        return $this->_TMS->parseSection($this->propertyName . 'Options');
    }



    public function onListingView($value, $propertyInfo, $clmn)
    {
        return $value;
    }

    public function handleTypeBack($property, $value = null)
    {
        return $property;
    }

    public function handleRegularReverseUrlTransformation($transformation, $explodedQuery)
    {
        return null;
    }


    public function prepareFacet($fieldName, $object, $field, &$matrix)
    {
        $fHash = md5($object['params'][$fieldName]);
        $matrix[$fieldName]['facet'][$fHash] = $object['params'][$fieldName];
        $matrix[$fieldName]['space'][$fHash][] = $object['id'];
    }

    public function buildFacet($values, $field)
    {
        return $values;
    }

    public function onListingPrepare($columns, $columnName)
    {

        return $columns;
    }

    public function handleTypeOnEdit($options, $value, $object)
    {
        return $value;
    }

    public function handleTypeOnSave($options, $value, $paramSet, $paramPath)
    {
        return $value;
    }

    public function handleTypeFront($value = null, $property = null, $object = null, $setName = null)
    {
        return $value;
    }

    public function handleOptions($options)
    {
        return $options;
    }

    public function handleSearchFilterSet($matrix, $field, $paramSet)
    {
        return $matrix;
    }

    public function handleSearchFilterGetFilterInfo($matrix, &$field, $outerLink = null)
    {
        return $matrix;
    }

    public function handleSearchFilterSorting($matrix, $field)
    {
        return $matrix;
    }

    public function handleSearchFilterValue($value, $options, $field)
    {
        return $value;
    }



    public function handleOnImport($value, $row, $rowName, $schemeRowName, $columns, $psetData, $oldRow)
    {
        $row[$rowName] = $value;
        return $row;

    }

    public function handleUrlTransformation($data, $property)
    {
        return null;
    }

    public function handleOnBeforeImport($key, $columns, $context)
    {
    }


    public function renderBackTemplate()
    {
        $this->loadDefaultTpl();
        return $this->_TMS->parseSection($this->propertyName);
    }

    public function valueTransform()
    {
    }
}