<?php

xConfig::pushConfig(array(
    'moduleVersion' => 1,
    'iconClass' => 'i-folder2',
    'admSortIndex' => 90,
    'boostTree' => false,
    'imageListingSizeWidth' => 90,
    'imageListingSizeHeight' => 0,
    'imageListingCrop' => "4:3",
    'cutWordsTextAreaListing' => 25,
    'currencyRebuildChunk'=>500,
    'skuGroupListItemsPerPage' => 500,
    'indexMoveStep'=>500,
    'compareObjectsCutSize'=>5,
    'defaultFilterInnerItemsCount' => 50,
    'dateListingFormat' => 'd-m-Y H:i:s',
    'actionable' => 1,
    'reindexPSGStepConsole'=>200,
    'fixCatalogStepConsole'=>500,
    'cacheInnerResources' => true,
    'cacheTree' => array
    (
        'tree' => true,
        'sku' => true,
        'searchForms' => true,
        'propertySetsTree' => true

    )

));

