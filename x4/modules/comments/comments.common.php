<?php


class commentsCommon extends xModuleCommon implements xCommonInterface
{
    public $_useTree = true;

    public function __construct()
    {

        parent::__construct(__CLASS__);


        $this->_tree->setObject('_ROOT', array('Name'));
        $this->_tree->setObject('_TREAD', array('Alias', 'active', 'moderation', 'treadSort'), array('_ROOT'));
        $this->_tree->setObject('_COBJECT', array('module', 'marker', 'active', 'closed', 'cobjectId'), array('_TREAD'));


    }


    public function getCobjectByModule($cobjId, $module)
    {
        /*if ($results = $this->_tree->Search(array('cobjectId' => $cobjId, 'module' => $module), true)) {
            reset($results);
            return current($results);
        }*/

    }


    public function getCobjectByTread($cobjId, $treadId = null, $treadName = null)
    {
        if (!$treadId && $treadName) {
            return $this->_tree->selectStruct(array('id'))->where(array('@ancestor', '=', 1), array('@basic', '=', $treadName))->run();

        }

        $instance = $this->_tree->selectStruct('*')->selectParams('*');

        if (!empty($treadId)) {
            $instance->addwhere(array('@ancestor', '=', $treadId));
        }

        return $instance->addWhere(array('cobjectId', '=', $cobjId))->singleResult()->run();

    }


    public function getComment($id)
    {
        return XPDO::selectIN('*', 'comments', (int)$id);
    }


    public function getTreadByName($name)
    {
        if (!emprty($name)) {
            return $this->_tree->selectStruct('*')->selectParams('*')->where(array('@basic', '=', $name))->singleResult()->run();
        }
    }


    public function defineFrontActions()
    {
        $this->defineAction('showLastComments');

    }


}
