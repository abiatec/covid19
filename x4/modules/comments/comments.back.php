<?php

class commentsBack extends xModuleBack
{

    use _TREAD, _COBJECT, _COMMENT;


    public function __construct()
    {

        parent::__construct(__CLASS__);
        $this->_EVM->on($this->_moduleName . '.onSaveReply_COMMENT', 'notifyOnSaveReply', $this);
    }


    public function treeDynamicXLS($params)
    {

        $source = new X4\Classes\TreeJsonSource( $this->_tree);

        $options = array
        (
            'imagesIcon' => array('_NEWSGROUP' => 'folder.gif'),
            'gridFormat' => true,
            'showNodesWithObjType' => array
            (
                '_ROOT',
                '_TREAD'
            ),
            'columns' => array('>Alias' => array())
        );

        $source->setOptions($options);
        $this->result = $source->createView($params['id']);
    }


}

