<?php


class showLastCommentsAction extends xAction
{

    public function run($params)
    {
        if ($comments = $this->get_last_comments($params)) {
            $this->_TMS->addFileSection(Common::get_site_tpl($this->_module_name, $params['Template']));

            foreach ($comments as $comment) {
                $this->_TMS->addMassReplace('_last_comment', $comment);
                $this->_TMS->parseSection('_last_comment', true);
            }

            return $this->_TMS->parseSection('_last_comments');
        }
    }


    public function getLastComments($params)
    {

        if (!empty($params['count']) && !empty($params["cobj"])) {
            $cobjects = $params["cobj"];
        } elseif ($params['treads']) {

            if ($treads = explode(',', $params['treads'])) {
                $cobjects = [];

                foreach ($treads as $treadId) {
                    if ($tchilds = $this->_tree->getChilds($treadId)) {
                        $cobjects = array_merge($cobjects, XARRAY::askeyval($tchilds, 'id'));
                    }
                }
            }

        }

        if (!empty($cobjects)) {
            ($params["sl"]) ? $offset = strval($params["sl"]) : $offset = "0";
            ($params["asc"] == "DSC") ? $order = '-' : $order = '';

            $sql = 'SELECT * FROM comments WHERE Active=1 AND cid in(' . implode(',', $cobjects) . ') ORDER BY ? date desc LIMIT ?,?';
            $query = xRegistry::get('XPDO')->prepare($sql);
            $pdoResult = $query->execute([$order, $offset, $params['count']]);

            while ($row = $pdoResult->fetch(\PDO::FETCH_ASSOC)) {
                $result[] = $row['items'];
            }

            return $result;
        } else {
            return array();
        }
    }


}
