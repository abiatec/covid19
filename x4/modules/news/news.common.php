<?php

use X4\Classes\XRegistry;

class newsCommon extends xModuleCommon implements xCommonInterface
{

    public $_useTree = true;

    public function __construct()
    {
        parent::__construct(__CLASS__);
        $this->_tree->setObject('_ROOT', array('Name'));
        $this->_tree->setObject('_NEWSGROUP', array
        (
            'Name',
            'commentsTread'
        ), array('_ROOT'));

    }


    public function defineFrontActions()
    {
        $this->defineAction('showNewsInterval');
        $this->defineAction('showNewsCategories');
        $this->defineAction('showCorrespodingTags');
        $this->defineAction('newsServer', array('serverActions' => array('showNewsCategories', 'showNews', 'showNewsInterval')));
    }


    public function selectNewsInterval($category, $startRow = 0, $rowsNum = '', $where = '', $order = 'DESC', $countOnly = false, $hideOldNews = false)
    {
        $rowsNum = (int)$rowsNum;

        if (!empty($category)) {

            $categoryQuery = 'and ';

            $catArray = array_fill(0, count($category), "FIND_IN_SET(?,`categories`)");

            $categoryQuery .= implode(' OR ', $catArray);
        } else {

            $categoryQuery = '';
        }

        if ($countOnly) {
            $count = "count(*) as ccount ";
            $rowsNum = '';

        } else {

            $count = "*";
        }

        $limit = ($rowsNum) ? " LIMIT $startRow, $rowsNum" : '';
        $oldNews = $hideOldNews ? 'AND news_end NOT BETWEEN 1 AND ' . time() . '' : '';

        $query = XRegistry::get('XPDO')->prepare("select $count FROM news WHERE news_date<" . time() . " " . $oldNews . "  and active = 1 $categoryQuery $where ORDER BY news_date $order " . $limit);

        $pdoResult = $query->execute($category);

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        if ($pdoResult && !$countOnly) {
            return $result;
        } elseif ($pdoResult) {
            return $result['ccount'];
        }
    }

    public function selectNewsById($id)
    {
        if ($pdoResult = XRegistry::get('XPDO')->selectIN('*', 'news', $id)) {
            return $pdoResult;
        }
    }

    public function selectNews($basic)
    {
        if ($pdoResult = XRegistry::get('XPDO')->selectIN('*', 'news', 'basic=' . $basic)) {
            return $pdoResult;
        }
    }


    public function getCategories()
    {
        return $this->_tree->selectStruct(array('id', 'basic'))->selectParams('*')->childs(1, 1)->run();
    }


}