<?php

use X4\Classes\XRegistry;

class showNewsIntervalAction extends xAction
{
    public $_props;

    public function build($params)
    {
        $hideOldNews = false;

        $this->_props['newsOnPage'] = $newsOnPage = isset($params['params']['OnPage']) ? (int)$params['params']['OnPage'] : $this->_config['showNewsPerPage'];

        $categories = json_decode($params['params']['Categories'], true);

        if ($params['params']['HideOldNews']) {
            $hideOldNews = true;
        }

        $this->_props['startPage'] = $startItems = isset($params['request']['requestData']['page']) ? $params['request']['requestData']['page'] : 0;

        if (is_array($categories)) {
            $newsList = $this->_commonObj->selectNewsInterval($categories, $startItems, $newsOnPage, null, 'desc', false, $hideOldNews);
            if (!empty($newsList)) {

                $categoriesInfo = $this->_tree->selectStruct('*')->selectParams('*')->where(array('@id', '=', $categories))->format('keyval', 'id')->run();
                $newsServerPage = $this->createPageDestination($params['params']['DestinationPage']);
                $this->_props['newList'] = $this->newsListTransform($newsList, $newsServerPage);
                $this->_props['count'] = $this->_commonObj->selectNewsInterval($categories, $startItems, $newsOnPage, null, 'asc', true, $hideOldNews);
                $this->_props['newsServerPage'] = $newsServerPage;
                $this->_props['categoriesInfo'] = $categoriesInfo;

            }
        }


    }

    public function run($params)
    {

        $this->loadModuleTemplate($params['params']['Template']);

        if (!empty($this->_props['newList'])) {
            Common::parseNavPages($this->_props['count'], $this->_props['newsOnPage'], $this->_props['newsOnPage'], $this->_props['newsServerPage'], $this->_TMS);
            return $this->renderNews($this->_props['newList'], $this->_props['newsServerPage'], $this->_props['categoriesInfo']);

        }
    }


    public function runHeadless($params)
    {

        if (!empty($this->_props['newList'])) {
            $this->_props['paginator'] = Common::parseNavPagesHeadless($this->_props['count'], $this->_props['newsOnPage'], $this->_props['newsOnPage'], $this->_props['newsServerPage']);
            return $this->_props;

        }

        return array('emptyNews' => true);
    }

}