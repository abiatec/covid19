<?php

use X4\Classes\XRegistry;


class showNewsAction extends xAction
{

    public $_props;

    public function build($params)
    {
        $pInfo = XRegistry::get('TPA')->getRequestActionInfo();

        if (isset($pInfo['requestActionPath'])) {

            $basic = substr($pInfo['requestActionPath'], 1);

            $news = $this->_commonObj->selectNews($basic);

            $this->setSeoData($news);

            $newsNode = array(
                'params' => array(
                    'Name' => $news['header']
                )
            );


            XRegistry::get('pagesFront')->pushAdditionalBones($newsNode);

            if (!empty($news['tags'])) {
                $news['tags'] = $this->tagsAgregate($news['tags'], $pInfo['pageLinkHost']);
            }

            $this->_props['news'] = $news;


        }
    }

    public function run($params)
    {

        $this->loadModuleTemplate($params['params']['Template']);
        $this->_TMS->addMassReplace('newsSingle', $this->_props['news']);
        return $this->_TMS->parseSection('newsSingle');

    }

    public function runHeadless($params)
    {
        return $this->_props;
    }

}



