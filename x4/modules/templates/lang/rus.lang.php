<?php
$LANG = [
    'edit_template' => 'Редактирование шаблона',
    'name_file_template' => 'Имя файла шаблона',
    'alias_template' => 'алиас шаблона',
    'template_maintenance' => 'Содержание шаблона',
    'templates' => 'Шаблоны',
    'template_name' => 'Имя шаблона',
    'size' => 'Размер',
    'path' => 'Путь к файлу'
];
