<?php

class pagesInstall extends pagesCommon
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run($installedDomain)
    {
        $this->_tree->writeNodeParams(1, array('Name'=>$installedDomain));
    }

}
