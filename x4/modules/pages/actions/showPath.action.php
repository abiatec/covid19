<?php

class showPathAction extends xAction
{

    public $_props;

    public function build($params)
    {

        if (is_array($bonesFull = $this->bones)) {

            if (!empty($this->additionalBones)) {
                foreach ($this->additionalBones as $bone) {
                    $bonesFull[] = $bone;
                }
            }

            $bonesLength = count($bonesFull);

            $i = 0;

            foreach ($bonesFull as $bone) {

                $i++;

                if (!$bone['link']) {
                    $link = $this->_commonObj->linkCreator($bone['basicPath']);

                } else {
                    $link = $bone['link'];
                }


                if (!$bone['params']['DisablePath'] && ($bonesLength != $i)) {
                    $bonesItems[] = [
                        'name' => $bone['params']['Name'],
                        'link' => $link
                    ];

                }

            }

            $bone = array_pop($bonesFull);
            $bonesItems[] = [
                'name' => $bone['params']['Name'],
                'link' => $link
            ];

        }


        $this->_props['bonesItems'] = $bonesItems;
    }

    public function run($params)
    {
        $this->loadModuleTemplate($params['params']['Template']);

        if (!empty($this->_props['bonesItems'])) {

            $items = array_slice($this->_props['bonesItems'], 0, -1);

            foreach ($items as $bone) {
                $this->_TMS->addMassReplace('_bones_item', $bone);
                $this->_TMS->parseSection('_bones_item', true);
            }

            $bone = array_pop($this->_props['bonesItems']);

            $this->_TMS->addMassReplace('_bones_item_no_link', $bone);
            $this->_TMS->parseSection('_bones_item_no_link', true);
        }

        return $this->_TMS->parseSection('_bones');

    }
}

