<?php

use X4\Classes\XPDO;
use X4\Classes\XRegistry;
use Thepixeldeveloper\Sitemap\Urlset;
use Thepixeldeveloper\Sitemap\Url;
use Thepixeldeveloper\Sitemap\Drivers\XmlWriterDriver;

class pagesFront extends xModule
{
    public $menuAncestor;
    public $devideByRows;
    public $langVersion;
    public $bones = [];
    public $moduleOrder = [];
    public $moveLink;
    public $domain;
    public $additionalBones = [];

    public function __construct()
    {
        parent::__construct(__CLASS__);
        if (xConfig::get('GLOBAL', 'currentMode') == 'front') {
            $this->_tree->cacheState($this->_config['cacheTree']['tree'], xConfig::get('GLOBAL', 'treeCacheTimeout'));
            if ($this->_config['boostTree']) {
                $this->_tree->startBooster();
                $this->_tree->setTreeBoosted();

            }
        }

    }

    static function hostJuggle()
    {
        $synonym = xConfig::get('DOMAINSYNONYMS', HTTP_HOST);
        if (!empty($synonym)) {
            return $synonym;
        }
        return HTTP_HOST;
    }

    public function getPageIdByPath($path)
    {

        $treePath = XARRAY::clearEmptyItems(explode('/', $path), true);
        $this->root = $this->_tree->getNodeInfo(1);

        $host = self::hostJuggle();

        $this->domain = $this->_tree->selectStruct('*')->selectParams('*')->where(array(
            '@basic',
            '=',
            $host
        ), array(
            '@ancestor',
            '=',
            1
        ))->singleResult()->run();


        xConfig::set('GLOBAL', 'domain', $this->domain);

        if (empty($this->domain)) {
            throw new Exception('no-domain-detected');
        }

        if (!$langVersions = $this->getLangVersions($this->domain['id'])) {
            throw new Exception('no-lang-versions-detected');
        }


        $langVersionsStack = XARRAY::arrToKeyArr($langVersions, 'id', 'basic');

        if (!$treePath) {
            if (!$this->domain['params']['StartPage']) {
                $this->langVersion = current($langVersions);

            } else {
                $this->langVersion = $langVersions[$langVersionsStack[$this->domain['params']['StartPage']]];
            }

            $this->_commonObj->nativeLangVersion = $this->langVersion['basic'];

            if (empty($this->langVersion['params']['StartPage'])) {
                throw new Exception('no-start-page-selected-for-this-lang-version');
            }

            $this->page = $this->_tree->getNodeInfo($this->langVersion['params']['StartPage']);

            return $this->pageFinalPoint() !== false;


        } else {

            if ((array_search($treePath[0], $langVersionsStack) !== false) && $langVersions[$treePath[0]]) {

                $this->langVersion = $langVersions[$treePath[0]];

                if (empty($this->domain['params']['StartPage'])) {
                    throw new Exception('no-start-page-selected-for-this-domain');
                }

                foreach ($langVersions as $lKey => $lVersion) {

                    if ($this->domain['params']['StartPage'] == $lVersion['id']) {

                        $this->_commonObj->nativeLangVersion = $lVersion['basic'];
                    }
                }

            } else {

                if (!isset($this->domain['params']['StartPage'])) {

                    throw new Exception('no-start-page-selected-for-this-domain');
                }

                foreach ($langVersions as $lKey => $lVersion) {
                    if ($this->domain['params']['StartPage'] == $lVersion['id']) {
                        $this->langVersion = $langVersions[$lKey];
                    }
                }


                array_unshift($treePath, $this->langVersion['basic']);
                $this->_commonObj->nativeLangVersion = $this->langVersion['basic'];
            }


            array_unshift($treePath, $host);
            $node = $this->_tree->idByBasicPath($treePath, array('_DOMAIN', '_LVERSION', '_PAGE', '_GROUP'), true);

            if (empty($node)) {
                return false;
            }

            $this->page = $this->_tree->getNodeInfo($node['id']);

            $pathParams = '';

            if ($this->page['obj_type'] == '_PAGE') {

                if ($this->_tree->readNodeParam($this->page['ancestor'], 'startPage') == $this->page['id']) {

                    if (!empty(XRegistry::get('TPA')->requestAction)) {

                        $pathParams = '/~' . XRegistry::get('TPA')->requestAction;

                        if (!empty(XRegistry::get('TPA')->requestActionPath)) {
                            $pathParams .= XRegistry::get('TPA')->requestActionPath;
                        }

                    }

                    if (isset($_SERVER['REDIRECT_QUERY_STRING'])) {

                        $pathParams .= '?' . $_SERVER['REDIRECT_QUERY_STRING'];
                    }

                    $this->moveLink = $this->_commonObj->createPagePath($this->page['ancestor'], true) . $pathParams;

                    if (XRegistry::get('TPA')->renderMode == 'HEADLESS') {

                        throw new Exception('headless-301-rebuild');

                    } else {
                        XRegistry::get('TPA')->move301Permanent($this->moveLink);
                    }

                }
            }

            if ($this->pageFinalPoint() === false) {
                return false;
            } else {
                $bones = array_slice($this->page['path'], 3);
                $bones[] = $this->page['id'];

                XRegistry::get('TPA')->setSeoData(array(
                        'Title' => $this->page['params']['Title'],
                        'Description' => $this->page['params']['Description'],
                        'Keywords' => $this->page['params']['Keywords'],
                        'Meta' => $this->page['params']['Meta']
                    )
                );

                if (!empty($bones)) {

                    $this->bones = $this->_tree->selectStruct('*')->selectParams('*')->getBasicPath('/', false)->where(array(
                        '@id',
                        '=',
                        $bones
                    ))->format('keyval', 'id')->run();
                }

                array_unshift($this->bones, $this->langVersion);
                return true;
            }
        }

    }

    public function getLangVersions($domainId = null)
    {
        $where[] = array('@obj_type', '=', '_LVERSION');

        if (!is_null($domainId)) {
            $where[] = array('@ancestor', '=', $domainId);
        }

        return $this->_tree->selectStruct('*')->selectParams('*')->getBasicPath('/', false)->where($where, true)->format('keyval', 'basic')->run();
    }

    private function pageFinalPoint()
    {
        $end = false;
        while (!$end) {
            if (in_array($this->page['obj_type'], array(
                '_GROUP',
                '_ROOT',
                '_LVERSION',
                '_DOMAIN'
            ))) {

                if (!empty($this->page['params']['StartPage'])) {
                    $this->page = $this->_tree->getNodeInfo($this->page['params']['StartPage']);
                    if ($this->page['params']['DisableGlobalLink']) {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                $end = true;
            }
        }

        return null;
    }

    public function getSlotzCrotch($tplSlotz)
    {
        if ($sPath = $this->page['path']) {
            $sPath[] = $this->page['id'];
            $result = $this->_tree->selectStruct('*')->selectParams('*')->childs($sPath, 2)->where(
                array('@obj_type', '=', array('_MODULE', '_SLOT')))->asTree()->run();

            if (!empty($result)) {
                foreach ($sPath as $pathPoint) {
                    if ($eSlots = $result->fetchArray($pathPoint)) {
                        foreach ($eSlots as $id => $slot) {
                            if ($eModules = $result->fetchArray($id)) {
                                foreach ($eModules as $kid => $module) {
                                    if (in_array($slot['basic'], $tplSlotz)) {
                                        $modules[$slot['basic']][] = $kid;
                                        $this->execModules[$kid] = $module;
                                        $this->modulesOrder[$kid] = $module['params']['_Priority'];
                                    }
                                }
                            }
                        }
                    }
                }

                if (isset($this->modulesOrder)) {
                    arsort($this->modulesOrder);
                }

                return $modules;
            }
        }
    }

    public function pushAdditionalBones($bone)
    {
        $this->additionalBones[] = $bone;
    }


    public function getRewrites()
    {
        static $routes;
        if (empty($routes)) {
            return $routes = XPDO::selectIN('*', 'routes', '', 'order by priority desc');
        } else {
            return $routes;
        }
    }


    public function generateSitemap()
    {
        $host = self::hostJuggle();

        $domain = $this->_tree->selectStruct('*')->selectParams('*')->where(array(
            '@basic',
            '=',
            $host
        ), array(
            '@ancestor',
            '=',
            1
        ))->singleResult()->run();

        $siteMapSource = $this->_tree->selectStruct('*')->selectParams('*')->getBasicPath()->childs($domain['id'])->where(
            array('@obj_type', '=', array('_LVERSION', '_GROUP', '_PAGE', '_LINK')))->asTree()->run();

        $siteMapSource->recursiveStep($domain['id'], $this, 'buildSiteMapXml');
        $urlset = new Urlset();
        if (!empty($this->urls)) {

            foreach ($this->urls as $url) {
                $urlset->add($url);
            }

            $driver = new XmlWriterDriver();
            $urlset->accept($driver);
            return $driver->output();
        }


    }

    public function buildSiteMapXml($node, $ancestor, $tContext, $extdata)
    {
        $url=$this->_commonObj->createPagePath($node['id']);
        $this->urls [] = new Url($url);
    }


}


