_PAGE = new Class({
    Extends: CRUN,
    initialize: function (context) {
        this.parent(context, {objType: '_PAGE', autoCreateMethods: true});
        context.pushToTreeClickMap(this.options.objType, 'edit_PAGE');
    },

    create: function (data) {

        let selectedId = this.context.tree.getSelectedRowId();
        let parentData = {};


        if (selectedId) {
            let objType = this.context.tree.getRowAttribute(selectedId, "obj_type");
            if (['_ROOT', '_DOMAIN'].indexOf(objType) == -1) parentData = {id: selectedId}
        }

        this.parent(parentData);

        if (parentData.id) {
            $(this.mainViewPort).find('#ancestor').val(this.context.getTreePathAncestor(['_PAGE']));
            $(this.mainViewPort).find('#ancestorId').val(selectedId);
            $(this.mainViewPort).find('.hiddenTemplates').show();

        }

        xoad.html.importForm('create_PAGE', this.context.connector.result.data);
    },

    saveEdited: async function (e) {
        e.preventDefault();
        this.parent();

        if (this.validated) {

            let modules = this._slotz.exportModules();
            let pageData = xoad.html.exportForm('edit_PAGE');
            await this.context.connector.executeAsync({
                onSaveEdited_PAGE: {
                    id: this.selectedId,
                    data: pageData,
                    modules: modules
                }
            });
        }

    },

    save: async function (e) {
        e.preventDefault();
        this.parent();

        if (this.validated) {

            let modules = this._slotz.exportModules();
            let pageData = xoad.html.exportForm('create_PAGE');
            let ancestor = pageData.ancestorId;
            delete pageData.ancestorId;
            await this.context.connector.executeAsync({
                onSave_PAGE: {
                    ancestor: ancestor,
                    data: pageData,
                    modules: modules
                }
            });
            this.context.tree.refreshItem(ancestor);

        }
    },

    edit: async function (data) {
        this.parent(data);
        this.context.tabs.addTab({
            id: 'teditpage',
            name: AI.translate('pages', 'page_editing'),
            temporal: true,
            active: true
        }, true);

        data.id = parseInt(data.id);

        await this.context.connector.executeAsync({
            onEdit_PAGE:
                {
                    id: data.id
                },
            getSlotz: {
                id: data.id
            },
            getModules: {
                id: data.id
            }
        });


        this.currentPageId = data.id;

        this._slotz = new Slotz({
            connector: this.context.connector,
            slotsInstance: this.context.connector.result.slotz,
            modulesInstance: this.context.connector.result.modules
        });
        xoad.html.importForm('edit_PAGE', this.context.connector.result.data);
        this.context.mainViewPortFind('a.showOnSite').attr('href', 'http://' + this.context.connector.result.data.pageFullPath);
        this.context.tree.loadByPath(this.context.connector.result.data.path);

    }

});

_GROUP = new Class({
    Extends: CRUN,
    initialize: function (context) {
        this.parent(context, {objType: '_GROUP', autoCreateMethods: true});
        context.pushToTreeClickMap(this.options.objType, 'edit_GROUP');
    },


    saveEdited: async function (e) {
        e.preventDefault();
        this.parent();

        if (this.validated) {

            let modules = this.context._slotz.exportModules();
            let pageData = xoad.html.exportForm('edit_GROUP');
            await this.context.connector.executeAsync({
                onSaveEdited_GROUP: {
                    id: this.selectedId,
                    data: pageData,
                    modules: modules
                }
            });
        }

    },

    save: async function (e) {
        e.preventDefault();
        this.parent();

        if (this.validated) {

            let modules = this.context._slotz.exportModules();
            let pageData = xoad.html.exportForm('create_GROUP');
            let ancestor = pageData.ancestorId;
            delete pageData.ancestorId;
            await this.context.connector.executeAsync({
                onSave_GROUP: {
                    ancestor: ancestor,
                    data: pageData,
                    modules: modules
                }
            });
            this.context.tree.refreshItem(ancestor);
        }
    },


    create: function (data) {

        let selectedId = this.context.tree.getSelectedRowId();
        let parentData = {};

        if (selectedId) {
            let objType = this.context.tree.getRowAttribute(selectedId, "obj_type");
            if (['_ROOT', '_DOMAIN'].indexOf(objType) == -1) parentData = {id: selectedId}
        }

        this.parent(parentData);


        if (parentData.id) {
            $(this.mainViewPort).find('#ancestor').val(this.context.getTreePathAncestor(['_GROUP']));
            $(this.mainViewPort).find('#ancestorId').val(selectedId);
            $(this.mainViewPort).find('.hiddenTemplates').show();

        }

        xoad.html.importForm('create_GROUP', this.context.connector.result.data);
        this.context._slotz = new Slotz({connector: this.context.connector});
    },

    edit: async function (data) {

        this.parent(data);
        this.context.tabs.addTab({
            id: 'teditgroup',
            name: AI.translate('pages', 'group_editing'),
            temporal: true,
            active: true
        }, true);

        data.id = parseInt(data.id);

        await this.context.connector.executeAsync({
            onEdit_GROUP: {id: data.id},
            getSlotzAll: {id: data.id},
            getModules: {id: data.id}
        });

        this.context._slotz = new Slotz({
            connector: this.context.connector,
            slotsInstance: this.context.connector.result.slotz,
            modulesInstance: this.context.connector.result.modules
        });
        xoad.html.importForm('edit_GROUP', this.context.connector.result.data);
        this.context.tree.loadByPath(this.context.connector.result.data.path);


    }

});

_DOMAIN = new Class({
    Extends: CRUN,
    initialize: function (context) {
        this.parent(context, {objType: '_DOMAIN', autoCreateMethods: true});
        context.pushToTreeClickMap(this.options.objType, 'edit_DOMAIN');
    },

    edit: async function (data) {
        this.parent(data);
        this.context.tabs.addTab({
            id: 'teditdomain',
            name: AI.translate('pages', 'domain_editing'),
            temporal: true,
            active: true
        }, true);

        data.id = parseInt(data.id);
        await this.context.connector.executeAsync({
            onEdit_DOMAIN: {id: data.id},
            getSlotzAll: {id: data.id},
            getModules: {id: data.id}
        });
        this.context._slotz = new Slotz({
            connector: this.context.connector,
            slotsInstance: this.context.connector.result.slotz,
            modulesInstance: this.context.connector.result.modules
        });


        xoad.html.importForm('edit_DOMAIN', this.context.connector.result.data);


    },


    save: async function (e) {
        e.preventDefault();
        this.parent();

        if (this.validated) {
            let pageData = xoad.html.exportForm('create_DOMAIN');
            await this.context.connector.executeAsync({onSave_DOMAIN: {data: data}});
        }
    },

    create: function (data) {
        this.parent(data);
        xoad.html.importForm('create_DOMAIN', this.context.connector.result.data);
    },

    saveEdited: async function (e) {
        e.preventDefault();
        this.parent();
        if (this.validated) {
            let data = xoad.html.exportForm('edit_DOMAIN');
            data.id = this.selectedId;
            let modules = this.context._slotz.exportModules();

            await this.context.connector.executeAsync({onSaveEdited_DOMAIN: {data: data, modules: modules}});
        }

    }
});


_LVERSION = new Class({
    Extends: CRUN,
    initialize: function (context) {
        this.parent(context, {objType: '_LVERSION', autoCreateMethods: true});
        context.pushToTreeClickMap(this.options.objType, 'edit_LVERSION');
    },

    edit: async function (data) {

        this.parent(data);
        this.context.tabs.addTab({
            id: 'teditlang',
            name: AI.translate('pages', 'lang_editing'),
            temporal: true,
            active: true
        }, true);

        data.id = parseInt(data.id);
        await this.context.connector.executeAsync({
            onEdit_LVERSION: {id: data.id},
            getSlotzAll: {id: data.id},
            getModules: {id: data.id}
        });


        this.context._slotz = new Slotz({
            connector: this.context.connector,
            slotsInstance: this.context.connector.result.slotz,
            modulesInstance: this.context.connector.result.modules
        });

        xoad.html.importForm('edit_LVERSION', this.context.connector.result.data);


    },


    save: async function (e) {
        e.preventDefault();
        this.parent();

        if (this.validated) {
            let data = xoad.html.exportForm('create_LVERSION');
            await this.context.connector.executeAsync({onSave_LVERSION: {data: data}});
        }
    },


    create: function (data) {
        this.parent(data);
        xoad.html.importForm('create_LVERSION', this.context.connector.result.data);
    },

    saveEdited: async function (e) {
        e.preventDefault();
        this.parent();
        if (this.validated) {
            let data = xoad.html.exportForm('edit_LVERSION');
            data.id = this.selectedId;
            let modules = this.context._slotz.exportModules();
            await this.context.connector.executeAsync({onSaveEdited_LVERSION: {data: data, modules: modules}});
        }

    }
});


_ROUTES = new Class(
    {
        Extends: CRUN,
        initialize: function (context) {
            this.parent(context, {autoCreateMethods: true});
        },

        route301switch: async function (id, cid, state) {
            await this.context.connector.executeAsync({route301Switch: {id: id, state: state}});
        },


        save: function (e) {

            e.preventDefault();
            let params = xoad.html.exportForm('new_route');
            this.context.connector.executeAsync({createNewRoute: params});
            this.refreshRoutes();

        },

        del: async function (kid, id) {

            await this.context.connector.executeAsync({deleteRoute: {id: id}});
            this.context.gridlistRoutes.deleteSelectedRows();
            this.refreshRoutes();

        },

        refreshRoutes: async function () {
            await this.context.connector.executeAsync({routesTable: true});
            this.context.gridlistRoutes.clearAll();

            if (this.context.connector.result.data_set) {
                this.context.gridlistRoutes.parse(this.context.connector.result.data_set, "xjson")
            }
        },


        doOnCellEdit: async function (stage, rowId, cellInd) {

            var cellObj = this.context.gridlistRoutes.cellById(rowId, cellInd);

            if (stage == 0 || stage == 1) {

                if (cellInd == 4) {
                    await this.context.connector.executeAsync({
                        saveRoutePart: {
                            part: 'full',
                            id: rowId,
                            text: cellObj.getValue()
                        }
                    });
                }
            }

            if (stage == 2) {

                if (cellInd == 1) {
                    await this.context.connector.executeAsync({
                        saveRoutePart: {
                            part: 'from',
                            id: rowId,
                            text: cellObj.getValue()
                        }
                    });
                }

                if (cellInd == 2) {
                    await this.context.connector.executeAsync({
                        saveRoutePart: {
                            part: 'to',
                            id: rowId,
                            text: cellObj.getValue()
                        }
                    });
                }


            }
            return true;
        }


    });


_LINK = new Class({
    Extends: CRUN,
    initialize: function (context) {
        this.parent(context, {objType: '_LINK', autoCreateMethods: true});
        context.pushToTreeClickMap(this.options.objType, 'edit_LINK');
    },

    edit: async function (data) {

        this.parent(data);
        this.context.tabs.addTab({
            id: 'teditlink',
            name: AI.translate('pages', 'link_editing'),
            temporal: true,
            active: true
        }, true);

        data.id = parseInt(data.id);
        await this.context.connector.executeAsync({onEdit_LINK: {id: data.id}});
        xoad.html.importForm('edit_LINK', this.context.connector.result.data);


    },


    save: async function (e) {
        e.preventDefault();
        this.parent();

        if (this.validated) {
            let data = xoad.html.exportForm('create_LINK');
            await this.context.connector.executeAsync({onSave_LINK: {data: data}});
        }
    },


    create: function (data) {
        this.parent(data);
        xoad.html.importForm('create_LINK', this.context.connector.result.data);
    },

    saveEdited: async function (e) {
        e.preventDefault();
        this.parent();
        if (this.validated) {
            let data = xoad.html.exportForm('edit_LINK');
            data.id = this.selectedId;
            await this.context.connector.executeAsync({onSaveEdited_LINK: {data: data}});
        }

    }
});

