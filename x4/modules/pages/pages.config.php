<?php

xConfig::pushConfig(array(

    'iconClass' => 'i-flow-tree',
    'admSortIndex' => 500,
    'actionable' => 1,
    'routesOnAdminPage'=>300,
    'boostTree' => false,
    'cacheTree' => array
    (
        'tree' => true

    )

));
