<?php

class pagesListener extends xListener implements xModuleListener
{
    public function __construct()
    {
        parent::__construct('pages');
        $this->_EVM->on('AdminPanel:afterCacheClear', 'boostTreeListener', $this);
        $this->_EVM->on('boot', 'checkForSitemapXml', $this);
    }

    public function boostTreeListener($params)
    {
        $this->boostTree($params);
    }

    public function checkForSitemapXml($params)
    {
        if (strstr($_SERVER['REQUEST_URI'], 'sitemap.xml')) {
            $pages = XCore::moduleFactory('pages.front');
            echo $pages->generateSitemap();
            die();
        }
    }

}
