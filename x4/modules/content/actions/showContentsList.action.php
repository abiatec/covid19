<?php


use X4\Classes\XRegistry;


class showContentsListAction extends xAction
{
    public $_props;

    public function build($params)
    {
        $pInfo = XRegistry::get('TPA')->getRequestActionInfo();

        $this->loadModuleTemplate($params['params']['listTemplate']);

        $startPage = isset($_GET['page']) ? (int)$_GET['page'] : 0;

        $onpage = (int)$params['params']['onPage'];

        $pages = xCore::loadCommonClass('pages');

        $destinationPage = $pages->createPagePath($params['params']['destinationPage']);

        $allContents = $this->_tree->selectStruct(array('id'))->childs($params['params']['category'], 1)->run();

        if ($allContentsCount = count($allContents)) {

            Common::parseNavPages($allContentsCount, $onpage, $startPage, $destinationPage, $this->_TMS, 'page', true);

            $articles = $this->_tree->selectStruct('*')->selectParams('*')->sortBy('@id', 'DESC', 'SIGNED')->childs($params['params']['category'], 1);

            if ($onpage) {
                $articles->limit($startPage, $onpage);
            }

            $articlesList = $articles->run();

            foreach ($articlesList as $article) {
                $articleList[] = array('params' => $article['params'], 'link' => $destinationPage . '/'  . $article['basic'], 'id' => $article['id'], 'contentData' => $this->getContentData($article['id']));

            }

        }

        $this->_props['category'] = $this->_tree->getNodeInfo($params['category']);
        $this->_props['articleList'] = $articleList;

        $this->seoConfirm($this->_props['category']);


    }

    public function runHeadless($params)
    {
        return $this->_props;
    }

    public function run($params)
    {
        $this->loadModuleTemplate($params['params']['listTemplate']);
        $this->_TMS->addMassReplace('articlesList', array('category' => $this->_props['category'], 'articlesList' => $this->_props['articleList']));
        return $this->_TMS->parseSection('articlesList', true);

    }
}
