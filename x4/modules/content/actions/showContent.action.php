<?php

use X4\Classes\XRegistry;

class showContentAction extends xAction
{
    public $_props;

    public function build($params)
    {

        $pInfo = XRegistry::get('TPA')->getRequestActionInfo();

        if (isset($pInfo['requestActionPath']) && !$params['params']['contentSourceId']) {
            $basic = substr($pInfo['requestActionPath'], 1);

            if (!$node = $this->_tree->selectStruct('*')->where(array
            (
                '@basic',
                '=',
                $basic
            ))->singleResult()->run()
            ) {
                XRegistry::get('TPA')->showError404Page();
            }

            $params['params']['contentSourceId'] = $node['id'];


        }

        $this->_props['node'] = $node = $this->_tree->getNodeInfo($params['params']['contentSourceId']);

        if (!empty($node)) {

            $this->setSeoData($node['params']);

            if (!$node['params']['__viewedTimes']) {
                $node['params']['__viewedTimes'] = 0;
            }

            $this->_tree->writeNodeParam($node['id'], '__viewedTimes', ++$node['params']['__viewedTimes']);

            $this->_props['object'] = $this->articleData = $this->getContentData($params['params']['contentSourceId']);

            if (!empty($basic)) {

                $pages = XRegistry::get('pagesFront');
                $pathElement = $node;
                $pathElement['link'] = '';
                $pages->pushAdditionalBones($pathElement);
            }

        }

    }

    public function run($params)
    {
        $template = ($params['params']['Template']) ? $params['params']['Template'] : $this->_props['node']['params']['Template'];
        $this->loadModuleTemplate($template);
        $this->_TMS->addReplace('content', 'object', $this->_props['object']);
        $ext = $this->_TMS->parseSection('content');

        return $ext;

    }

    public function runHeadless($params)
    {
        return $this->_props['object'];
    }
}    
