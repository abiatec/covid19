<?php

class contentListener extends xListener implements xModuleListener
{


    public function __construct()
    {
        parent::__construct('content');
        $this->_EVM->on('pages.back:slotModuleInitiated', 'onSlotModuleSave', $this);
        $this->_EVM->on('AdminPanel:afterCacheClear', 'boostTreeListener', $this);
        $this->_EVM->on('pages.back:slotModuleInitiated', 'onSlotModuleSave', $this);

    }

    public function onSlotModuleSave($params)
    {
        if ('showContentsList' == $params['data']['module']['params']['_Action']) {
            $pages = xCore::loadCommonClass('pages');
            $contentPageLink = $pages->createPagePath($params['data']['pageId'], true);
            $source = $contentPageLink . '/(?!~)(.*?)';
            $destination = $contentPageLink . '/~showContent$1/';
            $pages->createNewRoute($source, $destination);
        }

    }

    public function boostTreeListener($params)
    {
        $this->boostTree($params);

    }

}