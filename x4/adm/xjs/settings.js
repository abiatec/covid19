var settingsBack = new Class(
    {
        Extends: _xModuleBack,


        initialize: function (name) {

            this.setName(name);
            this.setLayoutScheme('menuListView', {});
            this.parent();
            this.connector = new Connector('Settings', '.adm');


        },

        onHashDispatch: function (e, v) {
            return true;
        },


        buildInterface: function () {
            this.parent();


            items = [
                {
                    link: AI.navHashCreate(this.name, 'settings'),
                    name: AI.translate('common', 'settingsGlobal')
                },
                {
                    link: AI.navHashCreate(this.name, 'moduleSettings'),
                    name: AI.translate('common', 'install_modules_and_plugins')
                },

                {
                    link: AI.navHashCreate(this.name, 'apiAccess'),
                    name: AI.translate('common', 'apiAccess')
                }

            ];


            this.nav = new Subnav('#settings #subNav', AI.translate('common', 'settings'), items);


        },

        apiAccess: function () {

            this.connector.execute({getModuleApiAccess:true});

            this.setMainViewPort(TH.getTplHB('AdminPanel','apiAccess'));
        },

        moduleSettings:function(){


        }

    });


    