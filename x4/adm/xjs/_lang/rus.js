
   
var _lang=[];
   
_lang_xoad={};
_lang_xoad['invalid_response']='Неверный ответ сервера';
_lang_xoad['session_time_expired']='Время сессии истекло, обновите страницу';
_lang_xoad['empty_response']='Возвращен пустой результат';

_lang['common']={};
_lang['common']['tag_editor']='Редактор тегов';
_lang['common']['info']='Информация';
_lang['common']['error']='Ошибка';     
_lang['common']['copy']='Копировать';
_lang['common']['paste']='Вставить';
_lang['common']['refresh']='Обновить';
_lang['common']['delete']='Удалить';
_lang['common']['download']='Скачать';
_lang['common']['editing']='Редактирование';
_lang['common']['edit']='Редактировать';
_lang['common']['save_error']='Ошибка сохранения';
_lang['common']['warning']='Внимание'; 
_lang['common']['group_success_saved']='Группа успешно сохранена';
_lang['common']['group_is_not_saved']='Группа не сохранена';
_lang['common']['add_group']='Добавить папку';
_lang['common']['add_folder']='Добавить папку';
_lang['common']['name']='Имя';
_lang['common']['alias']='Алиас';
_lang['common']['add']='Добавить';
_lang['common']['save']='Сохранить';
_lang['common']['initiate-editor']='Править в редакторе';
_lang['common']['tags']='Теги';
_lang['common']['tagManager']='Тег менеджер';
_lang['common']['settings']='Настройки';
_lang['common']['install_modules_and_plugins']='Установка модулей';
_lang['common']['set_permissions']='Установить права доступа';

_lang['common']['select']='Выбрать';
_lang['common']['change']='Изменить';            
_lang['common']['error_on_server']='Ошибка на сервере';
_lang['common']['export']='Экспорт';
_lang['common']['search-results']='Результаты поиска';
_lang['common']['manager']='Менеджер';
_lang['common']['date']='Дата';
_lang['common']['description']='Описание';
_lang['common']['subject']='Название формы';
_lang['common']['new_user']='Новый пользователь';
_lang['common']['currency']='Валюта';
_lang['common']['user']='Пользователь';
_lang['common']['active']='Активен';
_lang['common']['phone']='Телефон';
_lang['common']['component_location']='Найти компонент на страницах';
_lang['common']['properties_are_absent']='Свойства отсутствуют';
_lang['common']['new']='Новые'; 
_lang['common']['nothing_found']='Ничего не найдено';
_lang['common']['options']='Настройки';
_lang['common']['preservation_error']='Ошибка сохранения';
_lang['common']['category_success_saved']='Категория успешно сохранена';
_lang['common']['you_really_wish_to_remove_this_objects']='Вы действительно хотите удалить эти объекты?';
_lang['common']['you_really_wish_to_copy_this_object']='Вы действительно хотите скопировать этот объект?';
_lang['common']['you_really_wish_to_remove_this_object']='Вы действительно хотите удалить этот объект?';
_lang['common']['you_really_wish_to_disable_this_object']='Вы действительно хотите отключить этот объект?';
_lang['common']['you_really_wish_to_disable_this_objects']='Вы действительно хотите отключить эти объекты?';
_lang['common']['disable']='Отключить';
_lang['common']['comment']='Комментарий';
_lang['common']['comments']='Комментарии';
_lang['common']['image']='Картинка';
_lang['common']['author']='Автор';
_lang['common']['console-it']='Показать в консоли';
_lang['common']['select_all']='Выбрать все';            
_lang['common']['clear']='Очистить';    
_lang['common']['disabled-short']='Откл.';
_lang['common']['enable']='Включить';
_lang['common']['IPTC_data']='Данные IPTC';

_lang['common']['back']='Назад';
_lang['common']['file-manager']='Файл-менеджер';
_lang['common']['add_category']='Добавить категорию';
_lang['common']['it_is_changed']='Изменено';
_lang['common']['sec']='сек';
_lang['common']['modul']='Модуль';
_lang['common']['restore']='Восстановить';
_lang['common']['address']='Адрес';
_lang['common']['successfully_added']='успешно добавлен';
_lang['common']['status']='Статус';
_lang['common']['user_with_such_name_already_exists']='Пользователь с таким именем уже существует';
_lang['common']['user_success_saved']='Пользователь успешно сохранен';
_lang['common']['group_users_success_saved']='Группа пользователей успешно сохранена';
_lang['common']['name_group_properties']='Имя группы свойств';
_lang['common']['options_not_found'] ='Свойства отсутствуют';
_lang['common']['download_file'] ='Скачать файл';
_lang['common']['boolean']='Логический тип';
_lang['common']['January']='Январь';
_lang['common']['February']='Февраль';
_lang['common']['March']='Март';
_lang['common']['April']='Апрель';
_lang['common']['May']='Май';
_lang['common']['June']='Июнь';
_lang['common']['July']='Июль';
_lang['common']['August']='Август';
_lang['common']['September']='Сентябрь';
_lang['common']['October']='Октябрь';
_lang['common']['November']='Ноябрь';
_lang['common']['December']='Декабрь';
_lang['common']['typographed']='Типографирование завершено';
_lang['common']['view_comments']='Посмотреть комментарии';

_lang['common']['Mon']='Пн';
_lang['common']['Tue']='Вт';
_lang['common']['Wed']='Ср';
_lang['common']['Thu']='Чт';
_lang['common']['Fri']='Пт';
_lang['common']['Sat']='Cб';
_lang['common']['Sun']='Вск';  

_lang['common']['you_are_assured_what_wish_to_remove_all']='Вы уверены в том что  хотите очистить все объекты?';
_lang['common']['link']='Ссылка';

_lang['common']['type']='Тип';  

_lang['common']['fileManager']='Файл-менеджер';
_lang['common']['choose']='Выбор';  
_lang['common']['cancel']='Отмена';
_lang['common']['target']='Назначение';
_lang['common']['source']='Источник';
_lang['common']['priority']='Приоритет';


_lang['common']['cache_clear']='Очистка кеша';
_lang['common']['cache-cleared']='Кеш очищен';
_lang['common']['apiAccess']='Доступ к api';
_lang['common']['settingsGlobal']='Глобальные настройки';




_lang['validation']={};
_lang['validation']['This-field-is-required']='Данное поле обязательно к заполнению.';
_lang['validation']['validate-number']='В этом поле может быть только число.';
_lang['validation']['validate-digits']='В этом поле могут быть только цифры и точки.';
_lang['validation']['No-special-characters-allowed']='В этом поле можно В этом поле можно только буквы, цифры,cкобки,тире и пробел.';
_lang['validation']['validate-alpha-ext']='В этом поле можно только буквы,cкобки и пробел и спец.символы.';
_lang['validation']['No-special-characters-allowed-num']='В этом поле можно использовать символы только из дипазона от (a-Z), пробелы и прочие символы недопустимы.';
_lang['validation']['validate-date']='Это поле может содержать только дату.';
_lang['validation']['validate-email']='Введите правильный e-mail, например ivanov@mymail.ru';   
_lang['validation']['validate-url']='Введите правильный URL.';   
_lang['validation']['validate-date-au']= 'Используйте следующий формат даты dd/mm/yyyy';
_lang['validation']['validate-password']='пароль введен неправильно(не менее 6 символов,не должен быть равен имени пользователя)';
_lang['validation']['validate-password-again']='пароли не совпадают';
_lang['validation']['validate-selection']='Это поле должно иметь значение.';
_lang['validation']['validate-one-required']='Выберите хотя бы одну опцию.';
_lang['validation']['validate-float']='В этом поле могут быть только цифры и  значения с плавающей точкой.';


_lang['matrix']={};
_lang['matrix']['upload']='Закачка';
_lang['matrix']['security_error']='Ошибка безопасности';
_lang['matrix']['waiting']='Ожидание';
_lang['matrix']['ready']='Готово';
_lang['matrix']['file_manager']='Файл-менеджер';
_lang['matrix']['enter_folder_name']='Введите имя папки';   
_lang['matrix']['cant_create_folder']='Невозможно создать папку. Измените права доступа.';
_lang['matrix']['select_files_to_copy']='Выделите файлы для копирования';
_lang['matrix']['this_mode_allows_folders_selected_only']='В данном режиме могут быть выбраны только папки!';

_lang['matrix']['drop_files_here_to_upload']='Перетяните файлы для закачки сюда';
_lang['matrix']['browser_not_support_drag_upload']='Ваш браузер не поддерживают закачку файлов перетягиванием';
_lang['matrix']['use_form_to_upload']='Ваш браузер поддерживает  закачку только через обычную форму';
_lang['matrix']['file_is_too_big']="Файл слишком большой ({{filesize}}MiB). Макимальный доступный размер: {{maxFilesize}}MiB.";
_lang['matrix']['cant_upload_this_file_type']='Невозможно загрузить файл такого типа';
_lang['matrix']['server_respond_with_code']="Ответ сервера код -  {{statusCode}}.";
_lang['matrix']['cancel_upload']="Отменить закачку";
_lang['matrix']['shure_to_cancel']="Вы уверенны что хотите отменить закачку?";
_lang['matrix']['remove_file']="Удалить файл";
_lang['matrix']['max_files_exceed']="Вы не можете закачать больше файлов";




/*calandar related*/

            _lang['common']['Now']='Сейчас';
            _lang['common']['Today']='Сегодня'; 
            _lang['common']['Time']='Время';
            _lang['common']['Exact_minutes']='Точно минут';
            _lang['common']['Select_Date_and_Time']='Выбрать дату и время';
            _lang['common']['Select_Time']='Выберите время';
            _lang['common']['Open_calendar']='Открыть календарь';
