var _lang=[];
   
_lang_xoad={};
_lang_xoad['invalid_response']='invalid response';
_lang_xoad['session_time_expired']='Session time expired, please refresh this pages';
_lang_xoad['empty_response']='Empty response returned';

_lang['common']={};
_lang['common']['tag_editor']='Tag editor';
_lang['common']['info']='Information';
_lang['common']['error']='Error';     
_lang['common']['copy']='Copy';
_lang['common']['paste']='Paste';
_lang['common']['refresh']='Refresh';
_lang['common']['delete']='Delete';
_lang['common']['editing']='Editing';
_lang['common']['edit']='Edit';
_lang['common']['save_error']='Save error';
_lang['common']['warning']='Warning'; 
_lang['common']['group_success_saved']='Group save successfully';
_lang['common']['group_is_not_saved']='Group do not saved';
_lang['common']['add_group']='Add folder ';
_lang['common']['add_folder']='Add folder';
_lang['common']['name']='Name';
_lang['common']['alias']='Alias';
_lang['common']['add']='Add';
_lang['common']['save']='Save';
_lang['common']['initiate-editor']='Edit';
_lang['common']['tags']='Tags';
_lang['common']['tagManager']='Tag manager';

_lang['common']['file-manager']='File manager';
_lang['common']['set_permissions']='Set permissions';

_lang['common']['select']='Select';
_lang['common']['change']='Change';            
_lang['common']['error_on_server']='Server error';
_lang['common']['export']='Export';
_lang['common']['search-results']='Search results';
_lang['common']['manager']='Manager';
_lang['common']['date']='Date';
_lang['common']['description']='Description';
_lang['common']['subject']='Subject';

_lang['common']['add_category']='Add category';
_lang['common']['it_is_changed']='Changed';
_lang['common']['sec']='sec';
_lang['common']['modul']='Modul';
_lang['common']['restore']='Restore';
_lang['common']['address']='Address';
_lang['common']['successfully_added']='successfully added';
_lang['common']['status']='Status';
_lang['common']['user_with_such_name_already_exists']='User with such a name already exists';
_lang['common']['user_success_saved']='User successfully saved';
_lang['common']['group_users_success_saved']='Users group successfully saved';
_lang['common']['name_group_properties']='Properties group name ';
_lang['common']['options_not_found'] ='Proprties are absent ';
_lang['common']['download_file'] ='Download the file';
_lang['common']['nothing_found']='Found nothing';
_lang['common']['January']='January';
_lang['common']['February']='February';
_lang['common']['March']='March';              
_lang['common']['April']='April';
_lang['common']['May']='May';
_lang['common']['June']='June';
_lang['common']['July']='July';
_lang['common']['August']='August';
_lang['common']['September']='September';
_lang['common']['October']='October';
_lang['common']['November']='November';
_lang['common']['December']='December';
  

 _lang['common']['Mon']='Mon';
 _lang['common']['Tue']='Tue';
 _lang['common']['Wed']='Wed';
 _lang['common']['Thu']='Thur';
 _lang['common']['Fri']='Fri';
 _lang['common']['Sat']='Sat';
 _lang['common']['Sun']='Sun';

_lang['common']['you_really_wish_to_remove_this_objects']='You really wish to remove this objects ?';
_lang['common']['you_really_wish_to_copy_this_object']='You really wish to copy this object?';
_lang['common']['you_really_wish_to_remove_this_object']='You really wish to remove this object?';
_lang['common']['you_really_wish_to_disable_this_object']='You really wish to disable this object?';
_lang['common']['you_really_wish_to_disable_this_objects']='You really wish to disable this objects?';
_lang['common']['enable']='Enable';
_lang['common']['disable']='Disable';

_lang['common']['options']='Options';


_lang['common']['cache_clear']='Cache clear';
_lang['common']['cache-cleared']='Cache cleared';
_lang['common']['apiAccess']='API access';
_lang['common']['settingsGlobal']='Global settings';
_lang['common']['setTreeBoost']='Boost tree to Redis';



 _lang_xlist={};
_lang_xlist['choose']='Choose';  
_lang_xlist['cancel']='Cancel';  

_lang['validation']={};
_lang['validation']['required']='Field is reqired';
_lang['validation']['validate-number']='Only number can be input here';
_lang['validation']['validate-digits']='Only digits and dots can be input here';
_lang['validation']['validate-alpha']='Only letters digits, brackets, dashes and  spaces can be input here.';
_lang['validation']['validate-alpha-ext']='Only letters, brackets, spaces and special symbols can be input here.';
_lang['validation']['validate-alphanum']='Only symbols with a range a-Z can be input here, spaces and other symbols are unallowable';
_lang['validation']['validate-date']='Only date can be input here.';
_lang['validation']['validate-email']='Enter correct e-mail, e.g. ivanov@mymail.ru';   
_lang['validation']['validate-url']='Enter correct URL.';   
_lang['validation']['validate-date-au']= 'Use the folowing date format dd/mm/yyyy';
_lang['validation']['validate-password']="Password is incorrect (not less then 6 symbols, can't be the same as user name)";
_lang['validation']['validate-password-again']='Passwords do not coincide';
_lang['validation']['validate-selection']='The field must have value';
_lang['validation']['validate-one-required']='Choose at least one option.';

_lang['matrix']={};
_lang['matrix']['upload']='Uploading';
_lang['matrix']['security_error']='Sesurity error';
_lang['matrix']['waiting']='Waiting';
_lang['matrix']['ready']='Ready';
_lang['matrix']['enter_folder_name']='Enter folder name';   
_lang['matrix']['cant_create_folder']="Can't create folder. Change access right.";
_lang['matrix']['select_files_to_copy']='Select files to copy';
_lang['matrix']['this_mode_allows_folders_selected_only']='Only folders can be selected in this mode!';
 

 