var __x4frontEditorModule = new Class(
    {

        initialize: function (domReplica, moduleData) {
            this.domReplica = domReplica;
            this.moduleData = moduleData;
            this.connector = new Connector('fronteditor');
            this.getModuleParams();
            this.moduleRender();
        },


        getModuleParams: function () {
            this.connector.execute({getModuleParams: {id: this.moduleData.id}});
            this.moduleParams = this.connector.result.module;

        },

        moduleRender: function () {

            var li = '';
            if (this.moduleParams.templates) {

                $.each(this.moduleParams.templates, function (k, v) {
                    li += '<li><a target="_blank" href="/admin.php?#e/templates/edit_FILE/?id=' + v.fullPathBase + '"> ' + v.name + '</a></li>';
                })


            }

            moduleData = '<ul>' +
                '<li>' + this.moduleData.type + '</li>' + li +
                '<li>' + this.moduleData.executeTime + '</li>';
            '</ul>';

            this.domReplica.append(moduleData);

        }


    });

var __x4frontEditor = new Class(
    {

        initialize: function () {
            this.modules = [];
            this.slotz = [];
            this.switchView = true;
            this.connector = new Connector('fronteditor');

            jQuery(document).bind("keyup keydown", function (e) {
                if (e.ctrlKey) {
                    if (this.switchView) {
                        $('.__x4Slot,.__x4module').hide();
                        this.switchView = false;
                    } else {
                        $('.__x4Slot,.__x4module').show();
                        this.switchView = true;
                    }

                }
            });

            setTimeout(function () {
                this.absolutizeSlotz();
                this.absolutizeModules();

            }.bind(this), 300);

        },


        absolutizeSlotz: function () {

            var slotz = $('.__x4SlotMap');
            slotz.each(function (k, v) {
                data = $(v).data('info');
                replica = this.absolutize(v, 998, null, null, true);
                replica.addClass('__x4Slot');
                replica.attr('sourceid', data['id']);

                totalHeight = $(v).prop('scrollHeight');
                totalWidth = $(v).prop('scrollWidth');

                replica.html(data['name']);
                replica.css({
                        border: '2px dotted #A1A1A1', opacity: 0.5,
                        minHeight: '20px',
                        minWidth: '20px',

                        height: totalHeight,
                        width: totalWidth
                    }
                );

                this.slotz[slotz[i].id] = replica;

            }.bind(this));

        },


        absolutizeModules: function () {
            var oldpid = null;
            var modules = $('.__x4moduleMap');

            modules.each(function (k, v) {
                replica = this.absolutize(v, 999, 50, 50, true);

                data = $(v).data('info');
                replica.addClass('__x4module');
                replica.attr('sourceid', data['id']);
                replica.css(
                    {
                        minHeight: '20px',
                        minWidth: '20px',
                        padding: '8px',
                        border: '1px solid red',
                        background: 'white'
                    }
                );

                this.modules[data['id']] = new __x4frontEditorModule(replica, data);
            }.bind(this));

        }

        , absolutize: function (element, zindex, w, h, dchs) {

            if ($(element).css("position") == 'absolute') return;

            _elementHover = jQuery('<div />', {});

            var p = $(element).offset();

            _elementHover.css({
                'position': 'absolute',
                'z-index': zindex,
                'top': p.top,
                'left': p.left
            });

            $('body').append(_elementHover);

            return _elementHover;
        }


    });