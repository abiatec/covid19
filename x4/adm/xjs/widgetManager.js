
var _widget=new Class({
    
        Implements: [Options],      
        module:null,    
        connector:null,                         
        options: 
        {
            size:6 
        }
        ,
          
        initialize: function(options)
        {        
            this.setOptions(options);                     
            this.module=AI.loadModule(this.options.module, 'silent', true);        
            this.instanceName='#widget_'+this.options.name;
            this.connector=this.module.connector;
        },
        
        build:function()
        {
            
        },
        
        destroy:function()
        {
            
        }
        
           
 });

 var _widgetManager= new Class({
    
        Implements: [Options],      
        widgetStorage:{},
        options: {
            target:'#widgetContainer', 
        }
        ,
        
        get:function(name)
        {            
            if(Object.contains(this.widgetStorage,name))
            {
                return this.widgetStorage[name];             
            }
               
        },
        initialize: function(options)
        {        
            this.setOptions(options);
            this.container=$(this.options.target);            
        },   
        
        addWidget:function(instance)
        {
             this.widgetStorage[instance.options.module]=instance;   
             this.container.append(instance.buildHtml());
             instance.domInstance=$(instance.instanceName);
             instance.attach(this);
        },
        
        removeWidget:function()
        {
            if(Object.contains(this.widgetStorage,name))
            {
                this.widgetStorage[name].destroy();                
                delete this.widgetStorage[name];             
            }
              
        }
    
 });
 
 
 var ishopWidgetSales=new Class({
    
        
        Extends: _widget,        
        domInstance:null,  
        initialize: function(options)
        {        
            this.parent(options);
            this.template=TH.getTplHB('AdminPanel', 'ishopStat_widget');
        },
        
        buildHtml:function()
        {            
           var data=this.connector.execute({getWidgetStat:true});
            data.name=this.options.name;            
            return this.template({data:data});
        },
        
        attach:function(widgetManager) {
            this.connector.execute({getWidgetGraph: true});

           var data = this.connector.result.data;
        },
        
        destroy:function()
        {
            
        }
        
           
 });
      
      
      
      
var pagesCacheStat=new Class({
    
        
        Extends: _widget,        
        domInstance:null,  
        initialize: function(options)
        {        
            this.parent(options);
            this.template=TH.getTplHB('AdminPanel', 'pagesCacheStat_widget');
        },
        
        buildHtml:function()
        {         
            
            return this.template();
        },
        
        
        refresh:function(){
            
            setInterval(function(){
                
                  this.connector.execute({getWidgetCacheStat:true},function(data,con)
                  {
                      
                     data=con.result.data;

                  }.bind(this));   
                  
                  
            }.bind(this),60000);
            
        },
        
        destroy:function()
        {
            
        }
        
           
 });      
 
 
 
 
 var catalogStat=new Class({
    
        
        Extends: _widget,        
        domInstance:null,  
        initialize: function(options)
        {        
            this.parent(options);
            this.template=TH.getTplHB('catalog', 'catalogStat_widget');
            this.data=null;
        },
        
        buildHtml:function()
        {                     
            this.data=this.connector.execute({getWidgetStat:true});                                            
            return this.template(this.data);
        },
        
        
        refresh:function(){
            
          
            
        },
        
        
        attach:function(widgetManager)
        {
                                
            
        },
        
        
        destroy:function()
        {
            
        }
        
           
 });      
