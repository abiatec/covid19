<?php

namespace X4\Classes;

class Event
{

}

class XEventMachine extends \xSingleton
{
    public $firedEvents;
    protected $_callbacks = [];
    protected $lastReturn = null;

    /**
     *  detects action on event
     * @param string $eventName before@module:event  after@module:event   module:event
     * @param string $callback method
     * @param $callbackContext objectContext
     */

    public function on($eventName, $callback, $callbackContext)
    {
        $this->_callbacks[$eventName][] = array(
            'context' => $callbackContext,
            'callback' => $callback
        );
    }

    /**
     * unregisters event by name
     * @param string $eventName - event name
     * @param string $callback - remove given callback if set
     */
    public function unregister($eventName, $callback = null)
    {
        if (!empty($callback)) {
            foreach ($this->_callbacks[$eventName] as $k => $v) {
                if ($this->_callbacks[$eventName][$k]['callback'] == $callback) {
                    unset($this->_callbacks[$eventName][$k]['callback']);
                }
            }
        } else {
            unset($this->_callbacks[$eventName]);
        }
    }

    /**
     *  generates an event
     * @param string $eventName
     * @param array $data - event data
     */
    public function fire($eventName, $data = null)
    {
        if (!empty($this->_callbacks[$eventName])) {
            $lastReturn = null;

            foreach ($this->_callbacks[$eventName] as $callback) {

                if (method_exists($callback['context'], $callback['callback'])) {
                    if ($lastReturn) {
                        $data = $lastReturn;
                    }
                    $this->firedEvents[] = array('event' => $eventName, 'callbank' => $callback['callback']);

                    $return = call_user_func_array(array(
                        $callback['context'],
                        $callback['callback']
                    ), array(
                        array(
                            'context' => $callback['context'],
                            'data' => &$data,
                            'event' => $eventName
                        )
                    ));

                    if (!empty($return) && is_array($return)) {
                        $lastReturn = $return;
                    }
                } else {
                    trigger_error('event function not defined ' . get_class($callback['context']) . '-' . $callback['callback']);
                }
            }
            return $lastReturn;
        }
    }


}
