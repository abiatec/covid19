<?php

namespace X4\Classes;


class XCacheMapper
{
    static $driver;
    static $cacheDir;

    public static function initialize($cacheDir)
    {
        \Common::loadDriver('XCache', 'XCacheRedisDriver');
        \XCacheRedisDriver::initDriver(true);
        self::$cacheDir = $cacheDir;

    }

    public static function setMap($prefix, $keys, $item)
    {
        if (is_array($keys)) {
            $instance = \XCacheRedisDriver::getInstance();
            foreach ($keys as $key) {
                $instance->sadd($prefix . ':' . $key, $item);
            }
        }
    }

    public static function getMap($prefix, $key)
    {
        return \XCacheRedisDriver::getInstance()->smembers($prefix . ':' . $key);
    }


    public static function clearMemory($prefix, $keys)
    {
        $instance = \XCacheRedisDriver::getInstance();
        foreach ($keys as $key) {
            $instance->del($prefix . ':' . $key);
        }
    }

    public static function remove($prefix, $keys)
    {
        if (is_numeric($keys)) {
            $keys = array($keys);
        }

        foreach ($keys as $key) {
            $itemsToRemove = self::getMap($prefix, $key);
            if (!empty($itemsToRemove)) {
                foreach ($itemsToRemove as $item) {
                    unlink(self::$cacheDir . $item);
                }
            }
        }

    }
}
