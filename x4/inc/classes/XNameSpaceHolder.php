<?php

namespace X4\Classes;

class XNameSpaceHolder
{
    private static $nameSpaces;
    private static $callModels;
    private static $lastInstance;


    public static function getInstanceSource($ns, $method)
    {
        return self::$nameSpaces[$ns][$method];
    }

    /**
     *  Add all methods to namespace
     *
     * @param mixed $ns - namespace name
     * @param mixed $object - object with methods
     */
    public static function addObjectToNS($ns, $object)
    {
        $className = get_class($object);


        $f = new \ReflectionClass($className);

        $parent = $f->getParentClass();
        if ($methods = $f->getMethods(\ReflectionMethod::IS_PUBLIC)) {

            foreach ($methods as $method) {
                if (($method->class == $className || $method->class == $parent->name) && !(strstr($method->name, '__'))) {
                    self::$nameSpaces[$ns][$method->name] =& $object;
                }
            }
        }

    }

    /**
     * Adds method to namespace
     *
     * @param string $ns - namespace name
     * @param mixed $methods - one or array of methods
     * @param object $object - object caller
     */
    public static function addMethodsToNS($ns, $methods, $object)
    {
        if (!is_array($methods)) {
            $methods = array($methods);
        }

        if ($methods) {
            foreach ($methods as $method) {
                self::$nameSpaces[$ns][$method] =& $object;
            }
        }

    }

    /**
     *
     * Checks names existance, if method pointed then additional method check is ocurring
     * @param string $ns - namespace name
     * @param string $method -method name
     */
    public static function isNameSpaceExists($ns, $method = '')
    {

        if (isset(self::$nameSpaces[$ns])) {
            if ($method && method_exists(self::$nameSpaces[$ns][$method], $method)) {
                return true;

            } elseif (self::$nameSpaces[$ns] && !$method) {
                return true;

            } else {

                return false;
            }
        }

    }

    /**
     * Add call model
     *
     * @param mixed $name - model name
     * @param mixed $wakeUpFunction - lambda function for model call
     * standard call models module,plugin,classs
     */


    public static function addCallModel($name, $wakeUpFunction)
    {
        self::$callModels[$name] = $wakeUpFunction;

    }

    /**
     * get last called instance
     *
     */
    public static function getLastInstance()
    {
        return self::$lastInstance;
    }

    /**
     * Call a method from given namespace
     *
     * @param string $ns - namespace name
     * @param string $method - method name
     * @param mixed $arguments - call arguments
     * @return mixed
     */

    public static function call($ns, $method, $arguments = null, $additionalArguments = null)
    {

        if (!method_exists(self::$nameSpaces[$ns][$method], $method)) {
            $nsExpl = explode('.', $ns);
            if ($wakeUpFunction = self::$callModels[$nsExpl[0]]) {
                $wakeUpFunction($nsExpl);
            }
        }


        if (method_exists(self::$nameSpaces[$ns][$method], $method)) {
            self::$lastInstance = self::$nameSpaces[$ns][$method];

            $result = call_user_func_array(array(self::$nameSpaces[$ns][$method], $method), array($arguments, $additionalArguments));

            if ($result === null) {
                return true;
            } else {
                return $result;
            }

        } else {

            return null;
        }
    }

}


XNameSpaceHolder::addCallModel('plugin', function ($params) {


    $type = end($params);

    switch ($type) {

        case 'tpl':

            \xTpl::__load($params[1] . '.' . $params[2], true);

            break;

        case 'xfront':


            if ($xfrontModuleInstance = \xCore::pluginFactory($params[1] . '.' . $params[2] . '.xfront')) {
                XNameSpaceHolder::addObjectToNS('plugin.' . $params[1] . '.xfront', $xfrontModuleInstance);
            }

            break;

        case 'back':

                 \xCore::pluginFactory($params[1] . '.' . $params[2] . '.back');

            break;

    }

});


XNameSpaceHolder::addCallModel('module', function ($params) {

    switch ($params[2]) {

        case 'tpl':
            \xTpl::__load($params[1]);

            break;

        case 'xfront':

            if ($xfrontModuleInstance = \xCore::moduleFactory($params[1] . '.' . $params[2])) {
                XNameSpaceHolder::addObjectToNS('module.' . $params[1] . '.xfront', $xfrontModuleInstance);

                $xfrontModuleInstance->initiateXfrontPlugins();
                $xfrontModuleInstance->initiateFrontActionsCallNS();

            }

            break;

        case 'front':

            if ($frontModuleInstance = \xCore::moduleFactory($params[1] . '.' . $params[2])) {
                XNameSpaceHolder::addObjectToNS('module.' . $params[1] . '.front', $frontModuleInstance);
            }

            break;

        case 'back':


            if ($backModuleInstance = \xCore::moduleFactory($params[1] . '.' . $params[2])) {
                XNameSpaceHolder::addObjectToNS('module.' . $params[1] . '.back', $backModuleInstance);
                $backModuleInstance->initiateBackPlugins();
            }

            break;

        // deprecated
        case 'class':

            if ($classInstance = \xCore::incModuleFactory($params[1])) {

                XNameSpaceHolder::addObjectToNS('module.' . $params[1] . '.' . $params[2], $classInstance);

            }

            break;


        case 'adm':
            $className = "\\X4\AdminBack\\{$params[1]}";
            $classInstance = new $className();

            if (!empty($classInstance)) {
                XNameSpaceHolder::addObjectToNS('module.' . $params[1] . '.' . $params[2], $classInstance);

            }

            break;

        case 'default':
            break;

    }

});