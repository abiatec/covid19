<?php


namespace X4\Classes;

class TreeJsonSource
{
    private $_options;
    private $_tree;
    public $result;


    public static $fromTimeStamp;

    public static $cutWords;

    public function __construct($tree)
    {
        $this->_tree = $tree;
    }


    public function setOptions($options)
    {
        $this->_options = $options;
    }


    public function createView($id = 1, $page = null, $levels = 1)
    {

        if ($this->_options['emulateRoot'] && $id == 0) {
            $result['data_set']['rows'][1] = array('data' => $this->_options['emulateRoot']['data'], 'xmlkids' => 1, 'image' => $this->_options['emulateRoot']['image']);
            return $result;
        }


        if ($this->_options['showNodesWithObjType']) {
            $addWhere = array(array('@obj_type', '=', $this->_options['showNodesWithObjType']));
        }

        if ($page) {
            $currentPosition = ($page - 1) * $this->_options['onPage'];
        }

        $nodes = $this->_tree->getDisabled()->selectParams('*')->selectStruct('*')->childs($id, $levels)->where($addWhere, true)->limit($currentPosition, $this->_options['onPage']);

        if (!empty($this->_options['sortby'])) {
            $nodes->sortby($this->_options['sortby'][0], $this->_options['sortby'][1]);
        }

        $nodes = $nodes->run();

        if (!empty($nodes)) {
            $nodesCount = $this->_tree->nodesAllCount;
            $nodesStrip = \XARRAY::asKeyVal($nodes, 'id');
            $childsNodes = $this->_tree->childNodesExist($nodesStrip, $nodes[0]['ancestorLevel'], $this->_options['showNodesWithObjType']);
            foreach ($nodes as $node) {
                $nodeId = $node['id'];
                if (is_array($this->_options['columns'])) {

                    foreach ($this->_options['columns'] as $key => $tempValue) {

                        if ($key[0] == '>') {
                            $paramedKey = true;
                            $key = substr($key, 1);
                        } else {
                            $paramedKey = false;
                        }

                        if (!$tempValue['name']) {
                            $tempValue['name'] = $key;
                        }

                        $extData[$tempValue['name']] = ($paramedKey) ? $node['params'][$key] : $node[$key];


                        if ($tempValue['transformList']) {
                            $extData[$tempValue['name']] = $tempValue['transformList'][$extData[$tempValue['name']]];
                        }


                        if ($tempValue['onAttribute']) {
                            $extData[$tempValue['name']] = $tempValue['onAttribute']($tempValue['onAttributeParams'], $extData[$tempValue['name']], $nodeId);
                        }

                    }


                    if (!empty($this->_options['onRecord'])) {
                        $extData = $this->_options['onRecord']($extData);
                    }

                    if (!empty($this->_options['zeroLead'])) {
                        $idRevert = '0' . $nodeId;
                    } else {
                        $idRevert = $nodeId;
                    }

                    if ($this->_options['vanillaFormat']) {
                        $result['data'][$nodeId] = $extData;
                    } elseif ($this->_options['gridFormat']) {


                        $r = array('id' => $idRevert, 'image' => $this->_options['imagesIcon'][$node['obj_type']], 'data' => array_values($extData), 'obj_type' => $node['obj_type']);

                        if (in_array($node['obj_type'], $this->_options['showNodesAsParents'])) {
                            $r['xmlkids'] = 1;
                        }

                        if (in_array($nodeId, $childsNodes)) {
                            $r['xmlkids'] = 1;
                        }

                        $result['data_set']['rows'][$idRevert] = $r;

                    } else {


                        $result['data_set']['rows'][$idRevert] = array('data' => array_values($extData));
                    }
                }

            }

            if ($this->_options['onPage']) {
                $result['pagesNum'] = ceil($nodesCount / $this->_options['onPage']);
            }

            return $result;
        }
    }


}

TreeJsonSource::$fromTimeStamp = function ($params, $value, $id) {
    return date($params['format'], $value);
};


TreeJsonSource::$cutWords = function ($params, $value) {
    return XSTRING::findnCutSymbolPosition($value, " ", $params['count']);
};
