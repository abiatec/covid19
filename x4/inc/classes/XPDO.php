<?php

namespace X4\Classes;

class XPDOExceptionHandler
{
    public function __construct(\PDOException $e)
    {
        if (method_exists($this, $method = 'code' . $e->getCode())) {
            return call_user_func_array(array($this, $method), [$e]);
        } else {
            die($e->getMessage());
        }
    }
}


class XPDO extends \PDO
{
    private static $objInstance;
    private static $host;
    private static $dbname;
    private static $user;
    private static $password;
    private static $encoding;
    private static $port;
    public static $lastInserted;


    private function __clone()
    {
    }

    /**
     * Create PDO connection
     * @param
     * @return $objInstance;
     */
    public static function setSource($host, $dbname, $user, $password, $encoding = 'utf8', $port = 3306)
    {
        self::$host = $host;
        self::$dbname = $dbname;
        self::$user = $user;
        self::$password = $password;
        self::$encoding = $encoding;
        self::$port = $port;
        self::$lastInserted = null;
    }

    /**
     * @return \PDO
     */
    public static function getInstance()
    {
        if (!self::$objInstance) {
            try {
                self::$objInstance = new \PDO('mysql:host=' . self::$host . ';port=' . self::$port . ';dbname=' . self::$dbname, self::$user, self::$password);
                self::$objInstance->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            } catch (\PDOException $e) {
                throw $e;
            }

            self::$objInstance->exec('SET CHARACTER SET ' . self::$encoding);
            self::$objInstance->exec('set character_set_results=' . self::$encoding);
            self::$objInstance->exec('SET NAMES ' . self::$encoding);
        }

        return self::$objInstance;
    }


    /**
     * Get column names
     * @param $table
     * @return $mix
     */
    private static function getColumnNames($table)
    {
        static $tables = [];

        if (isset($tables[$table])) {
            return $tables[$table];
        }

        $PDO = self::$objInstance;
        $sql = 'SHOW COLUMNS FROM ' . $table;
        $stmt = $PDO->prepare($sql);
        $columnNames = [];
        try {
            if ($stmt->execute()) {
                while ($rawColumnData = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                    if ($rawColumnData['Field'] != 'id') {
                        $columnNames[$rawColumnData['Field']] = '';
                    } else {
                        $columnNames[$rawColumnData['Field']] = 'NULL';
                    }
                }

                return $tables[$table] = $columnNames;
            }
        } catch (\PDOException $e) {
            return new XPDOExceptionHandler($e);
        }
    }

    static function escapeInner($data)
    {
        $data = str_replace("\\", "\\\\", $data);
        $data = str_replace("'", "\'", $data);
        $data = str_replace('"', '\"', $data);
        $data = str_replace("\x00", "\\x00", $data);
        $data = str_replace("\x1a", "\\x1a", $data);
        $data = str_replace("\r", "\\r", $data);
        $data = str_replace("\n", "\\n", $data);
        return ($data);

    }


    /**
     * Get data from table
     * @param  array $select - params list
     * @param  string $from - table/s
     * @param  string $where - condition
     * @return int the number of rows deleted
     */
    public static function selectIN($select = '*', $from, $where = '', $specialCondition = '')
    {
        $PDO = self::$objInstance;

        if (is_array($select)) {
            $select = '`' . implode('`,`', $select) . '`';
        }

        if (is_array($where)) {
            $where = array_unique($where);
            $where = 'where id in ("' . implode('","', $where) . '")';
        } elseif (is_int($where)) {
            $where = 'where id=' . $where;
        } elseif ($where) {
            $where = 'where ' . $where;
        }

        $query = "select $select from $from $where  $specialCondition";

        if ($result = $PDO->query($query)) {
            return $result->fetchAll(\PDO::FETCH_ASSOC);
        }
    }


    public static function deleteIN($from, $where = '')
    {
        $PDO = self::$objInstance;

        if (is_array($where)) {

            $where = array_unique($where);
            $whereString = str_repeat('?,', count($where) - 1) . '?';
            $stmt = $PDO->prepare("delete from {$from} where id in ($whereString)");

            if ($stmt->execute($where)) {
                return $stmt->rowCount();
            }

        } elseif (is_int($where)) {

            $stmt = $PDO->prepare("delete from {$from} where id = :id");
            if ($stmt->execute([':id' => $where])) {
                return $stmt->rowCount();
            }

        } elseif (!empty($where)) {

            $where = 'where ' . $where;
            $query = "delete from $from $where";

            if ($PDO->query($query)) {
                return true;
            }
        }

    }


    /**
     * @return int
     */
    public static function getLastInserted()
    {
        return self::$lastInserted = self::$objInstance->lastInsertId();
    }

    /**
     * Insert data to table
     * @param  string $table - table name
     * @param  array $insertValues -  assoc  array of values
     * @return int
     */

    public static function insertIN($table, $insertValues)
    {
        $PDO = self::$objInstance;
        $checkFields = self::getColumnNames($table);

        foreach ($insertValues as $key => $val) {
            if (array_key_exists($key, $checkFields)) {
                $checkFields[$key] = is_null($val) ? $val : self::escapeInner($val);
            }
        }

        $values = array_values($checkFields);
        $fields = array_keys($checkFields);
        $fieldList = implode('`,`', $fields);
        $valuesBindsString = implode(',:', $fields);

        $query = "INSERT INTO `$table` (`{$fieldList}`) values (:{$valuesBindsString})";
        $stmt = $PDO->prepare($query);

        foreach ($checkFields as $param => $val) {

            if (is_null($val) or strtoupper($val) == 'NULL') {
                $stmt->bindValue(':' . $param, null, \PDO::PARAM_INT);
            } elseif (is_int($val)) {
                $stmt->bindValue(':' . $param, $val, \PDO::PARAM_INT);
            } else {
                $stmt->bindValue(':' . $param, $val);
            }
        }

        if ($stmt->execute()) {
            return self::getLastInserted();
        }
    }

    /**
     * Update table data
     * @param  string $table - table name
     * @param  mixed $express updating expression
     * @param  mixed $updateValues update data
     * @return bool
     */
    public static function updateIN($table, $express, $updateValues)
    {
        $PDO = self::$objInstance;

        if (is_int($express)) {
            $express = "`id` = '$express' LIMIT 1";
        }

        $updateLine = [];

        foreach ($updateValues as $key => $val) {
            $val = self::escapeInner($val);
            $updateLine[] = "`$key` = '$val'";
        }

        $implodedUpdate = implode($updateLine, ',');

        $query = "UPDATE `$table` SET $implodedUpdate WHERE $express";

        if ($PDO->exec($query) !== false) {
            return true;
        }
    }


    public static function multiInsertIN($tableName, $data, $replaceMode = false)
    {

        $pdoObject = self::$objInstance;

        $toBind = $rowsSQL = [];

        $columnNames = array_keys(current($data));

        foreach ($data as $arrayIndex => $row) {

            $params = [];

            foreach ($row as $columnName => $columnValue) {
                $param = ":" . str_replace('.', '__', $columnName) . $arrayIndex;
                $params[] = $param;
                $toBind[$param] = $columnValue;
            }

            $rowsSQL[] = "(" . implode(", ", $params) . ")";
        }

        if (!$replaceMode) {
            $type = 'INSERT';
        } else {
            $type = 'REPLACE';
        }

        $sql = "{$type} INTO `$tableName` (`" . implode("`,`", $columnNames) . "`) VALUES " . implode(", ", $rowsSQL);

        $pdoStatement = $pdoObject->prepare($sql);

        foreach ($toBind as $param => $val) {
            $pdoStatement->bindValue($param, $val);
        }

        try {

            return $pdoStatement->execute();

        } catch (\PDOException $e) {

            return new XPDOExceptionHandler($e);
        }
    }


}

