<?php

namespace X4\Classes;

class XRegistry
{
    protected static $store = [];

    protected function __construct()
    {
    }

    /**
     * checks if value by key exists
     *
     * @param string $name
     * @return bool
     */
    public static function exists($name)
    {
        return isset(self::$store[$name]);
    }

    /**
     * return data by key
     * @param string $name
     * @return unknown
     */
    public static function get($name)
    {
        return (isset(self::$store[$name])) ? self::$store[$name] : null;
    }

    /**
     * Saves data by key
     *
     * @param string or object with static property name $name
     * @param unknown $obj
     * @return unknown
     */
    public static function set($name, $obj = null)
    {
        if (is_object($name)) {
            return self::$store[$name->name] = $name;
        } else {
            return self::$store[$name] = $obj;
        }
    }

    protected function __clone()
    {
    }
}