<?php

class XSTRING
{
    public static function trimall($str, $charlist = " \n\r")
    {
        return str_replace(str_split($charlist), '', $str);
    }


    public static function declination($number, $titles)
    {
        $cases = array(
            2,
            0,
            1,
            1,
            1,
            2
        );
        return $titles[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }

    public static function dateRecognize($date)
    {
        $matched = false;
        if (is_string($date)) {
            $matched = preg_match('/^\s*(\d\d?)[^\w](\d\d?)[^\w](\d{1,4}\s*$)/', $date, $match);
        }

        if ($matched) {
            return strtotime($date);
        } else {
            return $date;
        }
    }

    public static function Reg($word)
    {
        $word = preg_replace("/(\.|\-|\_)/", "", strtolower($word));
        $patterns = file('censor.bws');
        for ($i = 0; $i < count($patterns); $i++) {
            $patterns[$i] = trim($patterns[$i]);
            if (preg_match($patterns[$i], $word)) {
                return true;
            }
        }

        return false;
    }

    public static function censorfilter($string)
    {
        $string = trim($string);
        $str_words = explode(' ', $string);
        for ($i = 0; $i < count($str_words); $i++) {
            if (self::Reg($str_words[$i])) {
                $str_words[$i] = ' <font color=red>[censored]</font> ';
            }
        }

        return $string = implode(' ', $str_words);
    }

    public static function findnCutSymbolPosition($str, $symbol, $maxposition, $end = '')
    {

        $offset = 0;
        $str = trim($str);
        $l = strlen($str);

        if ($l <= $maxposition) {
            return $str;
        } else {
            $pos = strpos($str, $symbol);

            if (!$pos) return $str;

            if ($pos > $maxposition) {
                $offset = $pos;

            } else {

                while (($pos <= $maxposition) && ($pos !== false)) {
                    $offset = $pos + 1;
                    $pos = strpos($str, $symbol, $offset);
                }

            }
            $resStr = substr($str, 0, $offset);

            return $resStr . $end;
        }
    }


    public static function Words2AllForms($text)
    {
        require_once(xConfig::get('PATH', 'EXT') . 'phpMorphy/src/common.php');

        $opts = array
        (
            'storage' => PHPMORPHY_STORAGE_MEM,
            'with_gramtab' => false,
            'predict_by_suffix' => true,
            'predict_by_db' => true
        );


        $dir = xConfig::get('PATH', 'EXT') . 'phpMorphy/dicts/';
        $dict_bundle = new phpMorphy_FilesBundle($dir, 'rus');
        $morphy = new phpMorphy($dict_bundle, $opts);

        setlocale(LC_CTYPE, array
        (
            'ru_RU.CP1251',
            'Russian_Russia.1251'
        ));

        $words = preg_split('#\s|[,.:;!?"\'()]#', $text, -1, PREG_SPLIT_NO_EMPTY);

        $bulkWords = [];


        foreach ($words as $v) {
            if (strlen($v) > 3) {
                $v = iconv("UTF-8", "windows-1251", $v);
                $bulkWords[] = strtoupper($v);
            }
        }

        return $morphy->getAllForms($bulkWords);
    }

    public static function Words2BaseForm($text)
    {

        static $dictBundle, $morphy;


        require_once(xConfig::get('PATH', 'EXT') . 'phpMorphy/src/common.php');

        if (!$dictBundle) {
            $dir = xConfig::get('PATH', 'EXT') . 'phpMorphy/dicts/';
            $dictBundle = new phpMorphy_FilesBundle($dir, 'rus');
        }

        if (!$morphy) {
            $opts = array(
                'storage' => PHPMORPHY_STORAGE_MEM,
                'with_gramtab' => false,
                'predict_by_suffix' => true,
                'predict_by_db' => true
            );

            $morphy = new phpMorphy($dictBundle, $opts);
        }


        setlocale(LC_CTYPE, array('ru_RU.CP1251', 'rus_RUS.CP1251', 'rus_RUS.CP1251', 'Russian_Russia.1251'));

        $words = preg_replace('#\[.*\]#isU', '', $text);
        $words = preg_split('#\s|[,.:;В«В»!?"\'()]#', $words, -1, PREG_SPLIT_NO_EMPTY);

        $bulkWords = [];

        foreach ($words as $v) {
            if (strlen($v) > 3) {
                $bulkWords[] = strtoupper($v);
            }
        }

        $baseForm = $morphy->getBaseForm($bulkWords);
        $fullList = [];

        if (is_array($baseForm) && count($baseForm)) {
            foreach ($baseForm as $v) {
                if (is_array($v)) {
                    foreach ($v as $v1) {
                        if (strlen($v1) > 3) {
                            $fullList[$v1] = 1;
                        }
                    }
                }
            }
        }
        return join(' ', array_keys($fullList));
    }

    public static function cyrillicTranslit($title)
    {
        $iso9_table = array(
            'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Ѓ' => 'G',
            'Ґ' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Є' => 'YE',
            'Ж' => 'ZH', 'З' => 'Z', 'Ѕ' => 'Z', 'И' => 'I', 'Й' => 'J',
            'Ј' => 'J', 'І' => 'I', 'Ї' => 'YI', 'К' => 'K', 'Ќ' => 'K',
            'Л' => 'L', 'Љ' => 'L', 'М' => 'M', 'Н' => 'N', 'Њ' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T',
            'У' => 'U', 'Ў' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'TS',
            'Ч' => 'CH', 'Џ' => 'DH', 'Ш' => 'SH', 'Щ' => 'SHH', 'Ъ' => '',
            'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA',
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'ѓ' => 'g',
            'ґ' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'є' => 'ye',
            'ж' => 'zh', 'з' => 'z', 'ѕ' => 'z', 'и' => 'i', 'й' => 'j',
            'ј' => 'j', 'і' => 'i', 'ї' => 'yi', 'к' => 'k', 'ќ' => 'k',
            'л' => 'l', 'љ' => 'l', 'м' => 'm', 'н' => 'n', 'њ' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
            'у' => 'u', 'ў' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'ts',
            'ч' => 'ch', 'џ' => 'dh', 'ш' => 'sh', 'щ' => 'shh', 'ъ' => '',
            'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya'
        );

        $name = strtr($title, $iso9_table);
        $name = preg_replace('~[^A-Za-z0-9\'_\-\.]~', '-', $name);
        $name = preg_replace('~\-+~', '-', $name); // --- на -
        $name = preg_replace('~^-+|-+$~', '', $name); // кил - на концах

        return $name;
    }


}
