<?php

class XCODE
{
    static function translit($text, $dont_strip_tags = false, $dont_clean_specialchars = false)
    {
        if (!$dont_strip_tags) {
            $text = strip_tags($text);
        }

        $filter = array(
            "А" => "A",
            "а" => "a",
            "Б" => "B",
            "б" => "b",
            "В" => "V",
            "в" => "v",
            "Г" => "G",
            "г" => "g",
            "Д" => "D",
            "д" => "d",
            "Е" => "E",
            "е" => "e",
            "Ё" => "Yo",
            "ё" => "yo",
            "Ж" => "J",
            "ж" => "j",
            "З" => "Z",
            "з" => "z",
            "И" => "I",
            "и" => "i",
            "Й" => "I",
            "й" => "i",
            "К" => "K",
            "к" => "k",
            "Л" => "L",
            "л" => "l",
            "М" => "M",
            "м" => "m",
            "Н" => "N",
            "н" => "n",
            "О" => "O",
            "о" => "o",
            "П" => "P",
            "п" => "p",
            "Р" => "R",
            "р" => "r",
            "С" => "S",
            "с" => "s",
            "Т" => "T",
            "т" => "t",
            "У" => "U",
            "у" => "u",
            "ў" => "u",
            "Ф" => "F",
            "ф" => "f",
            "Х" => "h",
            "х" => "h",
            "Ц" => "Z",
            "ц" => "z",
            "Ч" => "Ch",
            "ч" => "ch",
            "Ш" => "Sh",
            "ш" => "sh",
            "Щ" => "Sch",
            "щ" => "sch",
            "Э" => "E",
            "э" => "e",
            "Ю" => "Yu",
            "ю" => "yu",
            "Я" => "Ya",
            "я" => "ya",
            "Ь" => "",
            "ь" => "",
            "Ъ" => "",
            "ъ" => "",
            "Ы" => "I",
            "ы" => "i",
            " " => "-"
        );
        if (!$dont_clean_specialchars) {
            $filter += array(
                '"' => '',
                "'" => "",
                "+" => "_plus_",
                "!" => "",
                "?" => "",
                '`' => '',
                '*' => '',
                '#' => '',
                '%' => '',
                '^' => '',
                ',' => '-'
            );
        }
        return strtr($text, $filter);
    }


    static function jsonDecode($string, $assoc = null)
    {

        $value = json_decode($string, $assoc);
        $error = json_last_error();
        if ($error != JSON_ERROR_NONE) {
            throw new Exception('json-format-error ' . $string);
        }

        return $value;

    }


    static function urldecodeArray(&$var)
    {
        if (is_array($var)) {
            foreach ($var as $key => $value) {
                if (is_array($value)) {
                    $value = XCODE::urldecodeArray($value);
                } else {
                    $value = urldecode($value);
                }
                $var[$key] = $value;
            }
            return $var;
        }
    }


}
