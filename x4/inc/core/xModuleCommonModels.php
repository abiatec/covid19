<?php

class xModuleCommonModels
{
    public $_commonObj;
    private $_config;
    public $models=[];


    public function __construct($commonObj)
    {
        $this->_config = xConfig::get('MODULES', $commonObj->_moduleName);
        $this->_commonObj = $commonObj;
    }


    public function __get($model)
    {
        if (!in_array($model, $this->models)) {
            $definition = 'X4\\Modules\\' . $this->_commonObj->_moduleName . '\\models\\' . $model;
            $this->models[$model] = new $definition($this->_commonObj);
        }

        return $this->models[$model];
    }


}
