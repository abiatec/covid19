<?php
/**
 * Parent API class
 */
class xModuleApi  extends xModulePrototype
{
    public function error($errorText, $errorCode)
    {
        return array(
            'result' => false,
            'error' => array(
                'text' => $errorText,
                'code' => $errorCode
            )
        );
    }
}
